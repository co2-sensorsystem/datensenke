////////////////////////////////////////////////////
// connection to sensor nodes / micro controllers //
////////////////////////////////////////////////////

import {broadcastToWebclients} from "./frontendInterface";
import {dLog, FINEDUSTSENSOR_STATES, LED_STATES} from "../frontend/config.js";
import {
    addMissingSensorNodeToMeasurement,
    onMeasurementDateReceived,
    waitingForMeasurementCycleData
} from "./measurementController";
import {SERVER_STATE, STATE} from "./controller";
import * as DB from "./database";
import {sleep} from "../frontend/js/modules/utils.mjs";

require = require("esm")(module);
const UDP = require("dgram");
const IP = require("ip");
const SERVER_CONF = require('./config');

const UDP_PORT = 33333; // port for connection to microcontrollers
const UDP_HOST = IP.address(); // ip for connection to microcontrollers
let UDP_SERVER = null;

/**
 * Current power state of the LEDs on the sensor nodes.
 * @type {string}
 */
export let currentLEDState = LED_STATES.ON;

export function setLEDState(state) {
    currentLEDState = state;
}
/**
 * @typedef udpClientInfo
 * @property {string} mac
 * @property {int} id
 * @property {int} port
 * @property {string} address
 * @property {number} [battery]
 * @property {string} [LED]
 * @property {string} [fineDustSensor]
 */
/**
 * Current power state of the fine dust sensor on the sensor nodes.
 * @type {string}
 */
export let currentFineDustSensorState = FINEDUSTSENSOR_STATES.ON;

export function setFineDustSensorState(state) {
    currentFineDustSensorState = state;
}

/**
 * List of connected UDP clients, i.e. sensor nodes.
 * FIXME: make an array of used udpClients for measurements
 * @type {udpClientInfo[]}
 */
export var udpClients = [];
/**
 * List of UDP clients which have responded to the server recently.
 * @see alivePing for more info and usage of this list
 * @type {udpClientInfo[]}
 */
export var aliveUdpClients =  [];
/**
 * List of sensor nodes which were logged in during the current measurement cycle.
 * The sensor nodes are added to {@link udpClients} once the measurement cycle is completed.
 * <br>
 * This prevents waiting for data from UDP clients
 * which did not receive the message requesting measurement data.
 * @type {udpClientInfo[]}
 */
export let newUDPClients = [];

/**
 * Information on the currently running calibration (if one is running).
 * @type {{request: {null} | Object<messageId: string, sensorknots: int[]>, responses: Object<status: string, data: string, mac: string>[], notSent: int[], socket: WebSocket, timeoutId: int}}
 */
export let currentCalibrationInfo = {
    socket: null,
    request: null,
    responses: [],
    notSent: [],
    timeoutId: -1
};


export function init() {
    if (UDP_SERVER == null) {
        UDP_SERVER = UDP.createSocket("udp4");
        UDP_SERVER.on("message", function(msg, info) {
            onUdpMessageReceived(msg, info);
        });

        UDP_SERVER.on("listening",function(){
            const address = UDP_SERVER.address();
            const port = address.port;
            const family = address.family;
            const ipaddr = address.address;
            console.log("Netzwerkinformationen zum Konfigurieren der Sensorknoten:")
            console.log(" - Die IP-Adresse des Servers ist " + ipaddr);
            console.log(" - Der Server verwendet IP4/IP6 : " + family);
            console.log(" - Der Server hört auf Port " + port);
        });

        UDP_SERVER.on("error",function(error){
            console.error("UDP Server Error: " + error);
            UDP_SERVER.close();
        });

        UDP_SERVER.on("close",function(){
            console.log("Socket is closed !");
        });

        UDP_SERVER.bind(UDP_PORT, UDP_HOST);

        // start the routine checking the availability of all sensor nodes
        // which register with / login via UDP
        alivePing();
    }
}

function onUdpMessageReceived (msg, info){
    console.debug("Data received from client : " + msg.toString());
    console.debug("Received %d bytes from %s:%d\n", msg.length, info.address, info.port);
    let udpClient = {
        "port": info.port,
        "address": info.address,
        "mac": undefined,
        "id": undefined
    }
    let fData;
    try {
        fData = JSON.parse(msg.toString());
    } catch (e) {
        console.error("Data could not be parsed");
        console.error(msg.toString());
        console.error(e);
        return;
    }
    try {
        handleMessage(fData, udpClient);
    } catch (e) {
        console.error("Error while processing UDP message");
        console.error(e);
    }
}

function getSensorMac(id) {
    return udpClients.filter(c => c.id === id)[0];
}

function getSensorId(mac) {
    const clientFromList = udpClients.filter(c => c.mac === mac);
    if (clientFromList.length !== 0) {
        return clientFromList[0].id;
    }
    return DB.getSensorID(mac);
}

/**
 * Check if sensors responded to the calibration request and provide this data to the frontend.
 */
export function checkCalibrationStates() {
    if (currentCalibrationInfo.request == null) {
        // calibration states were already checked or something went wrong
        return;
    }
    // ensure that the method is not triggered again once the timeout completes
    clearTimeout(currentCalibrationInfo.timeoutId);

    let ids = currentCalibrationInfo.request.sensorknots;
    let errors = [];

    // calibration request was not sent to sensor
    currentCalibrationInfo.notSent.forEach((id) => {
        errors.push({
            id: id,
            mac: getSensorMac(id),
            cause: "Konnte Sensor nicht erreichen. Keine Aufforderung zur Kalibrierung gesendet."
        });
        ids = ids.filter(i => i !== id);
    })

    // handle sensor responses
    for (let i = 0; i < currentCalibrationInfo.responses.length; i++) {
        const info = currentCalibrationInfo.responses[i];
        const id = getSensorId(info.mac);
        ids = ids.filter(i => i !== id);
        if (info.status === "error") {
            errors.push({
                mac: info.mac,
                id: id,
                cause: info.data
            })
        }
    }

    // handle sensors which responses were not received or sent
    for (let i = 0; i < ids.length; i++) {
        errors.push({
            id: ids[i],
            mac: getSensorMac(ids[i]),
            cause: "Keine Rückmeldung des Sensors über eine erfolgreiche oder fehlerhafte Kalibrierung"
        })
    }

    let respObj = {
        event: "response",
        messageId: currentCalibrationInfo.request.messageId
    }
    if (errors.length === 0) {
        respObj.status = "ok";
    } else {
        respObj.status = "error";
        respObj.data = errors;
    }

    currentCalibrationInfo.socket.send(JSON.stringify(respObj));
}

function handleMessage(fData, udpClient) {
    if (fData.hasOwnProperty("event")) {
        if (fData.event === "login") {
            udpClient.mac = fData.mac;
            udpClient.id = DB.getSensorID(fData.mac);
            udpClient.LED = currentLEDState;
            udpClient.fineDustSensor = currentFineDustSensorState;
            fData.id = udpClient.id
            const loggedInMessage = {
                event: "response",
                messageId: "login",
                LED: udpClient.LED,
                fineDustSensor: udpClient.fineDustSensor
            }
            sendToUdpClient(loggedInMessage, udpClient);
            if (!isUdpClientInList(udpClients, udpClient)
                && !isUdpClientInList(newUDPClients, udpClient)) {
                if (waitingForMeasurementCycleData) {
                    // ensure that the server is not waiting for sensor nodes
                    // which were logged in after the measurement cycle was started
                    newUDPClients.push(udpClient);
                } else {
                    udpClients.push(udpClient);
                    addMissingSensorNodeToMeasurement(udpClient);
                }
            }
            // A client is active when it is sending a login message.
            // Therefore, add it to the list of alive clients to prevent logging it out accidentally.
            aliveUdpClients.push(udpClient);

            // notify the clients about the newly logged in sensor node
            const loginMessage = {
                event: "login",
                mac: fData.mac,
                id: fData.id,
                battery: fData.battery,
                LED: udpClient.LED,
                fineDustSensor: udpClient.fineDustSensor
            }
            broadcastToWebclients(JSON.stringify(loginMessage));
        } else if (fData.event === "alive") {
            const client = getUdpClient(DB.getSensorID(fData.mac));
            client.battery = fData.battery;
            aliveUdpClients.push(client);
        } else if (fData.event === "forcedCalibration") {
            console.info(`Sensor ${fData.mac} kalibriert; status ${fData.status}`)
            currentCalibrationInfo.responses.push(fData);
            if (currentCalibrationInfo.responses.length ===
                currentCalibrationInfo.request.sensorknots.length + currentCalibrationInfo.notSent.length) {
                checkCalibrationStates();
            }
        }
    } else if (fData.hasOwnProperty("co2")) { // TODO: refactor this message to use an event as well
        // forwarding message to webapp (timeout for incomplete data)
        dLog("Messdaten erhalten");
        onMeasurementDateReceived(fData);
    }
}

export function addSensorsHeldBack() {
    // add sensor nodes which connected to the server during the measurement cycle
    udpClients = udpClients.concat(newUDPClients);
    for (const newUDPClient of newUDPClients) {
        addMissingSensorNodeToMeasurement(newUDPClient);
    }
    newUDPClients = [];
}

/**
 * Checks whether a client is in a <code>udpClientInfo</code> list.
 * Checks whether <code>port</code> and <code>address</code> properties are identical.
 * By default, the <code>mac</code> property is also compared.
 * @param list {udpClientInfo[]}
 * @param client {udpClientInfo}
 * @param checkMac {boolean} Whether the <code>mac</code> property is also compared
 * @return bool returns <code>true</code> if there is a client in the list
 * which has the same <code>mac</code>, <code>port</code> and optionally <code>address</code>;
 * returns <code>false</code> otherwise.
 */
export function isUdpClientInList(list, client, checkMac = true) {
    if (checkMac) {
        return list.some(
            c => c.mac === client.mac && c.address === client.address && c.port === client.port
        );
    } else {
        return list.some(
            c => c.address === client.address && c.port === client.port
        );
    }
}

/**
 * Broadcast a message to all connected UDP clients / sensor nodes
 * @param msg {Buffer | string | Uint8Array | ReadonlyArray<any> | Object}
 */
export function udpBroadcast(msg) {
    for (let udpClient of udpClients) {
        sendToUdpClient(msg, udpClient);
    }
}

/**
 * Send a message to a specific sensor
 * @param {int} id
 * @param {Buffer | string | Uint8Array | ReadonlyArray<any> | JSON | Object} msg
 */
export function sendToSensor(id, msg) {
    sendToUdpClient(msg, udpClients.find((c) => c.id === id));
}

/**
 * @param msg {Buffer | string | Uint8Array | ReadonlyArray<any> | JSON | Object}
 * @param udpClient {udpClientInfo}
 */
function sendToUdpClient(msg, udpClient) {
    if (msg instanceof Object) {
        msg = JSON.stringify(msg);
    }
    UDP_SERVER.send(msg, udpClient.port, udpClient.address);
}

/**
 * @param {int} id
 * @return {udpClientInfo}
 */
export function getUdpClient(id) {
    return udpClients.find((c) => c.id == id);
}

/**
 * Clock which emits alive pings to UDP clients
 * while {@link ALIVE_PING_ENABLE} is <code>true</code>,
 * i.e. while the measurement has not been started yet.
 * <p>
 * Alive pings expect an answer from the clients.
 * If a client does not answer to two consecutive pings,
 * the UDP client is considered to be turned off and is removed.
 * The <code>logout</code> event is sent to the frontend for the corresponding UDP client.
 * @return void
 */
async function alivePing() {
    while (SERVER_CONF.ALIVE_PING_ENABLE) {
        await sleep(SERVER_CONF.ALIVE_INTERVAL);
        if (STATE === SERVER_STATE.IDLE && udpClients.length !== 0) {
            dLog("NEW ALIVE PING CYCLE");
            dLog("------------------------------------------");
            // clear the list of alive clients
            aliveUdpClients = []

            let udpFetch = Object.assign([], udpClients);
            const alivePingMsg = {
                "event": "aliveping"
            }
            // Send alive ping and wait for a MEASUREMENT_INTERVAL two times.
            // If there is no answer after two pings,
            // assume that there will be no response in the future.
            // Therefore, remove the client from the list of the active UDP clients
            for (let i = 0; i < 2; i++) {
                // send alive ping to sensor nodes
                dLog("alive ping sent");
                udpBroadcast(JSON.stringify(alivePingMsg));
                // wait
                await sleep(SERVER_CONF.MEASUREMENT_INTERVAL);
            }


            const batteryStatusMsg = {
                event: "batteryStatus",
                sensorknots: []
            }

            // remove inactive nodes from client array and send battery status for all active nodes
            for (let i = 0; i < udpFetch.length; i++) {
                if (aliveUdpClients.some(node => node.port == udpFetch[i].port)) {
                    batteryStatusMsg.sensorknots.push({
                        mac: udpClients[i].mac,
                        id: udpClients[i].id,
                        battery: udpClients[i].battery
                    })
                } else {
                    for (let j = 0; j < udpClients.length; j++) {
                        if (udpClients[j].address === udpFetch[i].address && udpClients[j].port == udpFetch[i].port) {
                            console.info("REMOVING node with mac ", udpClients[j].mac)
                            console.info("REMOVED connection with address and port ", udpFetch[i].address, ":", udpFetch[i].port)
                            let logoutMsg = {
                                event: "logout",
                                mac: udpClients[j].mac,
                                id: udpClients[j].id
                            }
                            broadcastToWebclients(JSON.stringify(logoutMsg))
                            udpClients.splice(j, 1);
                        }
                    }
                }
            }
            if (batteryStatusMsg.sensorknots.length > 0) {
                broadcastToWebclients(batteryStatusMsg);
            }
        }
    }
}

export function resetCurrentCalibrationInfo(socket, data) {
    currentCalibrationInfo = {
        socket: socket,
        request: data,
        responses: [],
        notSent: []
    }
}