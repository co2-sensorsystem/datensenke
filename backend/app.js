//////////////////////////////////////////////////////////////////////////////////////////////////
//// NODE.JS backend file
// / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /
// This file is run by the NPM command
//      npm run {test | all}
// and initializes the actual controller.
// This is necessary to use ES6 features inside Node.js and within the controller.
//////////////////////////////////////////////////////////////////////////////////////////////////

require = require("esm")(module);
const Controller = require("./controller");
Controller.init();
