import * as DB from "./database";
// Import the other modules after declaring everything inside the controller module.
// This is necessary, because many variables and functions declared in this module
// are used by the one imported below during initialization or shortly after that
import * as SensorNodes from "./sensorNodeController";
import * as FrontendInterface from "./frontendInterface.js";


let initialized = false;

export var SERVER_STATE = {
    IDLE: 0,
    MEASUREMENT: 1
}

export var STATE = SERVER_STATE.IDLE;
export function setState(state) {
    STATE = state;
}

/**
 * Get a list of all stored rooms.
 * @return {Array|-1} The Room array or <code>-1</code> if there was an error.
 */
export function getListOfStoredRooms() {
    let rooms = DB.getListOfRooms();
    if (rooms === -1) {
        return -1;
    }
    rooms.forEach(room => {
        if (room.image != null) {
            room.image = "rooms/" + room.image
        }
    });
    return rooms;
}

/**
 * Initialize the backend controller.
 * During this initialization, the other modules are initialized as well.
 */
export function init() {
    if (!initialized) {
        DB.createDB();
        SensorNodes.init();
        FrontendInterface.init();
    }
}