const Database = require('better-sqlite3');
const Constants = require('../frontend/classes');
const CONFIG = require("../frontend/config.js");

/**
 * Sanitize all string properties of an Object.
 * This function is recursive and sanitizes all properties on all levels.
 * @param {Object} unsafeObj
 * @return {Object}
 */
function escapeObj(unsafeObj) {
    for (let key in Object.keys(unsafeObj)) {
        if ("object" == typeof unsafeObj[key]) {
            unsafeObj[key] = escapeObj(unsafeObj[key]);
        } else if ("string" == typeof unsafeObj[key]) {
            unsafeObj[key] = escape(unsafeObj[key]);
        }
    }
    return unsafeObj;
}

class Room {
    constructor(id, name, width, length, height, volume, image) {
        this.id = id;
        this.name = name;
        this.width = width;
        this.length = length;
        this.height = height;
        this.volume = volume;
        this.image = image;
    }
}

class RoomElement {
    /**
     *
     * @param {int} roomId
     * @param {string} objectId
     * @param {int} type
     * @param {Object} data
     * @param {Coordinate} position
     */
    constructor(roomId, objectId, type, data, position) {
        this.roomId = roomId;
        this.objectId = objectId;
        this.type = type;
        this.data = data;
        this.position = position;
    }
}

class Position {
    constructor(x,y) {
        this.x = x;
        this.y = y;
    }
}
class Dataset {
    constructor(id, co2, humidity, temperature, pm25 = Constants.NO_DATA, pm10 = Constants.NO_DATA, battery) {
        this.id = id;
        this.co2 = co2;
        this.humidity = humidity;
        this.temperature = temperature;
        this.pm25 = pm25;
        this.pm10 = pm10;
        this.battery = battery;
    }
}

class ProtocolEntry {
    constructor(measurement, timestamp, type, data) {
        this.measurement = measurement;
        this.timestamp = timestamp;
        this.type = type;
        this.data = data;
    }
}

class ObjectConfiguration {
    constructor(measurementId, objectId, type, data, x, y) {
        this.measurementId = measurementId;
        this.objectId = objectId;
        this.type = type;
        this.data = data;
        this.x = x;
        this.y = y;
    }
}

const insertRoomElementQuery = `INSERT INTO "Room_Configuration"("room_id", "object_id", "type", "data", "position_x", "position_y") VALUES (?, ?, ?, ?, ?, ?)`;

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isValidRoomObject(roomObject) {
    return roomObject.hasOwnProperty("name") && typeof(roomObject.name) == "string"
        && roomObject.hasOwnProperty("length") && isNumeric(roomObject.length)
        && roomObject.hasOwnProperty("width") && isNumeric(roomObject.width)
        && roomObject.hasOwnProperty("height") && isNumeric(roomObject.height)
        && roomObject.hasOwnProperty("volume") && isNumeric(roomObject.volume);
}

function connect() {
    return new Database('./backend/co2.db');
}

function loadInitialRooms() {
    const fs = require('fs');

    try {

        const data = fs.readFileSync('./rooms.json', 'utf8');

        // parse JSON string to JSON object
        const roomEntries = JSON.parse(data);

        // insert all rooms
        roomEntries.forEach(room => {
            if(isValidRoomObject(room)) {
                addRoom(room, true);
            } else {
                console.log(`Invalid room entry in rooms.json`);
            }
        });

    } catch (err) {
        console.log(`Error reading rooms.json from disk: ${err}`);
    }
}

/**
 * creates a sqlite database and all necessary tables if they don't already exist
 *
 */
export function createDB() {
    let db;
    try {
        db = connect();

        const createMeasurementStmt = db.prepare(
            `CREATE TABLE IF NOT EXISTS "Measurement"
             (
                 "measurement_id" INTEGER NOT NULL,
                 "name"           TEXT    NOT NULL,
                 "room_id"        INTEGER NOT NULL,
                 "start_time"     INTEGER NOT NULL,
                 PRIMARY KEY ("measurement_id" AUTOINCREMENT),
                 FOREIGN KEY ("room_id") REFERENCES "Room" ("room_id")
             );`);
        const createMeasurementDataStmt = db.prepare(
            `CREATE TABLE IF NOT EXISTS "Measurement_Data"
             (
                 "sensor_id"      INTEGER NOT NULL,
                 "timestamp"      INTEGER NOT NULL,
                 "measurement_id" INTEGER NOT NULL,
                 "co2"            REAL    NOT NULL,
                 "humidity"       REAL    NOT NULL,
                 "temperature"    REAL    NOT NULL, 
                 "pm25"           REAL    NOT NULL DEFAULT -9999,
                 "pm10"           REAL    NOT NULL DEFAULT -9999,
                 "battery"        REAL    NOT NULL DEFAULT -9999,
                 PRIMARY KEY ("sensor_id", "timestamp", "measurement_id"),
                 FOREIGN KEY ("sensor_id") REFERENCES "Sensor" ("sensor_id")
             );`);
        const createObjectConfigStmt = db.prepare(
            `CREATE TABLE IF NOT EXISTS "Measurement_Configuration"
             (
                 "object_id"      INTEGER NOT NULL,
                 "measurement_id" INTEGER NOT NULL,
                 "position_x"     REAL    NOT NULL,
                 "position_y"     REAL    NOT NULL,
                 "type"           INTEGER NOT NULL,
                 "data"           TEXT,  
                 PRIMARY KEY ("object_id", measurement_id),
                 FOREIGN KEY ("measurement_id") REFERENCES "Measurement" ("measurement_id")
             )`);

        const createSensorMacsStmt = db.prepare(
            `CREATE TABLE IF NOT EXISTS "Sensor"
             (
                 "sensor_id"  INTEGER NOT NULL,
                 "mac"        TEXT    NOT NULL UNIQUE,
                 PRIMARY KEY ("sensor_id" AUTOINCREMENT)
             )`);

        const createRoomStmt = db.prepare(
            `CREATE TABLE IF NOT EXISTS "Room"
             (
                 "room_id" INTEGER NOT NULL,
                 "name"    TEXT    NOT NULL UNIQUE ,
                 "width"   REAL    NOT NULL,
                 "length"  REAL    NOT NULL,
                 "height"  REAL    NOT NULL,
                 "volume"  REAL    NOT NULL,
                 "image"   TEXT,
                 PRIMARY KEY ("room_id" AUTOINCREMENT)
             )`);

        const createRoomConfigStmt = db.prepare(`
            CREATE TABLE IF NOT EXISTS "Room_Configuration"(
                "room_id"    INTEGER NOT NULL,
                "object_id"  TEXT    NOT  NULL,
                "type"       INTEGER NOT NULL,
                "data"       TEXT,
                "position_x" REAL    NOT NULL,
                "position_y" REAL    NOT NULL,
                PRIMARY KEY ("room_id", "object_id"),
                FOREIGN KEY ("room_id") REFERENCES "Room" ("room_id")
            )`);

        const createProtocolStmt = db.prepare(
            `CREATE TABLE IF NOT EXISTS "Protocol"
            (
                "measurement_id" INTEGER NOT NULL,
                "timestamp"      INTEGER NOT NULL,
                "object_id"      TEXT,
                "type"           INTEGER NOT NULL,
                "data"           TEXT,
                PRIMARY KEY ("measurement_id", "timestamp", "object_id"),
                FOREIGN KEY ("measurement_id") REFERENCES "Measurement" ("measurement_id")
            )`);

        //create transaction function
        const transaction = db.transaction(() => {
            createRoomStmt.run();
            createMeasurementStmt.run();
            createProtocolStmt.run();
            createObjectConfigStmt.run();
            createSensorMacsStmt.run();
            createMeasurementDataStmt.run();
            createRoomConfigStmt.run();
        });
        transaction();

        loadInitialRooms();
    } catch (e) {
        console.error("Could not initialize database");
        throw e;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }

}

/**
 * returns a list of all measurements already created
 *
 * @returns {array} listOfMeasurements
 * [
 *      {
 *          id,
 *          name,
 *          timestamp
 *      }
 * ]
 */
export function getListOfMeasurements() {
    let db;
    try {
        db = connect();
        const getListStmt = db.prepare(`
            SELECT "Measurement"."measurement_id" AS "id",
                   "Measurement"."name"           AS "name",
                   "Measurement"."start_time"     AS "timestamp",
                   "Measurement".room_id          AS "room"
            FROM "Measurement"`);

        return getListStmt.all();
    } catch (e) {
        console.log(e);
        return [];
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * returns a list of all created rooms
 * @returns {array | -1}
 * [
 *      {
 *          id,
 *          name,
 *          width,
 *          length
 *      }
 * ]
 */
export function getListOfRooms() {
    let db;
    try {
        db = connect();
        const getListStmt = db.prepare(`
            SELECT "Room"."room_id" AS "id",
                   "Room"."name"    AS "name",
                   "Room"."width"   AS "width",
                   "Room"."length"  AS "length",
                   "Room"."height"  AS "height",
                   "Room"."volume"  AS "volume",
                   "Room"."image"   AS "image"
            FROM "Room";`)
        const getRoomElementsStmt = db.prepare(`SELECT * FROM Room_Configuration`);

        const roomList = getListStmt.all();
        // add room elements to the corresponding room
        roomList.forEach((r) => r.elements = []);
        const els = getRoomElementsStmt.all();
        els.forEach((el) => {
            const room = roomList.find((r) => r.id === el["room_id"]);
            room.elements.push(
                    new RoomElement(room.id, el["object_id"], el.type, JSON.parse(el.data),
                        {x: el["position_x"], y: el["position_y"]}))
        });
        return roomList;
    } catch (e) {
        console.log(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * loads a complete measurement
 *
 * @param {number} measurementID
 *
 * @returns {Measurement}
 */
export function getMeasurement(measurementID) {
    let result = {};
    let db;
    try {
        db = connect();
        const getMeasurementStmt = db.prepare(`
            SELECT "Measurement"."measurement_id" AS "id",
                   "Measurement"."name"           AS "name",
                   "Measurement"."start_time"     AS "timestamp",
                   "Room"."room_id"               AS "roomID",
                   "Room"."name"                  AS "roomName",
                   "Room"."width"                 AS "roomWidth",
                   "Room"."length"                AS "roomLength",
                   "Room"."height"                AS "roomHeight",
                   "Room"."volume"                AS "roomVolume",
                   "Room"."image"                 AS "roomImage"
            FROM "Measurement"
                     INNER JOIN "Room" ON "Measurement"."room_id" = "Room"."room_id"
            WHERE "measurement_id" = ?`);

        const getObjectConfigStmt = db.prepare(`
            SELECT Measurement_Configuration."object_id" AS "id",
                   Measurement_Configuration."position_x"  AS "x",
                   Measurement_Configuration."position_y"  AS "y",
                   Measurement_Configuration."type" AS "type",
                   Measurement_Configuration."data" AS "data",
                   "Sensor".mac           AS "mac"
            FROM Measurement_Configuration
                     LEFT JOIN Sensor ON Measurement_Configuration."object_id" = "Sensor"."sensor_id"
            WHERE "measurement_id" = ?`);

        const getMeasurementDataStmt = db.prepare(`
            SELECT "timestamp",
                   "co2",
                   "humidity",
                   "temperature",
                   "pm25",
                   "pm10",
                   "battery"
            FROM "Measurement_Data"
            WHERE "measurement_id" = ?
              AND "sensor_id" = ?
            ORDER BY "timestamp"`);

        const getProtocolEntries = db.prepare(`
            SELECT "timestamp",
                   "object_id" AS objectId,
                   "type",
                   "data"
            FROM Protocol
            WHERE "measurement_id" = ?
        `);

        //create transaction function
        const transaction = db.transaction(() => {
            let measurement = getMeasurementStmt.get(measurementID);

            if (measurement !== undefined) {
                //prepare result object
                result = {
                    timestamp: measurement.timestamp,
                    id: measurement.id,
                    name: measurement.name,
                    room: new Room(
                        measurement.roomID, measurement.roomName,
                        measurement.roomWidth, measurement.roomLength, measurement.roomHeight,
                        measurement.roomVolume, measurement.roomImage),
                    objects: [],
                    data: {},
                    events: []
                }

                //get sensor configs
                let objectConfigs = getObjectConfigStmt.all(measurementID);
                //if objects in measurement (should always be true,
                // because sensors are required to start a measurement)
                if (objectConfigs.length) {
                    // get values for each object
                    for (let objectConfig of objectConfigs) {
                        let parsedConfig = {
                            id: objectConfig.id,
                            type: objectConfig.type,
                            position: new Position(objectConfig.x, objectConfig.y),
                            data: {}
                        };
                        if (objectConfig.type === CONFIG.OBJECT_TYPE.SENSOR) {
                            // Query the measured values for each sensor
                            // FIXME: make this a separate query which is more efficient.
                            parsedConfig.mac = objectConfig.mac;
                            objectConfig.data = JSON.parse(objectConfig.data);
                            parsedConfig.height = objectConfig.data.height;
                            parsedConfig.note = objectConfig.data.note;
                            result.objects.push(parsedConfig);
                            //get sensor data
                            let sensorValues = getMeasurementDataStmt.all(measurementID, objectConfig.id);
                            if (sensorValues.length) {
                                for (let dataset of sensorValues) {
                                    if (result.data[dataset.timestamp] === undefined) {
                                        result.data[dataset.timestamp] = []
                                    }
                                    result.data[dataset.timestamp].push(
                                        new Dataset(
                                            objectConfig.id,
                                            dataset.co2,
                                            dataset.humidity,
                                            dataset.temperature,
                                            dataset.pm25,
                                            dataset.pm10,
                                            dataset.battery)
                                    );
                                }
                            }
                        } else {
                            parsedConfig.type = objectConfig.type;
                            result.objects.push(parsedConfig);
                            parsedConfig.data = objectConfig.data;
                        }
                    }
                }

                result.events = getProtocolEntries.all(measurementID);

            } else {
                result = {
                    id : measurementID,
                    error: "Messung wurde nicht gefunden"
                };
            }
        });

        transaction();
        return result;
    } catch (e) {
        console.log(e);
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * creates a measurement with the information about the measurement and the configuration of the sensor nodes
 * @param {object} configObj : {
 *     id,
 *     name,
 *     objects [
 *         {
 *             id,
 *             type,
 *             position: {
 *                 x,
 *                 y
 *             }
 *             data {}
 *         }
 *     ],
 *     room {
 *         id,
 *      OR
 *         name,
 *         width,
 *         length
 *     }
 * }
 * @returns {object} result
 *                   result.error,
 *                   result.measurementID
 *
 *
 */
export function addMeasurement(config) {
    let result = {};
    let db;
    try {
        db = connect();
        const insertMeasurementStmt = db.prepare(`
            INSERT INTO "Measurement"("name",
                                      "start_time",
                                      "room_id")
            VALUES (?, ?, ?)`);

        const insertObjectConfigurationStmt = db.prepare(`
            INSERT INTO Measurement_Configuration("object_id",
                                               "measurement_id",
                                               "position_x",
                                               "position_y",
                                               "type",
                                               "data")
            VALUES (?, ?, ?, ?, ?, ?)`);

        //create transaction function
        const transaction = db.transaction(() => {
            let roomID;
            //if the room already exists in the database | else use name/width/length to create a new room
            if (config.room.hasOwnProperty("id")) {
                roomID = config.room.id;
            } else {
                roomID = this.addRoom(
                    new Room(-1, config.room.name, config.room.width, config.room.length,
                        config.room.height, config.room.volume, null)
                );

            }
            // if a problem appeared while creating room
            if (roomID < 0) {
                result.error = "Raum konnte nicht angelegt werden, vermutlich existiert er bereits";
                return;
            }

            // create measurement entry and get measurement ID
            const insertMeasurementRslt = insertMeasurementStmt.run(config.name, Date.now(), roomID);
            result.measurementID = insertMeasurementRslt.lastInsertRowid;

            // Insert objects like air filters, windows and sensors.
            // Sensors do not need to be added to the Sensor table,
            // because they must be logged in (and thus present in that table).
            for (let object of config.objects) {
                insertObjectConfigurationStmt.run(
                    object.id, result.measurementID,
                    object.position.x, object.position.y,
                    object.type, JSON.stringify(object.data));
            }

        });
        transaction();
        return result;
    } catch (e) {
        console.log(e);
        result.error = "Beim Anlegen der Messung ist ein Fehler aufgetreten";
        return result;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * Add a room and its elements (windows, doors) to the database.
 * @param {Room} roomConfig
 * @param {null} roomConfig.id - room id is going to be generated
 * @param {RoomElement[]} roomConfig.elements
 * @param {null} roomConfig.elements[].id - room id is going to be generated
 * @param {boolean} [ignoreError] whether to not insert the room if a room with the same name
 * already exists in the DB. Default value: <code>false</code>
 * @returns {number} the room id
 *
 *
 */
export function addRoom(roomConfig, ignoreError = false) {
    let db;
    try {
        db = connect();
        const insertRoomStmt = db.prepare(`
                INSERT ${ignoreError ? 'OR IGNORE' : ''}
                INTO "Room"("name", "width", "length", "height", "volume", "image")
                VALUES (?, ?, ?, ?, ?, ?)`);
        const insertRoomElementStmt = db.prepare(insertRoomElementQuery);

        let roomImage = null;
        if (roomConfig.hasOwnProperty("image") && roomConfig.image != null) {
            roomImage = roomConfig.image;
        }

        //create transaction function
        const transaction = db.transaction(() => {
            let result = insertRoomStmt.run(
                roomConfig.name, roomConfig.width, roomConfig.length,
                roomConfig.height, roomConfig.volume, roomImage);
            const roomId = result.lastInsertRowid;
            if (roomConfig.elements != null) {
                roomConfig.elements.forEach((el) => {
                    insertRoomElementStmt.run(
                        roomId, el.id || el.objectId, el.type, JSON.stringify(el.data), el.position.x, el.position.y);
                });
            }
            return roomId;
        });
        return transaction();
    } catch (e) {
        console.error("Could not add room");
        if (e.toString().includes("UNIQUE constraint failed: Room.name")) {
            return -2;
        }
        console.log(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

export function updateRoom(room) {
    let db;
    try {
        db = connect();
        const updateRoomStmt = db.prepare(
            `UPDATE "Room" SET "name" = ?, "width" = ?, "length" = ?, "height" = ?,
                                   "volume" = ?, "image" = ?
                               WHERE "room_id" = ? `);

        const removeRoomElementsStmt = db.prepare(
            'DELETE FROM "Room_Configuration" WHERE room_id = ?');
        const insertRoomElementStmt = db.prepare(insertRoomElementQuery);
        let roomImage = null;
        if (room.hasOwnProperty("image") && room.image != null) {
            roomImage = room.image;
        }

        //create transaction function
        const transaction = db.transaction(() => {
           updateRoomStmt.run(
                room.name, room.width, room.length,
                room.height, room.volume, roomImage, room.id);
            removeRoomElementsStmt.run(room.id);
            if (room.elements != null) {
                room.elements.forEach((el) => {
                    console.log(room.id, el.id, el.type, el.data, el.position.x, el.position.y);
                    insertRoomElementStmt.run(
                        room.id, el.id, el.type, JSON.stringify(el.data), el.position.x, el.position.y);
                });
            }
            return 0;
        });
        return transaction();
    } catch (e) {
        console.error("Could not update room");
        console.log(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

export function updateRoomImage(roomId, image) {
    let db;
    try {
        db = connect();
        const updateRoomStmt = db.prepare(
            `UPDATE "Room"
             SET "image" = ?
             WHERE "room_id" = ? `);
        const transaction = db.transaction(() => {
            updateRoomStmt.run(image, roomId);
        });
        transaction();
        return 0;
    } catch (e) {
        console.log("Could not update room image for room " + roomId);
        console.log(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * @returns {number} 0 or -1 on Error
 * @param {number} measurementID
 * @param {object} measurementDataObj : {
 *     timestamp,
 *     sensorknots [
 *         {
 *             id,
 *             co2,
 *             humidity,
 *             temperature
 *         }
 *     ]
 * }
 *
 */
export function addValuesToMeasurement(measurementID, measurementDataObj) {
    let db;
    try {
        db = connect();
        const insertDataStmt = db.prepare(`
            INSERT INTO "Measurement_Data"("sensor_id",
                                           "timestamp",
                                           "measurement_id",
                                           "co2",
                                           "humidity",
                                           "temperature",
                                           "pm25",
                                           "pm10",
                                           "battery")
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`);

        //create transaction function
        const transaction = db.transaction(() => {
            for (let sensorknot of measurementDataObj.sensorknots) {
                insertDataStmt.run(
                    sensorknot.id, measurementDataObj.timestamp, measurementID,
                    sensorknot.co2, sensorknot.humidity, sensorknot.temperature,
                    sensorknot.pm25 || Constants.NO_DATA, sensorknot.pm10 || Constants.NO_DATA,
                    sensorknot.battery || Constants.NO_DATA);
            }
        });

        transaction();
        return 0;
    } catch (e) {
        console.error("Could not add values to measurement")
        console.error(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * returns the associated ID for a Mac address, if there is no ID, a new one is created.
 * @param {string} macAddress
 * @returns {number} sensorID
 */
export function getSensorID(macAddress) {
    let db;
    try {
        db = connect();
        const selectMacStmt = db.prepare(`
            SELECT "sensor_id" AS "sensorID"
            FROM "Sensor"
            WHERE "mac" = ?`);
        const insertMacStmt = db.prepare(`
            INSERT INTO "Sensor"(mac)
            VALUES (?)`);

        //create transaction function
        const transaction = db.transaction(() => {
            //if Sensor exists, return id
            let result = selectMacStmt.get(macAddress);
            if (result === undefined) {
                //if Sensor does not exist, return new id
                let insertResult = insertMacStmt.run(macAddress);
                return insertResult.lastInsertRowid;
            } else {
                return result.sensorID;
            }
        });

        return transaction();
    } catch (e) {
        console.error("Could not get sensor id")
        console.error(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 *
 * @param id
 * @return {null|string}
 */
export function getSensorMac(id) {
    let db;
    try {
        db = connect();
        const selectMacStmt = db.prepare(`
            SELECT "mac" AS "mac"
            FROM "Sensor"
            WHERE "sensor_id" = ?`);

        //create transaction function
        const transaction = db.transaction(() => {
            //if Sensor exists, return id
            let result = selectMacStmt.get(id);
            return result.mac;

        });

        return transaction();
    } catch (e) {
        console.error("Could not get sensor mac")
        console.error(e);
        return null;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

export function deleteMeasurement(id) {
    let db;
    try {
        db = connect();
        const deleteProtocol = db.prepare(`
            DELETE FROM Protocol
            WHERE measurement_id = ?`);
        const deleteConfig = db.prepare(`
            DELETE FROM Measurement_Configuration
            WHERE "measurement_id" = ?`);
        const deleteData = db.prepare(`
            DELETE FROM "Measurement_Data"
            WHERE "measurement_id" = ?`);
        const deleteMeasurement = db.prepare(`
            DELETE FROM "Measurement"
            WHERE "measurement_id" = ?`);

        const transaction = db.transaction(() => {
            deleteProtocol.run(id);
            deleteConfig.run(id);
            deleteData.run(id);
            deleteMeasurement.run(id);
        });
        return transaction();
    } catch (e) {
        console.log("Could not delete measurement, id: %s", id);
        console.log(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

/**
 * @param {Object} entry
 * @param {int} entry.measurement
 * @param {int} entry.timestamp
 * @param {string | null} entry.id
 * @param {int} entry.type
 * @param {Object} entry.data
 * @return {number|*}
 */
export function addProtocolEntry(entry) {
    let db;
    try {
        db = connect();
        const insertEntryStmt = db.prepare(
            `INSERT INTO Protocol("measurement_id", "timestamp", "object_id", "type", "data")
                VALUES (?, ?, ?, ?, ?)`);

        const transaction = db.transaction(() => {
            const result = insertEntryStmt.run(
                entry.measurement, entry.timestamp, entry.id, entry.type, JSON.stringify(entry.data));
            return result.lastInsertRowid;
        });
        transaction();
        return 0;
    } catch (e) {
        console.log(e);
        return -1;
    } finally {
        if (db !== undefined && db.open) {
            db.close();
        }
    }
}

