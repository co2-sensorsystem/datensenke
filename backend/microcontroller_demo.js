const UDP = require('dgram');
const IP = require('ip');
const HOST = IP.address();
const PORT = 33333
const Client = UDP.createSocket('udp4');
let id = ((Math.random() * 10000) >> 0).toString();
const isUsbPowered = Math.random() < 0.15;
let lastBatteryVoltage = 5.0;
let lastCo2 = -1;

Client.on('message',function(msg,info){
  try {
    const fData = JSON.parse(msg.toString());
    let response = {};
    if (fData.event === "startMeasurement") {
      console.log('Data received from server : ' + msg.toString());
      console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);
      response = {
        mac: id,
        battery: getBattery(),
        co2: getCo2(),
        humidity: (Math.random() * 100) >> 0,
        temperature: 5 + (Math.random() * 20) >> 0,
        pm25: parseFloat((Math.random() * 30).toFixed(1)),
        pm10: parseFloat((Math.random() * 60).toFixed(1)),
        timestamp: fData.timestamp
      }

    } else if (fData.event === "aliveping") {
      response = {
        event: "alive",
        mac: id,
        battery: getBattery()
      }
    } else if (fData.event === "phantomMode") {
      console.log("changing LED");
      return
    }
    if (Math.random() < 0.95 || fData.event === "aliveping") {
      // sometimes data can slip through
      sendMessage(JSON.stringify(response));
    }
  } catch (e) {
    console.log("Data could not be parsed");
  }
});

function sendMessage(data) {
  Client.send(data, PORT, HOST, function(error){
    if (error) {
      Client.close();
    } else {
      console.log('Data sent: ', data);
    }
  });
}


setTimeout(function () {
  sendMessage(Buffer.from('{"event": "login", "mac": "' + id + '", "battery": ' + getBattery() + '}'))
}, 2000);

function getBattery() {
  let change = Math.random();
  const sign = Math.random() < 0.5;
  const newBatteryVoltage = lastBatteryVoltage + (sign ? 1 : -1) * change;
  if (isUsbPowered) {
    lastBatteryVoltage = Math.max(Math.min(newBatteryVoltage, 6.), 4.8)
  } else {
    lastBatteryVoltage = Math.max(Math.min(newBatteryVoltage, 3), 0.05)
  }
  return lastBatteryVoltage;
}

function getCo2() {
  if (lastCo2 === -1) {
    return lastCo2 = 450 + ((Math.random() * 1200) >> 0);
  } else {
    let change;
    const prob = Math.random();
    if (prob < 0.2) {
      change = ((Math.random() * 15) >> 0);
    } else if (prob < 0.6) {
      change = ((Math.random() * 10) >> 0)
    } else {
      change = ((Math.random() * 5) >> 0)
    }
    const sign = Math.random() < 0.5;
    return lastCo2 = lastCo2 + (sign ? 1 : -1) * change;
  }
}