import {
    currentFineDustSensorState,
    currentLEDState,
    getUdpClient,
    udpClients
} from "./sensorNodeController";

let initialized = false;
const WEB_SOCKET = require("ws");
const fs = require('fs');
const CONFIG = require("../frontend/config");
import {WS_PORT, MEASUREMENT_INTERVAL} from "./config";
import * as sensorController from "./sensorNodeController";
const DB = require("./database");
import {
    getCurrentMeasurementId,
    startMeasurement, stopMeasurement
} from "./measurementController";
import {getListOfStoredRooms, STATE, SERVER_STATE} from "./controller";
import {NO_DATA, OBJECT_TYPE} from "../frontend/config";
import {getMeasurement} from "./database";
let WS_SERVER;

/**
 * @type {{extension: string, id?: int, uploadId?: int} | null}
 */
export var roomInfo;
let uploadId = 0;

export function init() {
    if (!initialized) {
        initialized = true;
        WS_SERVER = new WEB_SOCKET.Server({
            port: WS_PORT
        });
        WS_SERVER.on("connection", onWebSocketConnection);
    }
}

let webClients = [];

function onWebSocketConnection(socket) {
    // A new web client / frontend instance connected to the server.
    // Currently, only one measurement instance is supported.
    // Therefore, any other instances are set to a "blocked" state in which nothing can be done.
    console.log("New client connected:  %s", socket._socket.remoteAddress);
    let initMessage = {
        "event": "register-client",
    };
    //if (webClients.length === 0) {
    webClients.push(socket);

    // The frontend instance needs information about the server's state:
    // 1. Send a list with all available rooms
    initMessage.rooms = getListOfStoredRooms();

    // 2. Send a list of all currently connected sensor nodes
    initMessage.sensorknots = sensorController.udpClients.map(client => {
        return {id: client.id, mac: client.mac, battery: client.battery | NO_DATA, LED: client.LED, fineDustSensor: client.fineDustSensor || NO_DATA}
    });

    // 3. Send info whether a measurement is running.
    //    If a measurement is running, send the measurement data, too.
    //    This enables the frontend instance to display the running measurement instantly.
    initMessage.status = STATE === SERVER_STATE.MEASUREMENT ? "measurement" : "no-measurement";
    if (STATE === SERVER_STATE.MEASUREMENT) {
        let reqMeasurement = DB.getMeasurement(getCurrentMeasurementId())
        if (!reqMeasurement.hasOwnProperty("error")) {
            if (reqMeasurement.room instanceof Object && reqMeasurement.room.image != null) {
                reqMeasurement.room.image = "rooms/" + reqMeasurement.room.image
            }
            for (let obj of reqMeasurement.objects) {
                if (obj.type === OBJECT_TYPE.SENSOR) {
                    obj.LED = getUdpClient(obj.id).LED || currentLEDState;
                    obj.fineDustSensor = getUdpClient(obj.id).fineDustSensor || currentFineDustSensorState;
                }
            }
        }
        initMessage.measurement = reqMeasurement
    }
    //} else {
    //  initMessage.event = "register-client"
    //  initMessage.status = "blocked"
    //}
    socket.send(JSON.stringify(initMessage));

    // Register event listener for incoming messages from this frontend instance.
    socket.on("message", function(data) {
        if (data instanceof Buffer) {
            console.log("binary message received")
            handleBinaryMessage(data, socket);
        } else {
            console.log("Text message received: ", data)
            handleTextMessage(data, socket);
        }
    });

    // When a socket closes, or disconnects, remove it from the array.
    socket.on("close", function() {
        console.log("Connection closed %s", this.address);
        webClients = webClients.filter(s => s !== socket);
    });
}

/**
 * @param {Buffer} imageData first 64 bit roomId, the rest image binary
 * @param {WebSocket} socket
 */
function handleBinaryMessage(imageData, socket) {
    let resObj = {
        event: "response",
        messageId: "file",
        status: "error",
        data: {}
    }
    if (roomInfo == null) {
        resObj.data = "Keine Rauminformationen vorhanden zum Speichern des Bildes";
        socket.send(JSON.stringify(resObj));
    } else {
        const uploadIdLength = roomInfo.uploadId.length / 2; // hex conversion
        const receivedUploadId = imageData.toString('hex', 0, uploadIdLength);
        console.log("received upload id:" + receivedUploadId);
        console.log("expected upload id:" + roomInfo.uploadId);
        resObj.messageId = "file" + roomInfo.uploadId;
        console.log("image received");
        const name = `room-${roomInfo.id}.${roomInfo.extension}`;
        const path = "frontend/rooms/" + name;
        const ib = imageData.subarray(uploadIdLength)
        //const iBuf = new ArrayBuffer(imageData.byteLength - roomInfo.uploadId.length);
        imageData.copy(ib, 0, uploadIdLength);
        function write() {
            fs.writeFile(path, ib, function (err) {
                if (err) {
                    console.log(err);
                    resObj.data = "Datei konnte nicht geschrieben werden";
                } else {
                    DB.updateRoomImage(roomInfo.id, name);
                    roomInfo = null;
                    console.log('Datei geschrieben: ' + path);
                    resObj.status = "ok";
                }
                socket.send(JSON.stringify(resObj));
            });
        }

        fs.exists(path, (exists) => {
            if (exists) {
                fs.rm(path, write);
            } else {
                write();
            }
        });
    }

}

function handleTextMessage(data, socket) {
    let parsedData = undefined;

    try {
        parsedData = JSON.parse(data);
    } catch (e) {
        console.error("error while parsing message from frontend");
        console.error(e);
    }
    if (parsedData === undefined) {
        return;
    }
    if (parsedData.event === "roomplanImage") {
        console.log("roomplanImage", parsedData);
        let resObj = {
            event: "response",
            messageId: parsedData.messageId,
            status: "ok",
            data: {}
        }
        if (isAcceptedImageExtension(parsedData.extension)) {
            // generate a random 32 digit long upload id
            uploadId = Array.from(
                {length: 32},
                () => Math.floor(Math.random() * 16).toString()).join("");
            // for (let i = 0; i < 64; i++) {
            //     uploadId +=
            //     uploadId += crypto.randomBytes(8).readUInt32BE(0, true).toString();
            // }
            // if (uploadId.length % 2 !== 0) {
            //     uploadId = uploadId.substring(0, uploadId - 1);
            // }
            roomInfo = {
                extension: parsedData.extension,
                uploadId: uploadId
            };
            if (parsedData.hasOwnProperty("id")) {
                roomInfo.id = parsedData.id;
            }
            resObj.data = uploadId;
        } else {
            resObj.status = "error";
            resObj.data = "Unerlaubter Dateityp:" + parsedData.extension;
        }
        socket.send(JSON.stringify(resObj));
    } else {
        try {
            onFrontendMessageReceived(parsedData, socket);
        } catch (e) {
            console.error("error while handling message from frontend");
            console.error(e);
        }
    }
}

/**
 * Handles the data and commands which are received by a frontend instance.
 * @param {Object} data
 * @param {WebSocket} socket
 */
function onFrontendMessageReceived(data, socket) {
    // get message metadata and create response object
    /**
     * @property {string | null}
     */
    let messageId;
    if (data.hasOwnProperty("messageId")) {
        messageId = data.messageId;
    } else {
        messageId = null;
    }
    /**
     * Response Object which will be send back to the frontend.
     * @type {{data: {}, messageId, event: string, status: string}}
     * @property {string} event - Should NOT be modified.
     * The message generated out of this Object is a response to a previously sent request.
     * @property {string} messageId - the id of the received message which allows the frontend
     * to identify this message as an answer to the previously sent request.
     * @property {string} status - the status of the request;
     * either <code>ok</code> if the request was completed successfully
     * or <code>error</code> if a error occurred while processing the request.
     * @property {Object | string | number} data - requested data or error information
     */
    let resObj = {
        event: "response",
        messageId: messageId,
        status: "ok",
        data: {}
    }

    // Requests are identified by the "event" property.
    // If this property is missing, the request cannot be fulfilled.
    if (!data.hasOwnProperty("event")) {
        console.error("invalid message: no 'event' property");
        resObj.status = "error";
        resObj.data = "invalid request: no 'event' property";
        socket.send(JSON.stringify(resObj));
        return;
    }

    // handle event information / requests
    if (data.event === "start") {
        console.log("measurement start received");
        // new data will be requested in the next measurement cycle from the sensor nodes
        const sensorNodeIds = getMeasurement(data.id)
            .objects
            .filter((o) => o.type === OBJECT_TYPE.SENSOR)
            .map(o => parseInt(o.id));
        const clients = udpClients.filter((client) => sensorNodeIds.includes(client.id));
        const missingClientIds = sensorNodeIds.filter(sn => !clients.map(c => c.id).includes(sn));
        startMeasurement(data.id, clients, missingClientIds);
    } else if (data.event === "stop") {
        console.log("measurement stop received");
        // the current measurement cycle will be completed
        stopMeasurement();
        const stopMessage = {
            event: "stopMeasurement"
        }
        sensorController.udpBroadcast(JSON.stringify(stopMessage));
    } else if (data.event === "sendMeasurementList") {
        console.log("sendMeasurementList");
        resObj.data = DB.getListOfMeasurements()
        console.log("resObj: ", resObj);
    } else if (data.event === "sendSingleMeasurement") {
        console.log("single measurement requested, id: %s", data.id);
        let reqMeasurement = DB.getMeasurement(data.id)
        if (reqMeasurement.hasOwnProperty("error")) {
            resObj.status = "error";
            resObj.data = reqMeasurement.error;
        } else {
            resObj.data = reqMeasurement;
            if (reqMeasurement.room instanceof Object && reqMeasurement.room.image != null) {
                reqMeasurement.room.image = "rooms/" + reqMeasurement.room.image;
            }
        }
        console.log("resObj: ", resObj);
    } else if (data.event === "deleteMeasurement") {
        console.log("deleting measurement request, id: %s", data.id);
        let result = DB.deleteMeasurement(parseInt(data.id));
        if (result === -1) {
            resObj.status = "error";
            resObj.data = "Messung konnte nicht aus der Datenbank gelöscht werden";
        }
    } else if (data.event === "newMeasurementConfig") {
        console.log("Konfiguration senden");
        if (data.objects.filter((object) => object.type === CONFIG.OBJECT_TYPE.SENSOR).length === 0) {
            resObj.status = "error";
            resObj.data = "Es kann keine Messung ohne Sensorknoten gestartet werden.";
        } else {
            let measurementInfo = DB.addMeasurement(data);
            console.log("measurement saving returned: ", measurementInfo)
            if (measurementInfo.hasOwnProperty("error")) {
                resObj.status = "error";
                resObj.data = measurementInfo.error;
            } else {
                resObj.data = {
                    id: measurementInfo.measurementID
                };
            }
        }
    } else if (data.event === "createRoom") {
        console.log("Raum erstellen");
        const roomId = DB.addRoom(data.room);
        if (roomId === -1) {
            resObj.status = "error";
            resObj.data = "Der Raum konnte nicht erstellt werden.";
        } else if (roomId === -2) {
            resObj.status = "error";
            resObj.data = "Ein Raum mit diesem Namen existiert bereits.";
        } else {
            if (roomInfo != null) {
                roomInfo.id = roomId;
            }
            resObj.data = {id: roomId};
        }
    } else if (data.event === "updateRoom") {
        console.log("Raumkonfiguration ändern");
        if (data.room.image != null && data.room.image.startsWith("rooms/")) {
            data.room.image = data.room.image.replace("rooms/", "");
        }
        if (DB.updateRoom(data.room) !== 0) {
            resObj.status = "error";
            resObj.data = "Der Raum konnte nicht aktualisiert werden";
        } else {
            resObj.data.id = data.room.id;
        }
    } else if (data.event === "forcedCalibration") {
        console.log("Erzwungene Kalibrierung");

        let forcedCalibrationMessage = {
            event: "forcedCalibration"
        };
        sensorController.resetCurrentCalibrationInfo(socket, data);

        if (data.co2 != null) {
            forcedCalibrationMessage.co2 = data.co2;
        }
        if (data.temperature != null) {
            forcedCalibrationMessage.temperature = data.temperature;
        }

        data.sensorknots.forEach((id) => {
            if (data.co2 != null) {
                console.log(`Erzwungene Kalibrierung von Sensor ${id} mit ${forcedCalibrationMessage.co2}ppm`)
            }
            if (data.temperature != null) {
                console.log(`Erzwungene Kalibrierung von Sensor ${id} mit ${forcedCalibrationMessage.temperature}°C`)
            }
            try {
                sensorController.sendToSensor(id, forcedCalibrationMessage);
            } catch (e) {
                sensorController.currentCalibrationInfo.notSent.push(id);
            }
        });

        // wait for sensor nodes to send a response
        sensorController.currentCalibrationInfo.timeoutId = setTimeout(sensorController.checkCalibrationStates, MEASUREMENT_INTERVAL * 2);
        // a response to the frontend is sent in sensorController.checkCalibrationStates
        return;
    } else if (data.event === "trafficLight") {
        const trafficLightMessage = {
            event: "trafficLight",
            trafficLight: data.state,
            timestamp: Date.now()
        }
        if (data.hasOwnProperty("id")) {
            // change the LED state of a specific sensor node
            console.log(`Turning LED-traffic-light of sensor ${data.id} ${data.state}`);
            sensorController.sendToSensor(data.id, trafficLightMessage);
            getUdpClient(data.id).LED = data.state;
        } else {
            // change all traffic lights
            sensorController.setLEDState(data.state);
            console.log(`Turning LED-traffic-light of all sensors ${data.state}`);
            for (const udpClient of udpClients) {
                udpClient.LED = data.state;
            }
            sensorController.udpBroadcast(trafficLightMessage);
        }
        return;
    } else if (data.event === "fineDustSensor") {
        const fineDustSensorMessage = {
            event: "switchFineDustSensor",
            fineDustSensor: data.state
        }
        if (data.hasOwnProperty("id")) {
            // change the fine dust sensor state of a specific sensor node
            console.log(`Turning fine dust sensor of sensor ${data.id} ${data.state}`);
            sensorController.sendToSensor(data.id, fineDustSensorMessage);
            getUdpClient(data.id).fineDustSensor = data.state;
        } else {
            // change all fine dust sensors
            sensorController.setFineDustSensorState(data.state);
            console.log(`Turning fine dust sensor of all sensors ${data.state}`);
            for (const udpClient of udpClients) {
                udpClient.fineDustSensor = data.state;
            }
            sensorController.udpBroadcast(fineDustSensorMessage);
        }
        return;
    } else if (data.event === "addProtocolEntry") {
        const result = DB.addProtocolEntry(data);
        if (result !== 0) {
            resObj.status = "error";
            resObj.data = "Konnte Protokolleintrag nicht in Datenbank abspeichern."
        }
    } else if (data.event === "getRooms") {
        const rooms = getListOfStoredRooms();
        if (rooms === -1) {
            resObj.status = "error";
            resObj.data = "Fehler beim Laden der Räume aus der Datenbank."
        } else {
            resObj.data = rooms;
        }
    } else {
        // unhandled event
        resObj.status = "error";
        resObj.data = "Unbekanntes event: " + data.event;
    }

    // the frontend expects an answer to all requests
    socket.send(JSON.stringify(resObj));
}

/**
 * @param {string | Object} msg
 */
export function broadcastToWebclients (msg) {
    if (msg instanceof Object) {
        msg = JSON.stringify(msg);
    }
    for (let i = 0; i < webClients.length; i++) {
        webClients[i].send(msg);
    }
}

function isAcceptedImageExtension(extension) {
    const allowedExtensions = [
        "PNG", "JPG", "GIF", "TIFF", "SVG", "WEBP", "BMP"
    ]
    return allowedExtensions.includes(extension.toUpperCase());
}