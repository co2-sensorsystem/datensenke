import {broadcastToWebclients} from "./frontendInterface";
import {SERVER_STATE, setState, STATE} from "./controller";
import {dLog, NO_DATA} from "../frontend/config";
import {addSensorsHeldBack, sendToSensor} from "./sensorNodeController";
import { getSensorMac} from "./database";
import {sleep} from "../frontend/js/modules/utils.mjs";

const {MEASUREMENT_INTERVAL} = require("./config");
const {SEND_DATA_TIMEOUT} = require("./config");
const DB = require("./database");
/**
 * Whether the server is waiting for sensor nodes to send data for the current measurement cycle.
 * Is false when either no measurement is running or data from all sensor nodes was received.
 * @type {boolean}
 */
export let waitingForMeasurementCycleData = false;
/**
 * ID of the currently running measurement.
 * @type {int}
 */
let currentMeasurementId = -1;
/**
 * Sensor nodes taking part in the measurement.
 * @type {udpClientInfo[]}
 */
let sensorNodes = [];
let missingSensorNodeIds = [];
/**
 * Data of the current measurement cycle
 * @type {{sensorknots: *[], event: string, timestamp: int}}
 */
export const currData = {
    "event": "measurementCycleData",
    "timestamp": 0,
    "id": currentMeasurementId,
    "sensorknots": []
};
let measurementClient;
let isMeasurementClockRunning = false;


async function addMissingMeasurementCycleDataAndSend() {
    waitingForMeasurementCycleData = true;
    await sleep(SEND_DATA_TIMEOUT);
    if (waitingForMeasurementCycleData && STATE === SERVER_STATE.MEASUREMENT) {
        console.info("sending incomplete data");
        // Complete data with error values for sensor nodes which did not send data yet
        // and send data to frontend afterwards.
        for (let sensorNode of sensorNodes) {
            if (!currData.sensorknots.some(node => node.id === sensorNode.id)) {
                let missingNode = getMissingDataset(sensorNode.mac, sensorNode.id)
                currData.sensorknots.push(missingNode)
                console.info("sensor node with id: " + sensorNode.id + " is missing.")
            }
        }
        sendCurrentMeasurementCycleData();
    }
    // Add the sensors which have been added during the measurement cycle
    // to not wait for their answers while they did not even receive a request in the first place.
    addSensorsHeldBack();
}

function sendCurrentMeasurementCycleData() {
    waitingForMeasurementCycleData = false;
    // order data by sensor id to make data handling easier in frontend (e.g. chart and heatmap need ordered data)
    currData.sensorknots.sort((a, b) => a.id - b.id);
    broadcastToWebclients(JSON.stringify(currData));
    console.log(currData)
    const addedMeasureID = DB.addValuesToMeasurement(getCurrentMeasurementId(), currData);
    if (addedMeasureID === -1) {
        console.error("error while trying to insert measurement")
    }
    currData.sensorknots = [];
}

/**
 * Clock which triggers a UDP broadcasts calling for measurement data.
 * The clock's interval is the {@link MEASUREMENT_INTERVAL}.
 * @return void
 */
async function measurementCycleClock() {
    while (STATE === SERVER_STATE.MEASUREMENT) {
        console.log("new measurement cycle")
        let startMes = {
            "event": "startMeasurement",
            "timestamp": Date.now()
        }
        currData.timestamp = startMes.timestamp
        for (const sensorNode of sensorNodes) {
            sendToSensor(sensorNode.id, startMes);
        }
        console.log("start measurement sent");

        // add the data of the sensor nodes which are not connected and thus will not respond
        for (const missingSensorNodeId of missingSensorNodeIds) {
            currData.sensorknots.push(
                getMissingDataset(getSensorMac(missingSensorNodeId), missingSensorNodeId));
        }

        // We want cycles of exactly 5 seconds, therefore check and send data asynchronously.
        // The method itself waits for the durations specified in SEND_DATA_TIMEOUT.
        // noinspection ES6MissingAwait
        addMissingMeasurementCycleDataAndSend();

        await sleep(MEASUREMENT_INTERVAL);
    }
}

/**
 * <p>Start collecting data from sensors for a new measurement.</p>
 * New measurement data is requested from the sensor nodes periodically
 * (interval length = {@link MEASUREMENT_INTERVAL}) and saved to the database.
 * @param {int} id
 * @param {udpClientInfo[]} _sensorNodes
 * @param {int[]} _missingSensorNodeIds
 */
export function startMeasurement(id, _sensorNodes, _missingSensorNodeIds) {
    console.log("Starting measurement with ID " + id);
    currentMeasurementId = id;
    currData.id = currentMeasurementId;
    sensorNodes = _sensorNodes;
    missingSensorNodeIds = _missingSensorNodeIds;
    setState(SERVER_STATE.MEASUREMENT);
    measurementCycleClock();
}

export function stopMeasurement() {
    setState(SERVER_STATE.IDLE);
    // Once the measurement is considered to be stopped,
    // there might come in a few more data sets from sensor nodes
    // replying to previously broadcast measurement orders.
    // However, we will not take them into account anymore.
    // The current measurement cycle is aborted and a few datasets might get lost.
    waitingForMeasurementCycleData = false;
    currentMeasurementId = -1;
    sensorNodes = [];
}

export function onMeasurementDateReceived(nodeData) {
    if (nodeData.timestamp === currData.timestamp) {
        if (sensorNodes.some(node => node.mac === nodeData.mac)) {
            nodeData.id = DB.getSensorID(nodeData.mac)
            for (let i = 0; i < currData.sensorknots.length; i++) {
                if (currData.sensorknots[i].mac === nodeData.mac) {
                    currData.sensorknots.splice(i, 1);
                }
            }
            currData.sensorknots.push(nodeData);
            dLog("Erhaltene Messdaten: ", currData.sensorknots.length);
            dLog("Erwartete Messdaten: ", sensorNodes.length);
            if (currData.sensorknots.length >= sensorNodes.length) {
                console.info("sending complete data");
                sendCurrentMeasurementCycleData();
            }
        } else {
            console.log("Messdaten von Sensor erhalten, der nicht an der Messung teilnimmt")
        }
    } else {
        console.error("Messdaten sind veraltet: erwarteter Zeitstempel ", currData.timestamp,
            ", erhaltener Zeitstempel ", nodeData.timestamp)
    }
}

export function getCurrentMeasurementId() {
    return currentMeasurementId;
}

export function addMissingSensorNodeToMeasurement(udpClient) {
    if (!sensorNodes.includes(udpClient) && missingSensorNodeIds.includes(udpClient.id)) {
        missingSensorNodeIds = missingSensorNodeIds.splice(
            missingSensorNodeIds.indexOf(udpClient), 1);
        sensorNodes.push(udpClient);
    }
}

/**
 *
 * @param {string} mac
 * @param {number} id
 */
function getMissingDataset(mac, id) {
    return {
        mac: mac,
        id: id,
        battery: NO_DATA,
        co2: NO_DATA,
        humidity: NO_DATA,
        temperature: NO_DATA,
        pm25: NO_DATA,
        pm10: NO_DATA
    }
}