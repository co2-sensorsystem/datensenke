module.exports.WS_PORT = 12345; // port of webserver for connection to browser
module.exports.MEASUREMENT_INTERVAL = 5000; // interval of measurement in ms
module.exports.SEND_DATA_TIMEOUT = module.exports.MEASUREMENT_INTERVAL * (2/3); // timeout for sending incomplete data in ms (recommended  2/3 of measurement interval)
module.exports.ALIVE_PING_ENABLE = true; // alive ping is enabled or disabled
module.exports.ALIVE_INTERVAL = module.exports.MEASUREMENT_INTERVAL // interval of alive pings in ms
module.exports.ERROR_VAL = -9999;