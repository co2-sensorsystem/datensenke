const DB = require('./database');

console.log("Datenbank erstellen");
DB.createDB();
console.log("Datenbank erstellt oder bereits existent");

console.log("Mac-Adressen anlegen:");
let mac1 = "CC:50:E3:4A:31:F1";
let mac2 = "AA:BB:CC:DD:EE:FF";
console.log("SensorID: " + DB.getSensorID(mac1) + "; MAC: " + mac1);
console.log("SensorID: " + DB.getSensorID(mac2) + "; MAC: " + mac2);
console.log("Mac-Adressen angelegt oder existent");

console.log("Messung hinzufügen:");
let addMeasurementObj = {
    name: "test",
    sensorknots: [
        {
            id: 1,
            position: {
                x: 1,
                y: 1
            }
        },
        {
            id: 2,
            position: {
                x: 2,
                y: 2
            }
        }
    ],
    room: {
        name : "PLATZHALTER",
        width: 10,
        length: 20
    }
};
console.log("hinzuzufügende Messung:" + JSON.stringify(addMeasurementObj));

//WIRD BEIM ZWEITEN VERSUCH FEHLSChLAGEN, DA EIN RAUM MIT DEM NAMEN BEREITS EXISTIERT
let measurementResultObj = DB.addMeasurement(addMeasurementObj);
if(measurementResultObj.hasOwnProperty("error")) {
    console.log("Fehler beim Erstellen der Messung, Fehlernachricht: " + measurementResultObj.error);
} else {
    console.log("Messung hinzugefügt - ID: " + measurementResultObj.measurementID);

    console.log("Messwerte hinzufügen:");
    let sensorDataObj = {
        timestamp: Date.now(),
        sensorknots: [
            {
                id: 1,
                co2: 500
            },
            {
                id: 2,
                co2: 250
            }
        ]
    }
    console.log("hinzuzufügende Messdaten:" + JSON.stringify(sensorDataObj));

    DB.addValuesToMeasurement(measurementResultObj.measurementID, sensorDataObj);
    console.log("Messdaten hinzugefügt");

    console.log("Hinzugefügte Messung abfragen:");
    console.log(DB.getMeasurement(measurementResultObj.measurementID));
    console.log("Hinzugefügte Messung abgefragt");
}

console.log("Liste der Messungen abrufen:");
let measurements = DB.getListOfMeasurements();
console.log("Liste der Messungen: " + measurements);
