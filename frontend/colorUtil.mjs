/**
 * @author CodeDrome
 * @see https://github.com/CodeDrome/hslrgb-conversions-javascript
 * Originally created by <a href="https://github.com/CodeDrome/">CodeDrome</a>.
 * Modified by Tobias Groza.
 */

/**
 *
 * @param {number} R
 * @param {number} G
 * @param {number} B
 * @return {float}
 */
export function calculateLightness(R, G, B)
{
    let Max = 0.0
    let Min = 0.0

    let fR = R / 255.0;
    let fG = G / 255.0;
    let fB = B / 255.0;

    if(fR >= fG && fR >= fB)
        Max = fR;
    else if(fG >= fB && fG >= fR)
        Max = fG;
    else if(fB >= fG && fB >= fR)
        Max = fB;

    if(fR <= fG && fR <= fB)
        Min = fR;
    else if(fG <= fB && fG <= fR)
        Min = fG;
    else if(fB <= fG && fB <= fR)
        Min = fB;

    let Lightness = (Min + Max) / 2.0;

    return Lightness;
}

/**
 *
 * @param {number} R
 * @param {number} G
 * @param {number} B
 * @return {float}
 */
export function calculateSaturation(R, G, B)
{
    let Max = 0.0;
    let Min = 0.0;

    let fR = R / 255.0;
    let fG = G / 255.0;
    let fB = B / 255.0;

    if(fR >= fG && fR >= fB)
        Max = fR;
    else if(fG >= fB && fG >= fR)
        Max = fG;
    else if(fB >= fG && fB >= fR)
        Max = fB;

    if(fR <= fG && fR <= fB)
        Min = fR;
    else if(fG <= fB && fG <= fR)
        Min = fG;
    else if(fB <= fG && fB <= fR)
        Min = fB;

    let Lightness = exports.CalculateLightness(R, G, B);

    let Saturation;

    if(Max == Min)
    {
        Saturation = -1.0;
    }
    else
    {
        if(Lightness < 0.5)
        {
            Saturation = (Max - Min) / (Max + Min);
        }
        else
        {
            Saturation = (Max - Min) / (2.0 - Max - Min);
        }
    }

    return Saturation;
}

/**
 *
 * @param {number} R
 * @param {number} G
 * @param {number} B
 * @return {number}
 */
export function calculateHue(R, G, B)
{
    let Max = 0.0;
    let Min = 0.0;

    let fR = R / 255.0;
    let fG = G / 255.0;
    let fB = B / 255.0;

    if(fR >= fG && fR >= fB)
        Max = fR;
    else if(fG >= fB && fG >= fR)
        Max = fG;
    else if(fB >= fG && fB >= fR)
        Max = fB;

    if(fR <= fG && fR <= fB)
        Min = fR;
    else if(fG <= fB && fG <= fR)
        Min = fG;
    else if(fB <= fG && fB <= fR)
        Min = fB;

    let Hue;

    if(Max == Min)
    {
        Hue = -1.0;
    }
    else
    {
        if(Max == fR)
        {
            Hue = (fG - fB) / (Max - Min);
        }
        else if(Max == fG)
        {
            Hue = 2.0 + (fB - fR) / (Max - Min);
        }
        else if(Max == fB)
        {
            Hue = 4.0 + (fR - fG) / (Max - Min);
        }

        Hue *= 60.0;

        if(Hue < 0.0)
        {
            Hue += 360.0;
        }
    }

    return Hue;
}

/**
 *
 * @param {number} H
 * @param {number} S
 * @param {number} L
 * @return {RGBColor}
 */
export function HSLtoRGB(H, S, L)
{
    let colour = { r: 0, g: 0, b: 0 };

    if(H == -1.0 && S == -1.0)
    {
        colour.r = L * 255.0;
        colour.g = L * 255.0;
        colour.b = L * 255.0;
    }
    else
    {
        let temporary_1;

        if(L < 0.5)
            temporary_1 = L * (1.0 + S);
        else
            temporary_1 = L + S - L * S;

        let temporary_2;

        temporary_2 = 2.0 * L - temporary_1;

        let hue = H / 360.0;

        let temporary_R = hue + 0.333;
        let temporary_G = hue;
        let temporary_B = hue - 0.333;

        if(temporary_R < 0.0)
            temporary_R += 1.0;
        if(temporary_R > 1.0)
            temporary_R -= 1.0;

        if(temporary_G < 0.0)
            temporary_G += 1.0;
        if(temporary_G > 1.0)
            temporary_G -= 1.0;

        if(temporary_B < 0.0)
            temporary_B += 1.0;
        if(temporary_B > 1.0)
            temporary_B -= 1.0;

        // RED
        if((6.0 * temporary_R) < 1.0)
        {
            colour.r = (temporary_2 + (temporary_1 - temporary_2) * 6.0 * temporary_R) * 255.0;
        }
        else if((2.0 * temporary_R) < 1.0)
        {
            colour.r = temporary_1 * 255.0;
        }
        else if((3.0 * temporary_R) < 2.0)
        {
            colour.r = (temporary_2 + (temporary_1 - temporary_2) * (0.666 - temporary_R) * 6.0) * 255.0;
        }
        else
        {
            colour.r = temporary_2 * 255.0;
        }

        // GREEN
        if((6.0 * temporary_G) < 1.0)
        {
            colour.g = (temporary_2 + (temporary_1 - temporary_2) * 6.0 * temporary_G) * 255.0;
        }
        else if((2.0 * temporary_G) < 1.0)
        {
            colour.g = temporary_1 * 255.0;
        }
        else if((3.0 * temporary_G) < 2.0)
        {
            colour.g = (temporary_2 + (temporary_1 - temporary_2) * (0.666 - temporary_G) * 6.0) * 255.0;
        }
        else
        {
            colour.g = temporary_2 * 255.0;
        }

        // BLUE
        if((6.0 * temporary_B) < 1.0)
        {
            colour.b = (temporary_2 + (temporary_1 - temporary_2) * 6.0 * temporary_B) * 255.0;
        }
        else if((2.0 * temporary_B) < 1.0)
        {
            colour.b = temporary_1 * 255.0;
        }
        else if((3.0 * temporary_B) < 2.0)
        {
            colour.b = (temporary_2 + (temporary_1 - temporary_2) * (0.666 - temporary_B) * 6.0) * 255.0;
        }
        else
        {
            colour.b = temporary_2 * 255.0;
        }
    }

    colour.r = Math.round(Math.abs(colour.r));
    colour.g = Math.round(Math.abs(colour.g));
    colour.b = Math.round(Math.abs(colour.b));

    return colour;
}

/**
 *
 * @param {number} r
 * @param {number} g
 * @param {number} b
 * @return {HSLColor}
 */
export function RGBtoHSL(r, g, b) {
    return {
        h: calculateHue(r, g, b),
        s: calculateSaturation(r, g, b),
        l: calculateLightness(r, g, b)
    }
}

/**
 * @typedef RGBColor
 * @type Object
 * @property {number} r from 0 to 255
 * @property {number} g from 0 to 255
 * @property {number} b from 0 to 255
 */

/**
 * @typedef HSLColor
 * @type Object
 * @property {number} h from 0 to 365
 * @property {float} s from 0 to 1
 * @property {float} l from 0 to 1
 */