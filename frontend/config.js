/**
 * Position in percent, relatively to the whole container.
 * @typedef Coordinate
 * @property {int} x
 * @property {int} y
 */

/**
 * @typedef Datapoint
 * @property time {Date}
 * @property co2 {number}
 * @property humidity {number}
 * @property temperature {number}
 * @property battery {number}
 * @property pm25 {number}
 * @property pm10 {number}
 */

/**
 * @typedef MeasurementConfig
 * @type Object
 * @property {string} name
 * @property {Room} room
 * @property {Set[]} sensorknots
 * @property {string} sensorknots[].id
 * @property {Coordinate} sensorknots[].pos
 */

/**
 * @typedef Room
 * @type Object
 * @property {int} length
 * @property {int} width
 * @property {int} height
 * @property {number} volume
 * @property {string} name
 * @property {int} [id]
 * @property {string | null | Object} image
 * @property {string} image.bin
 * @property {string} image.base64
 * @property {RoomElement[]} [elements]
 */

/**
 * @typedef StoredMeasurementInfo
 * @type Object
 * @property {string} id
 * @property {string} name
 * @property {int} startTimestamp
 * @property {int} room - room id
 */

/**
 * @typedef SingleMeasurementData
 * @type Object
 * @property {int} co2
 * @property {int} humidity
 * @property {int} temperature
 */

/**
 * @typedef MeasurementEvent
 * @type {int} type
 * @type {int} timestamp
 */



/**
 * @typedef Measurement
 * @type Object
 * @property {int} id the measurement ID
 * @property {int} timestamp the timestamp at which the measurement was started
 * @property {string} name the measurement's name
 * @property {Room} room the room in which the measurement was performed
 * @property {MeasurementEvent[]} events the events which occurred during the measurement
 * @property {Object[]} objects the objects which are in the room like sensors, windows and air filters
 * @property {Datapoint[]} data - measurement data; each key is a timestamp
 */

export const CONFIG = {
    DEBUG: false,
    MEASUREMENT_INTERVAL: 5000,
}

export const MODE = {
    CONFIGURATION: "CONFIGURATION",
    MEASUREMENT: "MEASUREMENT",
    VIEWER: "VIEWER",
    BLOCKED: "BOCKED"
}

export const CONTENT = {
    ROOMPLAN: "ROOMPLAN",
    GRAPH: "GRAPH"
}

export const OBJECT_TYPE = {
    SENSOR: 0x00,
    WINDOW: 0x10,
    VENTILATION: 0x11,
    DOOR: 0x15,
    AIR_FILTER: 0x20,
    UV_DESINFECTOR: 0x21,
}

/**
 * Value for measurement data which could not be collected.
 */
export const NO_DATA = -9999;

/**
 * Log data into the debug console if {@see CONFIG.DEBUG} is <code>true</code>
 * @see {Console.debug}
 * @param data
 * @return void
 */
export function dLog(...data) {
    if (CONFIG.DEBUG) {
        console.debug(data)
    }
}

/**
 * States for the LEDs on sensor nodes.
 * @type {{OFF: "on", ON: "off"}}
 */
export const LED_STATES = {
    ON: "on",
    OFF: "off"
}

/**
 * States for the fine dust sensor on sensor nodes.
 * @type {{OFF: "off", ON: "an"}}
 */
export const FINEDUSTSENSOR_STATES = {
    ON: "on",
    OFF: "off"
}