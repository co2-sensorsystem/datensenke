class SensorHandler {
    /**
     * @type {Controller}
     */
    controller
    /**
     * @type {Sensor[]}
     */
    sensors = [];
    /**
     * @type {Sensor[]}
     */
    removedSensors = [];

    /**
     *
     * @param {Controller} controller
     */
    constructor(controller) {
        this.controller = controller
    }

    /**
     * @param {Sensor} sensor
     */
    addSensor = function (sensor) {
        this.sensors.push(sensor);
    }

    /**
     * @param sensor {Sensor}
     */
    removeSensor = function (sensor) {
        this.sensors.forEach((s, index) => {
            if (sensor.id === s.id) {
                this.removedSensors.push(sensor);
                this.sensors.splice(index, 1);
            }
        })
    }

    reAddSensor = function (index) {
        if (index >= 0 && index < this.removedSensors.length) {
            let sensor = this.removedSensors[index];
            this.sensors.push(sensor);
            this.removedSensors.splice(index, 1);
            return sensor;
        }
        return null;
    }

    /**
     * Get a Sensor by its id
     * @param sensorId {int} either sensor id or
     * sensor div id which is preceded by <code>sensor-</code>
     * @return {null, Sensor}
     */
    getSensorById = function(sensorId) {
        const possibleSensors = this.sensors.filter(
            /** @param {Sensor} sensor */ (sensor) =>
                sensor.id === sensorId || 'sensor-' + sensor.id === sensorId);
        if (possibleSensors) return possibleSensors[0];
        return null;
    }

    /**
     *
     * @param hasRunningMeasurement {boolean}
     */
    reset = function (hasRunningMeasurement) {
        if (hasRunningMeasurement) {
            this.sensors.forEach((sensor, index) => {
                sensor.data = [];
            });
        } else {
                this.sensors = [];
                this.removedSensors = [];
        }
    }
}

class Sensor {
    battery = -1;
    /**
     * @param position {Coordinate}
     * @param id {int}
     * @param mac {string}
     * @param data {Datapoint[]} latest datapoint has the highest index
     * @param height {number}
     * @param note {string}
     */
    constructor(id, mac, position, data, height, note) {
        this.pos = position;
        this.data = data;
        this.id = id;
        this.mac = mac;
        this.height = height;
        this.note = note;
    }
}