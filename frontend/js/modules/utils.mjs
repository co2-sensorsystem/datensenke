/**
 * Create a DOM element from an HTML string.
 * @param {string} htmlString
 * @return {Node}
 */
export function createElementFromHTML(htmlString) {
    let div = document.createElement('div');
    div.innerHTML = htmlString.trim();
    // Change this to div.childNodes to support multiple top-level nodes.
    return div.firstChild;
}

/**
 * Helper function to make Bootstrap's radio toggles work correctly.
 * @param toggle
 * @param toggles
 */
export function selectToggle(toggle, toggles) {
    // toggle the inputs manually
    // for some reason, this is not always done by Bootstrap...
    for (const t of toggles) {
        t.checked = false;
        t.removeAttribute("checked");
    }
    toggle.checked = true;
    toggle.setAttribute("checked", "");
}

/**
 * Convert a hex string to an ArrayBuffer.
 *
 * @param {string} hexString - hex representation of bytes
 * @return {ArrayBuffer} - The bytes in an ArrayBuffer.
 */
export function hexStringToArrayBuffer(hexString) {
    // remove the leading 0x
    hexString = hexString.replace(/^0x/, '');

    // ensure even number of characters
    if (hexString.length % 2 != 0) {
        console.log('WARNING: expecting an even number of characters in the hexString');
    }

    // check for some non-hex characters
    var bad = hexString.match(/[G-Z\s]/i);
    if (bad) {
        console.log('WARNING: found non-hex characters', bad);
    }

    // split the string into pairs of octets
    var pairs = hexString.match(/[\dA-F]{2}/gi);

    // convert the octets to integers
    var integers = pairs.map(function(s) {
        return parseInt(s, 16);
    });

    var array = new Uint8Array(integers);
    console.log(array);

    return array.buffer;
}

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   {number}  h       The hue; element in [0, 1]
 * @param   {number}  s       The saturation; element in [0, 1]
 * @param   {number}  l       The lightness; element in [0, 1]
 * @return  {RGBColor}        The RGB representation
 */
export function hslToRgb(h, s, l){
    let r, g, b;

    if (s == 0){
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t){
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1/6) return p + (q - p) * 6 * t;
            if (t < 1/2) return q;
            if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        const p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return {r: Math.round(r * 255), g: Math.round(g * 255), b: Math.round(b * 255)};
}

/**
 *
 * @param {Date} date
 * @param {string} time In the format <code>hh:mm</code> or <code>hh:mm:ss</code>
 * @return {number}
 */
export function getLocalizedDateForTimeString(date, time) {
    const utcDateOfMeasurementStart = date;
    const utcDateStringOfGivenTime = utcDateOfMeasurementStart.toDateString() + " " + time + " UTC";
    const utcMilliForTime = Date.parse(utcDateStringOfGivenTime);
    const currentTimeZoneOffset = new Date().getTimezoneOffset() * 60 * 1000;
    return utcMilliForTime + currentTimeZoneOffset;
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}