import * as CommunicationHandler from "../../CommunicationHandler.mjs";
import {createSelectFromListModal} from "./modalHelper.mjs";
import * as ErrorModal from "./ErrorModal.mjs";

/**
 * @type {ModalWrapper}
 */
let mw = undefined;

let onSuccess, onDismiss;

/**
 * @type {Room[]}
 */
let rooms;

/**
 * @param {function(Room[])} onRoomsLoaded
 */
export function init(onRoomsLoaded) {
    if (mw !== undefined) {
        return;
    }

    updateRoomList(onRoomsLoaded);
}

/**
 *
 * @param {function(Room)} _onSuccess
 * @param {function} _onDismiss
 */
export function openSelectRoomDialog(_onSuccess, _onDismiss) {
    mw.open();
    onSuccess = _onSuccess;
    onDismiss = _onDismiss;
}

function onSuccessProxy() {
    const id = mw.body.querySelector('li.active').dataset.apiRoomId;
    onSuccess(rooms.find((r => r.id == id)));
}

function onDismissProxy() {
    onDismiss();
}

/**
 * @return {Room[]}
 */
export function getRooms() {
    return rooms;
}

/**
 * @param {Room} room
 */
export function addRoom(room) {
    rooms.push(room);
}

export function updateRoomList(onRoomsLoaded) {
    const id = "chooseRoomModal";

    CommunicationHandler.send({event: "getRooms"}, {
        context: null,
        onSuccess: (data) => {
            if (mw) {
                mw.modal.dispose();
                mw.div.remove();
            }
            rooms = data;
            let rows = [];
            rooms.forEach((room, idx) => {
                rows.push(`<li class="list-group-item ${idx === 0 ? 'active' : ''}" data-api-room-id="${room.id}">${room.name}</li>`);
            })
            const selectButton = {
                html: '<div class="btn btn-success col ms-2" data-bs-dismiss="modal">OK</div>',
                action: onSuccessProxy
            }
            const dismissButton = {
                html: '<div class="btn btn-danger col me-2" data-bs-dismiss="modal">Abbrechen</div>',
                action: onDismissProxy
            }
            mw = createSelectFromListModal(id, "Raum wählen", rows, selectButton, dismissButton, true, {})
            if (onRoomsLoaded) {
                onRoomsLoaded(rooms);
            }
        },
        onError: (error) => {
            ErrorModal.open("Laden der Raumliste fehlgeschlagen", error);
            if (onRoomsLoaded) {
                onRoomsLoaded([]);
            }
        }
    })

}