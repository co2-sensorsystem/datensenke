import {createModal} from "./modalHelper.mjs";
import {dLog} from "../../../../config.js";

/**
 * @type {ModalWrapper}
 */
let mw = undefined;

function init() {
    if (mw !== undefined) {
        return;
    }
    const dialog = `
        <div class="modal fade" id="errorModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ein Fehler ist aufgetreten</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h5 id="errorTitle"></h5>
                        <div id="errorDescription"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>`;
    const id = 'errorModal';
    mw = createModal({keyboard: false}, dialog, id,
        () => { /* no success button */  }, () => { /* just close it */  });
}

export function open(action, info, id = null) {
    dLog("showing error modal")
    dLog(action)
    dLog(info)

    document.getElementById("errorTitle").innerText = action
    document.getElementById("errorDescription").innerHTML = info.toString()
    if (id == null) {
        mw.div.removeAttribute("data-id")
    } else {
        mw.div.setAttribute("data-id", id)
    }
    mw.modal.show()
}

export function hide (id = null) {
    // noinspection EqualityComparisonWithCoercionJS
    if (id != null && mw.div.dataset["id"] != id) {
        return;
    }
   mw.modal.hide();
}

init();

