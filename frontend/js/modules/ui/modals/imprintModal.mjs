import * as ModalHelper from './modalHelper.mjs';
let initialized;
/**
 * @type ModalWrapper
 */
let mw;

export function init() {
    if (initialized) {
        updateListeners();
        return;
    }
    const id = 'imprintModal';
    const html = `
        <!-- modal for showing copyright -->
        <div class="modal fade" id="${id}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="copyrightModalLabel">Impressum</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div id="uni-logo" class="img-responsive">
                            <img src="/UOL_Logo_Blau.png" alt="Carl von Ossietzky Universität Oldenburg">
                        </div>
                        <p class="mb-0">Diese Website inklusive des Sensornetzwerkes wurde im Rahmen des Proseminars "Kooperatives
                            Studierendenseminar zur Entwicklung eines verteilten Umweltsensorsystems" entwickelt.
                            Die Betreuung erfolgte durch Herrn Prof. Dr.-Ing. Oliver Theel, Marvin Banse und Christian Linder.
                            Die Entwicklung erfolgte durch
                            Niklas Altmann, Jan Berkhout, Tobias Boelsen, Grigorios Giotas,
                            Tobias Joel Groza, Andree Hildebrandt und Marten Kante.
                            <br>
                            <br> Im Rahmen einer Bachelorarbeit wurde das Messsystem von Tobias Boelsen und Tobias Joel Groza
                            weiterentwickelt und u.a. um die Prognoseanwendung ergänzt.
                            Herr Prof. Dr.-Ing. Oliver Theel und Marvin Banse betreuten die Arbeit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    `;
    mw = ModalHelper.createModal({}, html, id,
        () => { /* the modal just contains info to read, no action required */},
        () => { /* the modal just contains info to read, no action required */})
    updateListeners();
    initialized = true;
}

export function open() {
    if (!initialized) {
        init();
    }
    mw.open();
}

export function updateListeners() {
    document.querySelectorAll('[data-api-action="openImprintModal"]').forEach(e => {
        e.addEventListener('click', open)
    })
}