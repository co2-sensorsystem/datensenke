let modalNumber = 0;
export class ModalWrapper {
    /**
     * @type {bootstrap.Modal}
     */
    _modal;
    /**
     * @type {HTMLDivElement}
     */
    _div;
    /**
     * modal body
     * @type {HTMLDivElement}
     */
    _body;
    _footer;
    /**
     * @type {function}
     */
    #onDismiss;
    /**
     * @type {function}
     */
    #onSuccess;
    constructor(modal, div, onSuccess, onDismiss) {
        this._modal = modal;
        this._body = div.querySelector('.modal-body');
        this._footer = div.querySelector('.modal-footer');
        this.#onSuccess = onSuccess;
        this.#onDismiss = onDismiss;

        if (this._footer != null) {
            const ctx = this;
            const successButton = this._footer.querySelector('div.btn-success');
            if (successButton) {
                successButton.addEventListener('click', () => ctx.#onSuccess());
            }
            const dismissButton = this._footer.querySelector('.btn-danger');
            if (dismissButton) {
                dismissButton.addEventListener('click', () => ctx.#onDismiss());
            }
        }
        this._div = div;
    }

    open() {
        this._modal.show();
    }

    close() {
        this._modal.hide();
    }

    get modal() {
        return this._modal;
    }

    get div() {
        return this._div;
    }

    get body() {
        return this._body;
    }

    get footer() {
        return this._footer;
    }
}

/**
 *
 * @param {Object} options
 * @param {string} html
 * @param {string} id
 * @param {function} onSuccess
 * @param {function | null} onDismiss
 * @return {ModalWrapper}
 */
export function createModal(options, html, id, onSuccess, onDismiss) {
    const wrapper = document.createElement('div');
    wrapper.id = `modal-wrapper-${++modalNumber}`;
    wrapper.innerHTML = html;
    document.body.appendChild(wrapper);
    const div = document.getElementById(id);
    const modal = new bootstrap.Modal(div, options);
    return new ModalWrapper(modal, div, onSuccess, onDismiss);
}


/**
 * @typedef ButtonWithAction
 * @property {string} html
 * @property {function(Object)} action
 */



/**
 * @param {string} id
 * @param {string} title
 * @param {string[]} list list of <code>HTML</code> strings representing <code>LI</code> elements
 * having the class <code>list-group-item</code>.
 * @param {ButtonWithAction} success
 * @param {ButtonWithAction | null} dismiss
 * @param {boolean} select
 * @param {Object} [options]
 * @return {ModalWrapper}
 */
export function createSelectFromListModal(id, title, list, success, dismiss, select, options = {}, ) {
    const dialog = `
        <div class="modal fade" id="${id}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable list-modal ${select ? 'modal-list-select' : ''}">
                <div class="modal-content">
                    <div class="modal-header border">
                        <h5 class="modal-title">${title}</h5>
                    </div>
                    <div class="modal-body p-0">
                        <ul class="list-group rounded-0" id="${id}List">
                            ${list.join("\n")}
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <div class="row w-100">
                            ${(dismiss.html || '' ) + "\n" + success.html}
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
    const modal = createModal(options, dialog, id, success.action, dismiss.action || null);
    const listElements = modal.body.getElementsByTagName('LI');
    for (let listEl of listElements) {
        listEl.addEventListener('click', function () {
            let active = modal.body.getElementsByClassName("list-group-item active")
            if (active && active.item(0) !== null) {
                active.item(0).classList.remove("active")
            }
            listEl.classList.add('active');
        }, false);
    }
    return modal;
}