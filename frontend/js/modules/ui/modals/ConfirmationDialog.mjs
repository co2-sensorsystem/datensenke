export class ConfirmationDialog {
    modalElement = null;
    bsModal = null;

    /**
     * Create a confirmation dialog
     * @param title {string}
     * @param message {string}
     * @param onPositive {function}
     * @param onNegative {function}
     * @param type {int | Object} specifies the dialog's type - either one of the predefined types
     * from {@see ConfirmationDialog.TYPE} or an <code>Object</code> which contains the text
     * for the <code>positive</code> and <code>negative</code> buttons.
     * @param type.positive {string}
     * @param type.negative {string}
     * @param context {Object}
     */
    constructor(title, message, onPositive, onNegative, type, context) {
        if (this.modalElement == null) {
            this.modalElement = document.getElementById("confirmationModal");
            this.bsModal = new bootstrap.Modal(this.modalElement, {
                backdrop: 'static',
                keyboard: false
            });
        }
        const footer = this.modalElement.querySelector(".modal-footer *");
        const negativeButton = footer.children[0];
        const positiveButton = footer.children[1];
        // set content
        this.modalElement.querySelector(".modal-title").innerHTML = title;
        this.modalElement.querySelector(".modal-body").innerHTML = message;
        if (type === TYPE.YES_NO) {
            negativeButton.innerHTML = "Nein";
            positiveButton.innerHTML = "Ja";
        } else if (type === TYPE.OK) {
            negativeButton.innerHTML = "Abbrechen";
            positiveButton.innerHTML = "OK";
        } else if (type === TYPE.CONTINUE) {
            negativeButton.innerHTML = "Abbrechen";
            positiveButton.innerHTML = "Fortfahren";
        } else {
            negativeButton.innerHTML = type.negative;
            positiveButton.innerHTML = type.positive;
        }
        // set event listener
        negativeButton.addEventListener("click", () => {
            cleanUpBackdrops();
            onNegative.call(context);
        });
        positiveButton.addEventListener("click", () => {
            cleanUpBackdrops();
            onPositive.call(context);
        });
        // display dialog
        this.bsModal.show();
    }
}

export const TYPE = {
    YES_NO: 0,
    OK: 1,
    CONTINUE: 2
}

function cleanUpBackdrops() {
    document.querySelectorAll('.modal-backdrop.fade.show').forEach(e => e.remove());
}