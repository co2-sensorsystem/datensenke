import {createModal} from "../modalHelper.mjs";
import {SensorNodeRoomplanElement} from "../../../room/RoomplanElement.mjs";
let initialized;
/**
 * @type ModalWrapper
 */
let mw;
/**
 * @type {function(Object)}
 */
let onSuccess;
/**
 * @type {function()}
 */
let onDismiss;
/**
 * @type HTMLInputElement
 */
let heightInput;
/**
 * @type HTMLTextAreaElement
 */
let notesInput;

export function init() {
    if (initialized) return;
    const id = 'sensorNodePropertiesModal';
    const html = `
    <div class="modal properties-modal" id="${id}" data-create-type="door" data-dialog-context="create" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sensorknoten bearbeiten</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="sensorNode-properties-form" class="needs-validation" novalidate>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-3">
                                <label for="sensorNodePropertiesModal-height" class="col-form-label">Höhe</label>
                            </div>
                            <div class="col-5">
                                <input type="number" min="0" max="25" step="0.01" id="sensorNodePropertiesModal-height" 
                                class="form-control" aria-describedby="windowDoorPropertiesModal-openingHeightHelp" required>
                            </div>
                            <div class="col-3">
                                <span id="sensorNodePropertiesModal-heightHelp" class="form-text">
                                  in Meter
                                </span>
                            </div>
                        </div>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-12">
                                <label for="sensorNodePropertiesModal-note">Anmerkungen</label>
                            </div>
                            <div class="col-12">
                                <textarea id="sensorNodePropertiesModal-note" class="form-control" placeholder="Hier is Platz für Anmerkungen"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success w-100">Speichern</button>
                </div>
            </div>
        </div>
    </div>`;

    mw = createModal({keyboard: false}, html, id, onSuccessProxy, onDismissProxy);
    const form = mw.body.querySelector("form");
    mw.footer.querySelector("button").addEventListener('click', function (event) {
        if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
        } else {
            mw.close();
            onSuccessProxy();
        }
        form.classList.add('was-validated')
    }, false);
    heightInput = document.getElementById('sensorNodePropertiesModal-height');
    notesInput = document.getElementById('sensorNodePropertiesModal-note');
}

/**
 *
 * @param {SensorNodeRoomplanElement | int} element
 * the existing {@link RoomplanElement}
 * or the {@link OBJECT_TYPE} of the element to create;
 * either {@link OBJECT_TYPE.WINDOW} or {@link OBJECT_TYPE.DOOR}
 * @param {function(Object)} _onSuccess
 * @param {function()} _onDismiss
 */
export function openModal(element, _onSuccess, _onDismiss) {
    if (!(element instanceof SensorNodeRoomplanElement)) {
        // should not happen; better be safe than sorry
        return;
    }
    onSuccess = _onSuccess;
    onDismiss = _onDismiss;
    heightInput.value = element.data.height ? element.data.height : '1.2';
    notesInput.value = element.data.note ? element.data.note : '';

    mw.open();
}

function onSuccessProxy() {
    const properties = {
        height: parseFloat(heightInput.value),
        note: notesInput.value
    }
    onSuccess(properties);
    clear();
}

function onDismissProxy() {
    if (onDismiss) {
        onDismiss();
    }
    clear();
}

function clear() {
    const inputs = mw.body.getElementsByTagName('input');
    for (const input of inputs) {
        input.value = '';
    }
}

init();