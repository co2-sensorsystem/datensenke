import {createModal} from "../modalHelper.mjs";
import {RoomplanElement} from "../../../room/RoomplanElement.mjs";
import {OBJECT_TYPE} from "../../../../../config.js";
let initialized;
/**
 * @type ModalWrapper
 */
let mw;
/**
 * @type {function(Object)}
 */
let onSuccess;
/**
 * @type {function()}
 */
let onDismiss;
/**
 * @type HTMLInputElement
 */
let heightInput, widthInput, tiltedInput, turnedInput;
/**
 * @type NodeListOf<HTMLElement>
 */
let tiltedTypeInputs, turnedTypeInputs;
/**
 * @type HTMLTextAreaElement
 */
let notesInput;

export function init() {
    if (initialized) return;
    const id = 'windowDoorPropertiesModal';
    const html = `
    <div class="modal properties-modal" id="${id}" data-create-type="door" data-dialog-context="create" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span data-element-type="door">Tür</span>
                        <span data-element-type="window">Fenster</span>
                        <span data-dialog-context="create">erstellen</span>
                        <span data-dialog-context="edit">bearbeiten</span>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="opening-properties-form" class="needs-validation" novalidate>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-3">
                                <label for="windowDoorPropertiesModal-openingHeight" class="col-form-label">Höhe</label>
                            </div>
                            <div class="col-5">
                                <input type="number" min="0" max="25" step="0.01" id="windowDoorPropertiesModal-openingHeight" 
                                class="form-control" aria-describedby="windowDoorPropertiesModal-openingHeightHelp" required>
                            </div>
                            <div class="col-3">
                                <span id="windowDoorPropertiesModal-openingHeightHelp" class="form-text">
                                  in Meter
                                </span>
                            </div>
                        </div>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-3">
                                <label for="windowDoorPropertiesModal-openingWidth" class="col-form-label">Breite</label>
                            </div>
                            <div class="col-5">
                                <input type="number" min="0" max="25" step="0.01" id="windowDoorPropertiesModal-openingWidth" 
                                class="form-control" aria-describedby="windowDoorPropertiesModal-openingWidthHelp" required>
                            </div>
                            <div class="col-3">
                                <span id="windowDoorPropertiesModal-openingWidthHelp" class="form-text">
                                  in Meter
                                </span>
                            </div>
                        </div>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-3">
                                <label for="windowDoorPropertiesModal-openingTurned" class="col-form-label">Drehweite</label>
                            </div>
                            <div class="col-5">
                                <input type="number" min="0" max="90" step="0.01" id="windowDoorPropertiesModal-openingTurned" 
                                class="form-control" aria-describedby="windowDoorPropertiesModal-openingTurnedHelp" value="0" required>
                            </div>
                            <div class="col-3">
                                <div class="btn-group" role="group" id="windowDoorPropertiesModal-openingTurnedType">
                                    <input type="radio" class="btn-check" name="windowDoorPropertiesModal-openingTurnedType" 
                                    id="windowDoorPropertiesModal-openingTurnedTypeCm" autocomplete="on">
                                    <label class="btn btn-outline-primary" for="windowDoorPropertiesModal-openingTurnedTypeCm">cm</label>
                                
                                    <input type="radio" class="btn-check" name="windowDoorPropertiesModal-openingTurnedType" 
                                    id="windowDoorPropertiesModal-openingTurnedTypeDegrees" autocomplete="on" checked>
                                    <label class="btn btn-outline-primary" for="windowDoorPropertiesModal-openingTurnedTypeDegrees">Grad</label>
                                </div>
                            </div>
                        </div>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-3">
                                <label for="windowDoorPropertiesModal-openingTilted" class="col-form-label">Kippweite <span data-element-type="door">(nur für Kipp-Dreh-Türen)</span></label>
                            </div>
                            <div class="col-5">
                                <input type="number" min="0" max="25" step="0.01" id="windowDoorPropertiesModal-openingTilted" 
                                class="form-control" aria-describedby="windowDoorPropertiesModal-openingTiltedHelp" value="0" required>
                            </div>
                            <div class="col-3">
                                <div class="btn-group" role="group" id="windowDoorPropertiesModal-openingTiltedType">
                                    <input type="radio" class="btn-check" name="windowDoorPropertiesModal-openingTiltedType" id="windowDoorPropertiesModal-openingTiltedTypeCm" autocomplete="on" checked>
                                    <label class="btn btn-outline-primary" for="windowDoorPropertiesModal-openingTiltedTypeCm">cm</label>
                                
                                    <input type="radio" class="btn-check" name="windowDoorPropertiesModal-openingTiltedType" id="windowDoorPropertiesModal-openingTiltedTypeDegrees" autocomplete="on">
                                    <label class="btn btn-outline-primary" for="windowDoorPropertiesModal-openingTiltedTypeDegrees">Grad</label>
                                </div>
                            </div>
                        </div>
                        <div class="row g-3 mb-3 align-items-center">
                            <div class="col-12">
                                <label for="windowDoorPropertiesModal-openingNote">Anmerkungen</label>
                            </div>
                            <div class="col-12">
                                <textarea id="windowDoorPropertiesModal-openingNote" class="form-control" placeholder="Hier is Platz für Anmerkungen"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" form="opening-properties-form" class="btn btn-success w-100">Speichern</button>
                </div>
            </div>
        </div>
    </div>`;

    mw = createModal({keyboard: false}, html, id, onSuccessProxy, onDismissProxy);
    const form = mw.body.querySelector("form");
    mw.footer.querySelector("button").addEventListener('click', function (event) {
        if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
        } else {
            mw.close();
            onSuccessProxy();
        }
        form.classList.add('was-validated')
    }, false);
    widthInput = document.getElementById('windowDoorPropertiesModal-openingWidth');
    heightInput = document.getElementById('windowDoorPropertiesModal-openingHeight');
    tiltedInput = document.getElementById('windowDoorPropertiesModal-openingTilted');
    turnedInput = document.getElementById('windowDoorPropertiesModal-openingTurned');
    tiltedTypeInputs = document.getElementsByName('windowDoorPropertiesModal-openingTiltedType');
    turnedTypeInputs = document.getElementsByName('windowDoorPropertiesModal-openingTurnedType');
    notesInput = document.getElementById('windowDoorPropertiesModal-openingNote');
    for (let group of [tiltedTypeInputs, turnedTypeInputs]) {
        for (const input of group) {
            input.addEventListener('change', function () {
                // toggle the inputs manually
                // for some reason, this is not always done by Bootstrap...
                for (const i of group) {
                    i.checked = false;
                    i.removeAttribute("checked");
                }
                input.checked = true;
                input.setAttribute("checked", "");
            })
        }
    }
}

/**
 *
 * @param {DoorRoomplanElement | WindowRoomplanElement | OpeningRoomplanElement | int} element
 * the existing {@link RoomplanElement}
 * or the {@link OBJECT_TYPE} of the element to create;
 * either {@link OBJECT_TYPE.WINDOW} or {@link OBJECT_TYPE.DOOR}
 * @param {boolean} create
 * @param {function(Object)} _onSuccess
 * @param {function()} _onDismiss
 */
export function openModal(element, create, _onSuccess, _onDismiss) {
    onSuccess = _onSuccess;
    onDismiss = _onDismiss;
    let type = element;
    if (element instanceof RoomplanElement) {
        type = element.getObjectType();
        widthInput.value = element.data.width;
        heightInput.value = element.data.height;
        tiltedInput.value = parseFloat(element.data.tiltedOpening).toString(); // remove trailing "cm" or "deg"
        activateUnitButton(tiltedTypeInputs, element.data.tiltedOpening);
        turnedInput.value = parseFloat(element.opening.turnedOpening).toString();
        activateUnitButton(turnedTypeInputs, element.opening.turnedOpening);
        notesInput.value = element.data.notes;
        function activateUnitButton(buttonGroup, value) {
            if (value == null || value.length === 0 || typeof value === "number") return;
            buttonGroup.forEach(node => {
                if ((value.includes("cm") && node.id.includes("TypeCm"))
                    || value.includes("deg") && node.id.includes("TypeDegrees")) {
                    node.click();
                }
            })
        }
    }
    if (type !== OBJECT_TYPE.DOOR && type !== OBJECT_TYPE.WINDOW) {
        // invalid type
        return;
    }
    mw.div.setAttribute("data-dialog-context", create ? 'create': 'edit');
    mw.div.setAttribute("data-create-type", type === OBJECT_TYPE.DOOR ? 'door': 'window');
    mw.open();
}

function onSuccessProxy() {
    /**
     * @param {NodeListOf<HTMLInputElement>} inputs
     * @return {string}
     */
    function getType(inputs) {
        const input = Array.from(inputs).find((i) => i.checked);
        if (input.id.includes("Degrees")) {
            return "deg";
        } else {
            return "cm";
        }
    }

    const tilted = parseFloat(tiltedInput.value);
    const turned = parseFloat(turnedInput.value);
    const properties = {
        width: parseFloat(widthInput.value),
        height: parseFloat(heightInput.value),
        tiltedOpening: tilted === 0.0 || Number.isNaN(tilted) ? 0 : tilted + getType(tiltedTypeInputs),
        turnedOpening: turned === 0.0 || Number.isNaN(turned) ? 0 : turned + getType(turnedTypeInputs),
    }
    onSuccess(properties);
}

function onDismissProxy() {
    if (onDismiss) {
        onDismiss();
    }
}

function clear() {
    const inputs = mw.body.getElementsByTagName('input');
    for (const input of inputs) {
        input.value = '';
    }
}

init();