import {ContextMenu, ContextMenuOption} from "./ContextMenu.js";
import {
    AirFilterRoomplanElement,
    DoorRoomplanElement, ROOMPLAN_ELEMENTS, SensorNodeRoomplanElement,
    WindowRoomplanElement
} from "./RoomplanElement.mjs";
import * as MeasurementViewUi from "../measurement/MeasurementViewUi.mjs";
import {openModal as openOpeningElementPropertiesModal} from "../ui/modals/roomplanElement/openingProperties.mjs";
import {openModal as openSensorNodeElementPropertiesModal} from "../ui/modals/roomplanElement/sensorNodeProperties.mjs";
import {send} from "../CommunicationHandler.mjs";
import {Opening} from "../simulation/AirExchange.mjs";
import {FINEDUSTSENSOR_STATES, LED_STATES, MODE, OBJECT_TYPE} from "../../../config.js";

let windowCount = 0, doorCount = 0, airFilterCount = 0;

/**
 * Creates a {@link HTMLDivElement} for the passed {@link RoomplanElement},
 * initializes it with default values and classes and adds it to the roomplan.
 * @param {RoomplanElement} incompleteEl
 * @param {HTMLDivElement} roomplan The roomplan to which the {@link RoomplanElement}
 * is going to be added.
 * @param {Controller} controller
 * @return {HTMLDivElement} The created DIV for the passed {@link RoomplanElement}
 */
function prepareRoomplanElement(incompleteEl, roomplan, controller) {
    const outerElement = document.createElement('div');
    outerElement.classList.add("roomplan-element");
    outerElement.style.left = incompleteEl.position.x + '%';
    outerElement.style.top = incompleteEl.position.y + '%';
    outerElement.id = incompleteEl.id;
    outerElement.draggable = controller.mode === MODE.CONFIGURATION;
    outerElement.addEventListener('app.mode',
        /** @param event {ModeChangeEvent} */ (event) => {
            outerElement.draggable = event.newMode === MODE.CONFIGURATION;
        });

    function createIconElement (iconClass) {
        const iconElement = document.createElement('i');
        if (iconClass.startsWith("bi-")) {
            iconElement.classList.add("bi");
        }
        iconElement.classList.add("icon", iconClass);
        outerElement.append(iconElement);
    }

    if (incompleteEl instanceof WindowRoomplanElement) {
        outerElement.classList.add("window");
        createIconElement("sni-window-closed");
        createIconElement("sni-window-tilted");
        createIconElement("sni-window-open");
    } else if (incompleteEl instanceof DoorRoomplanElement) {
        outerElement.classList.add("door");
        createIconElement("bi-door-closed-fill");
        createIconElement("bi-door-open-fill");
    } else if (incompleteEl instanceof AirFilterRoomplanElement) {
        outerElement.classList.add("airFilter");
        createIconElement("bi-filter-square");
        createIconElement("bi-filter-square-fill");
    } else if (incompleteEl instanceof SensorNodeRoomplanElement) {
        outerElement.classList.add('sensor');
        outerElement.innerHTML = `<div><div>${incompleteEl.data.id}</div></div>`;
    }
    roomplan.append(outerElement);
    incompleteEl.setNode(outerElement);
    incompleteEl.setState(incompleteEl.state);
    return outerElement;
}


/**
 * @param {RoomplanElement} el
 * @param {string} name
 * @param {Controller} controller
 * @return {ContextMenuOption}
 */
function createDeleteElementContextMenuOption(el, name, controller) {
    return new ContextMenuOption(
        "bi-trash",
        name + " entfernen",
        function () {
            return controller.mode === MODE.CONFIGURATION
        },
        function () {
            el.delete.call(el);
        });
}

/**
 * @param {OpeningRoomplanElement} el
 * @param {string} name
 * @param {Object} controller
 * @param {string} controller.mode
 * @return {ContextMenuOption}
 */
function createEditElementPropertiesContextMenuOption(el, name, controller) {
    return new ContextMenuOption(
        "bi-pen",
        name + " bearbeiten",
        function () {
            return controller.mode === MODE.CONFIGURATION
        },
        function () {
            openOpeningElementPropertiesModal(el, false,
                (o) => {
                    el.data.height = o.height;
                    el.data.width = o.width;
                    el.data.tiltedOpening = o.tiltedOpening;
                    el.data.turnedOpening = o.turnedOpening;
                    el.opening.height = o.height;
                    el.opening.width = o.width;
                    el.opening.tiltedOpening = o.tiltedOpening;
                    el.opening.turnedOpening = o.turnedOpening;
                }, () => {/* do nothing, because editing was canceled */})
        });
}

/**
 * @param {RoomplanElement} incompleteEl
 * @param {HTMLDivElement} roomplan
 * @param {null} incompleteEl.node
 * @param {ContextMenuHandler} contextMenuHandler
 * @param {Controller} controller
 * @return {void}
 */
function createAirFilter(incompleteEl, roomplan, contextMenuHandler, controller) {
    // create element and add it to roomplan
    const airFilterElement = prepareRoomplanElement(incompleteEl, roomplan, controller);

    // add context menu
    incompleteEl.contextMenu = new ContextMenu(airFilterElement, contextMenuHandler, incompleteEl, [
        new ContextMenuOption(
            "bi-filter-square-fill",
            "Luftfilter anschalten",
            function () {
                return this.state === AirFilterRoomplanElement.prototype.STATES.OFF
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(AirFilterRoomplanElement.prototype.STATES.ON)
            }),
        new ContextMenuOption(
            "bi-filter-square",
            "Luftfilter ausschalten",
            function () {
                return this.state === AirFilterRoomplanElement.prototype.STATES.ON
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(AirFilterRoomplanElement.prototype.STATES.OFF)
            }),
        createDeleteElementContextMenuOption(incompleteEl, "Luftfilter", controller)
    ]);

}

/**
 * @param {RoomplanElement} incompleteEl
 * @param {HTMLDivElement} roomplan
 * @param {null} incompleteEl.node
 * @param {ContextMenuHandler} contextMenuHandler
 * @param {Controller} controller
 * @return {void}
 */
function createDoor(incompleteEl, roomplan, contextMenuHandler, controller) {
    // create element and add it to roomplan
    const doorElement = prepareRoomplanElement(incompleteEl, roomplan, controller);

    // create context menu
    new ContextMenu(doorElement, contextMenuHandler, incompleteEl, [
        new ContextMenuOption(
            "bi-door-closed-fill",
            "Tür geschlossen",
            function () {
                return this.state === DoorRoomplanElement.prototype.STATES.OPEN
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(DoorRoomplanElement.prototype.STATES.CLOSED);
            }),
        new ContextMenuOption(
            "bi-door-open-fill",
            "Tür geöffnet",
            function () {
                return this.state === DoorRoomplanElement.prototype.STATES.CLOSED
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(DoorRoomplanElement.prototype.STATES.OPEN)
            }),
        createEditElementPropertiesContextMenuOption(incompleteEl, "Tür", controller),
        createDeleteElementContextMenuOption(incompleteEl, "Tür", controller)
    ]);
}

/**
 * @param {RoomplanElement} incompleteEl
 * @param {HTMLDivElement} roomplan
 * @param {null} incompleteEl.node
 * @param {ContextMenuHandler} contextMenuHandler
 * @param {Controller} controller
 * @return {void}
 */
function createWindow(incompleteEl, roomplan, contextMenuHandler, controller) {
    // create element and add it to roomplan
    const windowElement = prepareRoomplanElement(incompleteEl, roomplan, controller);

    new ContextMenu(windowElement, contextMenuHandler, incompleteEl, [
        new ContextMenuOption(
            "sni-window-closed",
            "Fenster geschlossen",
            function () {
                return this.state !== WindowRoomplanElement.prototype.STATES.CLOSED
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(WindowRoomplanElement.prototype.STATES.CLOSED);
            }),
        new ContextMenuOption(
            "sni-window-tilted",
            "Fenster gekippt",
            function () {
                return this.state !== WindowRoomplanElement.prototype.STATES.TILTED
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(WindowRoomplanElement.prototype.STATES.TILTED)
            }),
        new ContextMenuOption(
            "sni-window-open",
            "Fenster geöffnet",
            function () {
                return this.state !== WindowRoomplanElement.prototype.STATES.OPEN
                    && controller.mode !== MODE.VIEWER
            },
            () => {
                incompleteEl.setState(WindowRoomplanElement.prototype.STATES.OPEN)
            }),
        createEditElementPropertiesContextMenuOption(incompleteEl, "Fenster", controller),
        createDeleteElementContextMenuOption(incompleteEl, "Fenster", controller)
    ]);
}

function createSensorNode(incompleteEl, roomplan, contextMenuHandler, controller) {
    const node = prepareRoomplanElement(incompleteEl, roomplan, controller);
    node.onclick = (event) => MeasurementViewUi.onClickSensor(event);
    const LEDMessage = {
        event: "trafficLight",
        id: incompleteEl.data.id
    }
    const doNothing = {
        onSuccess: () => {},
        onError: () => {}
    }
    const FineDustSensorMessage = {
        event: "fineDustSensor",
        id: incompleteEl.data.id
    }
    new ContextMenu(node, contextMenuHandler, incompleteEl, [
        new ContextMenuOption(
            "bi-pen",
            "Sensoreigenschaften bearbeiten",
            function () {
                return controller.mode === MODE.CONFIGURATION;
            },
            () => {
                openSensorNodeElementPropertiesModal(incompleteEl,
                        properties => {
                    incompleteEl.getSensor().height = properties.height;
                    incompleteEl.getSensor().note = properties.note;
                    MeasurementViewUi.updateUiWithSensor(incompleteEl.getSensor());
                },
                    () => {})
            }),
        new ContextMenuOption(
            "bi-brightness-high-fill",
            "LED-Ampel anschalten",
            function () {
                return controller.mode === MODE.MEASUREMENT && incompleteEl.data.LED === LED_STATES.OFF;
            },
            () => {
                incompleteEl.data.LED = LED_STATES.ON;
                LEDMessage.state = LED_STATES.ON
                send(LEDMessage, doNothing);
            }),
        new ContextMenuOption(
            "bi-circle",
            "LED-Ampel abschalten",
            function () {
                return controller.mode === MODE.MEASUREMENT && incompleteEl.data.LED === LED_STATES.ON;
            },
            () => {
                incompleteEl.data.LED = LED_STATES.OFF;
                LEDMessage.state = LED_STATES.OFF
                send(LEDMessage, doNothing);
            }),
        new ContextMenuOption(
            "bi-brightness-high-fill",
            "Feinstaubsensor anschalten",
            function () {
                return controller.mode === MODE.MEASUREMENT && incompleteEl.data.FINEDUSTSENSOR === FINEDUSTSENSOR_STATES.OFF;
            },
            () => {
                incompleteEl.data.FINEDUSTSENSOR = FINEDUSTSENSOR_STATES.ON;
                FineDustSensorMessage.state = FINEDUSTSENSOR_STATES.ON
                send(FineDustSensorMessage, doNothing);
            }),
        new ContextMenuOption(
            "bi-circle",
            "Feinstaubsensor abschalten",
            function () {
                return controller.mode === MODE.MEASUREMENT && incompleteEl.data.FINEDUSTSENSOR === FINEDUSTSENSOR_STATES.ON;
            },
            () => {
                incompleteEl.data.FINEDUSTSENSOR = FINEDUSTSENSOR_STATES.OFF;
                FineDustSensorMessage.state = FINEDUSTSENSOR_STATES.OFF
                send(FineDustSensorMessage, doNothing);
            }),
    ]);
}

/**
 * Create the UI part for a new {@link RoomplanElement} and add it to the roomplan.
 * @param {Object} incompleteElement
 * @param {int} type
 * @param {HTMLDivElement} roomplan
 * @param {ContextMenuHandler} contextMenuHandler
 * @param {Controller} controller
 * @type function
 * @return {void}
 */
function createRoomplanElementDOM(incompleteElement, type, roomplan, contextMenuHandler, controller) {
    switch (type) {
        case OBJECT_TYPE.AIR_FILTER:
            createAirFilter(incompleteElement, roomplan, contextMenuHandler, controller); return;
        case OBJECT_TYPE.DOOR:
            createDoor(incompleteElement, roomplan, contextMenuHandler, controller); return;
        case OBJECT_TYPE.WINDOW:
            createWindow(incompleteElement, roomplan, contextMenuHandler, controller); return;
        case OBJECT_TYPE.SENSOR:
            createSensorNode(incompleteElement, roomplan, contextMenuHandler, controller); return;
    }

}

/**
 *
 * @param {HTMLDivElement} roomplan
 * @param {RoomplanElement | number} type the type of {@link RoomplanElement} to create;
 *                                        can be a number from {@link OBJECT_TYPE}
 * @param {Coordinate} position
 * @param {string} id
 * @param {Object | Sensor} data
 * @param {ContextMenuHandler} contextMenuHandler
 */
export function createRoomplanElement(roomplan, type, position,
                                      id = undefined, data = {}, contextMenuHandler, controller) {
    let germanName, roomplanElementArray, roomplanElementClass, opening;
    if (type === DoorRoomplanElement || type === OBJECT_TYPE.DOOR) {
        germanName = "Tür";
        roomplanElementArray = ROOMPLAN_ELEMENTS.doors;
        roomplanElementClass = DoorRoomplanElement;
        if (id === undefined) {
            const num = ++doorCount;
            id = "door-" + num;
        }
        opening = Opening.deserialize(data);
    } else if (type === WindowRoomplanElement || type === OBJECT_TYPE.WINDOW) {
        germanName = "Fenster";
        roomplanElementArray = ROOMPLAN_ELEMENTS.windows;
        roomplanElementClass = WindowRoomplanElement;
        if (id === undefined) {
            const num = ++windowCount;
            id = "window-" + num;
        }
        opening = Opening.deserialize(data);
    } else if (type === AirFilterRoomplanElement || type === OBJECT_TYPE.AIR_FILTER) {
        germanName = "Luftfilter";
        roomplanElementArray = ROOMPLAN_ELEMENTS.airFilters;
        roomplanElementClass = AirFilterRoomplanElement;
        if (id === undefined) {
            const num = ++airFilterCount;
            id = "airFilter-" + num;
        }
    } else if (type === SensorNodeRoomplanElement || type === OBJECT_TYPE.SENSOR) {
        germanName = "Sensor";
        roomplanElementArray = ROOMPLAN_ELEMENTS.sensors;
        roomplanElementClass = SensorNodeRoomplanElement;
        if (id === undefined) {
            id = "sensor-" + data.id;
        }
    } else {
        // something is wrong here
        return;
    }

    let name;
    if (data.hasOwnProperty('name')) {
        name = data.name;
    } else {
        name = germanName + " "+ id.split("-")[1];
    }

    let state;
    if (opening) {
        if (opening.isClosed) {
            state = WindowRoomplanElement.prototype.STATES.CLOSED;
        } else if (opening.isTilted) {
            state = WindowRoomplanElement.prototype.STATES.TILTED;
        } else {
            state = WindowRoomplanElement.prototype.STATES.OPEN;
        }
    }

    /**
     * @type {RoomplanElement}
     */
    let roomplanElement;
    if (opening) {
        roomplanElement = new roomplanElementClass(
            position, id, name, data, state, null, controller, opening);
    } else {
        roomplanElement = new roomplanElementClass(
            position, id, name, data, state, null, controller);
    }
    createRoomplanElementDOM(
        roomplanElement, roomplanElement.getObjectType(), roomplan, contextMenuHandler, controller);
    roomplanElementArray.push(roomplanElement);
    return roomplanElement;
}
