import {createElementFromHTML, hexStringToArrayBuffer, sleep} from "../utils.mjs";
import {createModal} from "../ui/modals/modalHelper.mjs";
import {createRoomplanElement} from "./RoomplanElementFactory.mjs";
import {
    AirFilterRoomplanElement,
    clearAllElements,
    DoorRoomplanElement,
    findElement,
    getAllElements,
    OpeningRoomplanElement,
    removeElement,
    ROOMPLAN_ELEMENTS,
    WindowRoomplanElement
} from "./RoomplanElement.mjs";
import {ContextMenu, ContextMenuOption} from "./ContextMenu.js";
import * as CommunicationHandler from "../CommunicationHandler.mjs";
import {getRooms, updateRoomList} from "../ui/modals/SelectRoomModal.mjs";
import {openModal as openWindowDoorPropertiesModal} from '../ui/modals/roomplanElement/openingProperties.mjs';
import {MODE, OBJECT_TYPE} from "../../../config.js";
import  * as ErrorModal from "../ui/modals/ErrorModal.mjs";

/**
 * Whether the current room is new and needs to be created when stored in the database.
 * @type {boolean}
 */
let createRoom;
/**
 * Whether new {@link OpeningRoomplanElement}s were added to this roomplan.
 * @type {boolean}
 */
export let wasOpeningElementAddedManually = false;
/**
 * @type {Room | null}
 */
let room;
/**
 * @type {HTMLDivElement}
 */
let editor, roomplan, roomplanContainer, roomplanBackgroundImage;
/**
 * @type {HTMLInputElement}
 */
let nameInput, lengthInput, widthInput, heightInput, volumeInput, imageInput;
/**
 * @type {ModalWrapper | null}
 */
let modalWrapper;

/**
 * @type {ContextMenuHandler}
 */
let contextMenuHandler;

let controller;

/**
 * @type {function(Room) | undefined}
 */
let onSave;
/**
 * @param {Object} _controller
 * @param {string} _controller.mode
 * @param {ContextMenuHandler} _contextMenuHandler
 * @param {boolean} useJustAsRoomEditor
 * @param {HTMLDivElement} [propertyContainerWrapper]
 * @param {HTMLDivElement} [roomplanContainerWrapper]
 */
export function init(_controller, _contextMenuHandler, useJustAsRoomEditor, propertyContainerWrapper, roomplanContainerWrapper) {
    controller = _controller;
    contextMenuHandler = _contextMenuHandler;
    const roomProperties = `
        <div id="room-properties">
            <form id="room-properties-form" class="needs-validation" novalidate>
                <div class="row mb-3" data-room-info="true">
                    <label class="col-12 col-md-4 col-form-label" for="room-property-name">Raumname: </label>
                    <div class="col"><input class="form-control" type="text" name="room-property-name" required/></div>
                </div>
                <div class="row mb-3" data-room-info="true">
                    <label class="col-12 col-md-4 col-form-label" for="room-property-width">Breite (m):</label>
                    <div class="col"><input class="form-control" type="number" min="1" max="1000" step="0.01" name="room-property-width" value="10"  required/></div>
                </div>
                <div class="row mb-3" data-room-info="true">
                    <label class="col-12 col-md-4 col-form-label" for="room-property-length">Länge (m):</label>
                    <div class="col"><input class="form-control" type="number" min="1" max="1000" step="0.01" name="room-property-length" value="10"  required/></div>
                </div>
                <div class="row mb-3" data-room-info="true">
                    <label class="col-12 col-md-4 col-form-label" for="room-property-height">Höhe (m):</label>
                    <div class="col"><input class="form-control" type="number" min="0.5" max="100" step="0.01" name="room-property-height" value="3"  required/></div>
                </div>
                <div class="row mb-3" data-room-info="true">
                    <label class="col-12 col-md-4 col-form-label" for="room-property-volume">Volumen (m³):</label>
                    <div class="col"><input class="form-control" type="number" step="0.01" name="room-property-volume" value="300"  required/></div>
                    <div class="col-auto d-flex" style="align-items: center; margin-right: 1rem"><i class="bi-calculator-fill" title="berechnen" data-api-action="calculate-room-property-volume" ></i></div>
                </div>
                <div class="row mb-3" data-room-info="true">
                <label class="col-12 col-md-4 col-form-label" for="room-property-volume">Grundriss:</label>
                    <div class="col d-flex align-items-center">
                        <input type="file" accept="image/*" id="roomplan-image-input" name="room-property-roomplan-image" class="form-control" />
                        <span class="ms-3">unverändert</span>
                    </div>
                </div>
            </form>
        </div>
    `;
    const roomplanC = `
        <div id="roomplan-container" class="hidden-with-content-graph">
            <div id="roomplan-background-placeholder"></div>
            <img id="roomplan-background-image" src="" alt="" />
            <div id="roomplan"><!-- roomplan elements are going to be placed here --></div>
        </div>
    `;


    // either create the editor inside a modal or append it to the specified
    if (useJustAsRoomEditor) {
        createRoomEditorModal(roomplanC , roomProperties)
    } else {
        if (propertyContainerWrapper != null) {
            propertyContainerWrapper.prepend(createElementFromHTML(roomProperties));

        }
        if (roomplanContainerWrapper != null) {
            roomplanContainerWrapper.prepend(createElementFromHTML(roomplanC));

        }
    }

    editor = document.getElementById('room-editor');
    roomplan = document.getElementById('roomplan');
    roomplanContainer = document.getElementById('roomplan-container');
    roomplanBackgroundImage = document.getElementById('roomplan-background-image');

    if (propertyContainerWrapper != null || useJustAsRoomEditor) {
        const container = propertyContainerWrapper == null ? editor : propertyContainerWrapper;
        nameInput = container.querySelector('input[name="room-property-name"]');
        widthInput = container.querySelector('input[name="room-property-width"]');
        lengthInput = container.querySelector('input[name="room-property-length"]');
        heightInput = container.querySelector('input[name="room-property-height"]');
        volumeInput = container.querySelector('input[name="room-property-volume"]');

        for (const input of [nameInput, widthInput, lengthInput, heightInput, volumeInput]) {
            input.onchange = () => {
                changeRoom(getRoom(), false, false, false)
            }
        }
        imageInput = container.querySelector('input[name="room-property-roomplan-image"]');
        imageInput.onchange = function () {
            imageInput.classList.remove("unchanged");
            addRoomPlanImage();
        }

    }
    const form = document.getElementById("room-properties-form");
    document.querySelectorAll('[data-api-action="calculate-room-property-volume"]').forEach(
        (d) => {d.addEventListener('click', () => calculateRoomVolume())});
    document.querySelectorAll('[data-api-action="room-editor-save"]').forEach(
        (d) => {d.addEventListener('click', (event) => {
            if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
            } else {
                saveRoom(true).then(
                    () => modalWrapper.close(),
                    e => {
                        ErrorModal.open("Raum Speichern", e, "save-room")
                    }
                )
            }
            form.classList.add('was-validated')
        })});

    roomplan.addEventListener('dragover', onDragElementOverRoomplan);
    roomplan.addEventListener('drop', onDropElementOnRoomplan);

    // variable is unused, but the Object is bound to other Objects
    // noinspection JSUnusedLocalSymbols
    const contextMenu = new RoomplanContextMenu(controller, contextMenuHandler, !useJustAsRoomEditor);
    window.onresize = resizeRoomplanArea;
}

/**
 *
 * @param {string} roomplanC
 * @param {string} roomProperties
 */
function createRoomEditorModal(roomplanC, roomProperties) {
    const id = 'room-editor-modal';
    const dialog = `
        <div class="modal fade" id="${id}" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
              <div class="modal-header border-0">
                <h5 class="modal-title" id="exampleModalLabel">Raum bearbeiten</h5>
                <button type="button" class="btn btn-sm btn-danger me-3" style="margin-left: auto" data-bs-dismiss="modal">Abbrechen</button>
                <button type="button" id="save-room-properties" form="room-properties-form" class="btn btn-sm btn-success" data-api-action="room-editor-save">Speichern</button>
              </div>
              <div class="modal-body p-0">
                <div class="container-fluid g-0 h-100" id="room-editor">
                  <div class="row g-0 h-100">
                    <div id="roomplanContainerWrapper" class="col">
                      ${roomplanC}
                    </div>
                    <div id="sidebar" class="col-3 col-md-4">
                      <div>${roomProperties}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer d-none">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
    `;
    modalWrapper = createModal({keyboard: false}, dialog, id, () => {}, () => {});
    document.getElementById(id).addEventListener('shown.bs.modal', resizeRoomplanArea);
}

/**
 * Open the modal to edit or create a room.
 * @param {Room | null} _room the room to edit. If <code>null</code>, a new room will be created.
 */
export function openModal(_room, changeElements = true) {
    createRoom = _room == null;
    changeRoom(_room, changeElements, false);
    modalWrapper?.open();
    // the roomplan will be resized once again once the modal is shown
}

/**
 * @param _createRoom {boolean}
 */
export function setCreateRoom(_createRoom) {
    createRoom = _createRoom;
}

export function generateRoomplanElements(_room) {
    changeRoom(_room, true, false);
    _room.elements = getRoom().elements;
}

/**
 *
 * @param {Room | null} _room
 * @param {boolean} changeElements
 * @param {boolean} [globalElements] whether to display the new room's <code>elements</code>
 * or the global {@link ROOMPLAN_ELEMENTS}
 * @param {boolean} [forceLoadImage] whether the roomplan's background image must be changed and retrieved from the server
 */
export function changeRoom(_room, changeElements, globalElements = false, forceLoadImage = true) {
    if (changeElements) {
        clear();
    }

    // create a default room if no room is going to be set
    if (_room == null) {
        room = {
            name: "",
            width: 10,
            length: 10,
            height: 3,
            volume: 300,
            image: null,
            elements: []
        };
    } else {
        room = _room;
    }

    if (widthInput != null) {
        nameInput.value = room.name;
        widthInput.value = room.width;
        lengthInput.value = room.length;
        heightInput.value = room.height;
        volumeInput.value = room.volume;
        if (typeof room.image === "string") {
            imageInput.classList.add('unchanged');
        }
    }

    let hbi = document.getElementById('roomplan-background-image');
    let hbp = document.getElementById('roomplan-background-placeholder');


    if (room.image == null || forceLoadImage) {
        hbi.classList.add("d-none")
        hbp.classList.remove("d-none")
    }
    if (room.image != null && forceLoadImage) {
        hbi.addEventListener("load", () => {
            hbp.classList.add("d-none")
            hbi.classList.remove("d-none")
        })
        if (typeof room.image === "string") {
            hbi.src = room.image + "?" + Date.now();
        } else if (typeof room.image === "object" && room.image.hasOwnProperty("base64")) {
            hbi.src = room.image.base64;
        }

    }

    resizeRoomplanArea();

    if (changeElements) {
        const roomplanElements = globalElements ? getAllElements() : room.elements;
        for (const roomEl of roomplanElements) {
            createRoomplanElement(
                roomplan, roomEl.type || roomEl.getObjectType(),
                roomEl.position, roomEl.id, roomEl.data,
                contextMenuHandler, controller)
        }
    }

    // TODO: update all values if possible
}

export function clear() {
    console.log("createRoom: ", createRoom);
    room = null;
    if (nameInput != null) {
        for (const input of [widthInput, lengthInput, heightInput, volumeInput]) {
            if (input.type == 'number') {
                input.value = 100;
            }
        }
        nameInput.value = "";
    }

    clearAllElements();

    ROOMPLAN_ELEMENTS.doors.forEach((e) => e.delete());
    ROOMPLAN_ELEMENTS.windows.forEach((e) => e.delete());
    // should not have been touched, but clearing anyway
    ROOMPLAN_ELEMENTS.airFilters.forEach((e) => e.delete());
    ROOMPLAN_ELEMENTS.sensors.forEach((e) => e.delete());

    wasOpeningElementAddedManually = false;

    // clear everything else, e.g. context menus
    roomplan.innerHTML = '';
}

export function clearStandardOpenings() {
    room?.elements.forEach(re => {
        if (re instanceof OpeningRoomplanElement) {
            removeElement(re, re.type);
            re.delete();
        } else if (re.type === OBJECT_TYPE.DOOR || re.type === OBJECT_TYPE.WINDOW) {
            removeElement(findElement(re.objectId, re.type), re.type);
        }
    })
}

function calculateRoomVolume() {
    const width = parseFloat(widthInput.value);
    const length = parseFloat(lengthInput.value);
    const height = parseFloat(heightInput.value);
    volumeInput.value = (width * length * height).toString();
}

/**
 *
 * @return {Room}
 */
export function getRoom() {
    return {
        id: room?.id,
        width: parseFloat(widthInput.value),
        length: parseFloat(lengthInput.value),
        height: parseFloat(heightInput.value),
        volume: parseFloat(volumeInput.value),
        name: nameInput.value,
        image: room?.image,
        elements: [].concat(ROOMPLAN_ELEMENTS.doors, ROOMPLAN_ELEMENTS.windows)
    }
}

/**
 * @return {number | Promise<number | null>}
 */
export function getRoomId() {
    if (room != null) {
        return room.id
    } else {
        return null;
    }
    return new Promise(function () {
        if (room == null) {
            return null;
        } else if (room.id != null) {
            return room.id;
        } else {
            return saveRoom(r => r.id, rej => null);
        }

    })

}

/**
 * @param returnPromise {boolean}
 * @return {void | Promise<number|null>}
 */
export function saveRoom(returnPromise = false) {
    room = getRoom();
    const roomplanImage = room.image;
    const updateRoomImage = room.image != null && typeof room.image === "object";
    if (updateRoomImage) {
        const ep = room.image.name.split(".");
        const fileExtension = ep[ep.length - 1];
        room.image = "room-" + room.id + "." + fileExtension;
    }
    const roomObj = Object.assign(room);
    roomObj.elements = room.elements.map(e => e.getDBInfo());
    const data = {
        event: (createRoom ? "createRoom" : "updateRoom"), // create a new room or update the current one
        room: room
    };
    let roomId = undefined;
    function save() {
        CommunicationHandler.send(data, {
            context: null,
            onSuccess: (data) => {
                if (data.hasOwnProperty("id")) {
                    updateRoomList(() => {
                        roomId = data.id;
                        const _room = getRooms().find((r) => r.id === data.id);
                        controller.updateRoomImage(_room);
                        if (updateRoomImage) {
                            sendRoomplanImage(_room, roomplanImage);
                        }
                    });
                }
            },
            onError: (error) => {
                roomId = null;
                ErrorModal.open("Konnte Raum nicht speichern", error, "save-room");
            },
        })
        if (onSave != null) {
            // use fresh room to have actual RoomplanElements and not their serialized version
            onSave(getRoom());
        }
    }

    if (returnPromise) {
        return new Promise(async function (resolve, reject) {
            save();
            while (roomId === undefined) {
                await sleep(50)

            }
            if (roomId == null) {
                return reject;
            } else {
                return resolve(roomId);
            }
        });
    } else {
        save();
        createRoom = false;
    }

}

/**
 * Load roomplan image from input field and update the image on the roomplan accordingly.
 */
export function addRoomPlanImage() {
    const file = imageInput.files[0];
    if (file == null) {
        return;
    }
    room.image = {
        name: file.name
    };

    // Load the binary format representation of the image to be able to save it as a file later.
    const binReader = new FileReader();
    binReader.onload = function(e) {
        room.image.bin = e.target.result;
    }
    binReader.readAsArrayBuffer(file);

    // Load the data URL representation of the image and update the image UI with it.
    const dataUrlReader = new FileReader();
    dataUrlReader.onload = function (e) {
        room.image.base64 = e.target.result;
        changeRoom(room, false);
    }
    dataUrlReader.readAsDataURL(file);
}

/**
 * @param {function(Room) | null} _onSave
 */
export function setOnSave(_onSave) {
    onSave = _onSave;
}

class RoomplanContextMenu extends ContextMenu {
    constructor(controller, contextMenuHandler, displayAirFilterElement) {
        const options = [
            new ContextMenuOption("bi-door-closed-fill", "Tür platzieren",
                () => controller.mode === MODE.CONFIGURATION,
                function () {
                    openWindowDoorPropertiesModal(OBJECT_TYPE.DOOR, true, (properties) => {
                        wasOpeningElementAddedManually = true;
                        createRoomplanElement.call(
                            controller, roomplan, DoorRoomplanElement, this.menu.position,
                            undefined, properties, contextMenuHandler, controller)
                    }, () => {/* just cancel the creation */});
                }),
            new ContextMenuOption("sni-window-closed", "Fenster platzieren",
                () => controller.mode === MODE.CONFIGURATION,
                function () {
                    openWindowDoorPropertiesModal(OBJECT_TYPE.WINDOW, true, (properties) => {
                        wasOpeningElementAddedManually = true;
                        createRoomplanElement.call(
                            controller, roomplan, WindowRoomplanElement, this.menu.position,
                            undefined, properties, contextMenuHandler, controller)
                    }, () => {/* just cancel the creation */});
                }),
            new ContextMenuOption("bi-filter", "Luftfilter platzieren",
                () => controller.mode === MODE.CONFIGURATION && displayAirFilterElement,
                function () {
                    createRoomplanElement.call(controller, roomplan, AirFilterRoomplanElement, this.menu.position, undefined, {}, contextMenuHandler, controller)
                })
        ]
        super(roomplan, contextMenuHandler, null, options);
    }
}

export function resizeRoomplanArea() {
    if (room == null) return;

    roomplanContainer.style.height = "1px" // if height is 0, the offsetWidth is also 0
    roomplanContainer.style.width = "100%";
    const footerHeight = document.getElementById("footer")?.offsetHeight || 0;
    const navHeight = document.getElementById("nav")?.offsetHeight
        || document.querySelector('#room-editor-modal .modal-header')?.offsetHeight
        || 0;
    let maxHeight = document.documentElement.clientHeight - (footerHeight + navHeight);
    let autoHeight = roomplanContainer.offsetWidth * room.length / room.width;
    if (autoHeight < maxHeight) {
        roomplanContainer.style.height = autoHeight + "px";
        roomplan.style.height = "auto";
    } else {
        roomplanContainer.style.width
            = maxHeight / (room.length / room.width) + 'px';
        roomplanContainer.style.height
            = roomplanContainer.offsetWidth * room.length / room.width + 'px';
    }
}

function onDragElementOverRoomplan(event) {
    event.preventDefault()
}

function onDropElementOnRoomplan(event) {
    event.preventDefault();
    event.dataTransfer.dropEffect = "move";
    const elementType = parseInt(event.dataTransfer.getData("type"));
    const elementId = event.dataTransfer.getData("id");

    let newX, newY;
    if (event.target === event.currentTarget) { // dropped directly on roomplan
        newX = event.offsetX;
        newY = event.offsetY;
    } else { // dropped on other roomplan element
        // calculate the new position on the roomplan
        const currentTargetRect = roomplan.getBoundingClientRect();
        newX = event.pageX - currentTargetRect.left;
        newY = event.pageY - currentTargetRect.top;
    }
    const newPosition = {
        x: (newX / roomplan.offsetWidth) * 100,
        y: (newY / roomplan.offsetHeight) * 100
    }

    const element  = findElement(elementId, elementType);
    if (element !== undefined) {
        element.changePosition(newPosition);
    }

}

/**
 * Send the roomplan image to the backend and store it there for later use.
 * @param _room
 * @param {Object} roomplanImage
 * @param {ArrayBuffer} roomplanImage.bin
 */
export function sendRoomplanImage(_room, roomplanImage) {
    const ep = _room.image.split(".");
    const extension = ep[ep.length - 1];
    const metaData = {
        event: "roomplanImage",
        extension: extension,
    };
    if (_room.id != null) {
        metaData.id = _room.id;
    }

    /**
     * @param {string} uploadId
     */
    function sendImage(uploadId) {
        const uploadIdBuffer = hexStringToArrayBuffer(uploadId);
        const tmpBuffer = new Uint8Array(uploadIdBuffer.byteLength + roomplanImage.bin.byteLength);
        tmpBuffer.set(new Uint8Array(uploadIdBuffer), 0);
        tmpBuffer.set(new Uint8Array(roomplanImage.bin), uploadIdBuffer.byteLength);
        CommunicationHandler.send(tmpBuffer.buffer, {
            context: null,
            onSuccess: () => {
                _room.image = "rooms/" + metaData.name;
            },
            onError: (error) => {console.log("Bild nicht gespeichert", error)},
        }, "file", "file" + uploadId);
    }

    // send the textual info and request an upload ID
    CommunicationHandler.send(metaData, {
        context: null,
        onSuccess: (uploadId) => {
            sendImage(uploadId);
        },
        onError: (error) => {console.log("metadaten nicht gesendet", error);}
    })

}
