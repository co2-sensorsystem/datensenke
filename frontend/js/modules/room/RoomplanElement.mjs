import * as MeasurementViewUi from "../measurement/MeasurementViewUi.mjs";
import {
    EVENT,
    EventHandler,
    EventTypeToState,
    RoomplanElementEvent
} from "../EventHandler.mjs";
import {MODE, OBJECT_TYPE} from "../../../config.js";

export class RoomplanElement {
    /**
     *
     */
    #controller;
    /**
     * The element's relative position (in percent) on the room plan.
     * @type {Coordinate}
     */
    position;
    /**
     * This element's ID used by DOM and database.
     * @type {string}
     */
    _id;
    /**
     * Description displayed in the UI
     * @type {string}
     */
    _name;
    /**
     * @type {Object}
     */
    data = {};
    /**
     * The corresponding DOM element.
     * @type {HTMLDivElement}
     */
    node;
    /**
     * @type {ContextMenu}
     */
    contextMenu;
    /**
     * Current element state.
     */
    state;
    /**
     * List of possible states.
     * Set for each {@link RoomplanElement} type.
     * @type {Object}
     */
    STATES = [];
    /**
     * Timestamps are keys, objects are the corresponding {@link RoomplanElementEvent}s.
     * @type {{number: RoomplanElementEvent}}
     */
    events = {};

    /**
     *
     * @param {Coordinate} position
     * @param {string} id
     * @param {string} name
     * @param {Object} data
     * @param state
     * @param {HTMLDivElement} node
     * @param controller
     */
    constructor(position, id, name, data, state, node, controller) {
        this.position = position;
        this.data = data;
        this.state = state;
        this.node = node;
        this.#controller = controller;
        this._id = id;
        this._name = name;
    }

    /**
     * <p>Change the state of this {@link RoomplanElement}.</p>
     * <p>A state change caused by the user while the application is in {@link MODE.MEASUREMENT}
     * is stored in the database and displayed in the right sidebar</p>
     */
    setState (state) {
        this.state = state;
        if (this.node != null) {
            this.node.dataset.hmElState = state;
        }
        const justDisplay = JSON.stringify(new Error().stack).split(/\\n/)[1].startsWith("displayAt");
        if (this.#controller.mode === MODE.MEASUREMENT
            && !this.#controller.isLoadingMeasurement
            && !MeasurementViewUi.isPlaybackActive()
            && !justDisplay) {
            // Register the event with the EventHandler and store it in the database
            // if the state change happens in measurement mode and is not caused by a playback
            // (and thus by the user).
            // The event is registered once it has been successfully stored in the database.
            const event = new RoomplanElementEvent(this, this.stateToEventType.call(this, state),{});
            // controller.addEvent.call(controller, event);
            // the event is registered once it has been stored successfully to the database
            this.#controller.sendEvent.call(this.#controller, event, this._id);
        }
    }

    /**
     *
     * @param {number} timestamp
     */
    displayAt (timestamp) {
        const timestamps = Object.keys(this.events).map(t => parseInt(t));
        let last = -1;
        for (const current of timestamps) {
            if (current > timestamp) {
                break;
            }
            last = current;
        }
        if (last === -1) {
            return;
        }
        const state = EventTypeToState(this.events[last].type);
        if (this.state !== state) {
            this.setState(state);
            console.log(state)
        }
    }

    stateToEventType (state) {
        // must be implemented by child classes
        throw new Error("Not implemented");
    }

    setNode (node) {
        this.node = node;
        this.node.ondragstart = this.onDragStart.bind(this);
        // TODO: also register click events if necessary (e.g. sensor node)
    }

    onDragStart (event) {
        event.stopPropagation();
        event.dataTransfer.setData("id", this._id);
        if (this instanceof WindowRoomplanElement) {
            event.dataTransfer.setData("type", OBJECT_TYPE.WINDOW.toString());
        } else if (this instanceof DoorRoomplanElement) {
            event.dataTransfer.setData("type", OBJECT_TYPE.DOOR.toString());
        } else if (this instanceof AirFilterRoomplanElement) {
            event.dataTransfer.setData("type", OBJECT_TYPE.AIR_FILTER.toString());
        } else if (this instanceof SensorNodeRoomplanElement) {
            event.dataTransfer.setData("type", OBJECT_TYPE.SENSOR.toString());
            // show sensor delete button
            document.getElementById("remove-sensor")?.classList.remove("d-none");
        }
        event.dataTransfer.effectAllowed = "move";
    }

    /**
     * @param {Coordinate} newPosition
     */
    changePosition (newPosition) {
        this.position = newPosition;
        if (this.node != null) {
            this.node.style.top = newPosition.y + '%';
            this.node.style.left = newPosition.x + '%';
        }
    }

    delete () {
        if (this.contextMenu != null) {
            this.contextMenu.delete();
        }
        removeElement(this);
        if (this.node != null) {
            this.node.remove();
        }
    }

    getDBInfo() {
        return {
            id: this._id,
            type: this.getObjectType(),
            data: this.getData(),
            position: this.position,
        }
    }

    /**
     * @return int
     */
    getObjectType() {
        throw Error("must be implemented")
    }

    getData() {
        return this.data;
    }

    /**
     * <p>Open the <i>properties</i> dialog for this element.</p>
     * This dialog enables the user to set or modify the properties of this element.
     * @param {boolean} create whether the dialog is called to create the element or change the properties
     * @param {function} positiveCallback function called when the dialog was exited by saving the results
     * @param {function} negativeCallback function called when the dialog was discarded
     */
    openPropertiesDialog = function (create, positiveCallback, negativeCallback) {

    }


    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }
}

/**
 *
 * @type {
 * {doors: DoorRoomplanElement[],
 * sensors: SensorNodeRoomplanElement[],
 * airFilters: AirFilterRoomplanElement[], windows: WindowRoomplanElement[]}}
 */
export const ROOMPLAN_ELEMENTS = {
    sensors: [],
    airFilters: [],
    windows: [],
    doors: []
}

/**
 * Get all registered {@link RoomplanElement}s.
 * @return {RoomplanElement[]}
 */
export function getAllElements() {
    return [].concat(
        ROOMPLAN_ELEMENTS.sensors, ROOMPLAN_ELEMENTS.airFilters,
        ROOMPLAN_ELEMENTS.windows, ROOMPLAN_ELEMENTS.doors)
}

/**
 * Find a registered {@link RoomplanElement}.
 * @param {string} id
 * @param {int | undefined} type
 * @return {RoomplanElement | undefined}
 */
export function findElement(id, type = undefined) {
    /**
     * @param {string} ar - array name
     * @return {RoomplanElement |undefined}
     */
    function getElement(ar) {
        return ROOMPLAN_ELEMENTS[ar].find(e => e.id === id || e.objectId === id);
    }
    if (type === undefined) {
        let el = getElement("sensors");
        if (el !== undefined) return el;
        el = getElement("doors");
        if (el !== undefined) return el;
        el = getElement("airFilters");
        if (el !== undefined) return el;
        return getElement("windows");
    } else {
        switch (type) {
            case OBJECT_TYPE.SENSOR: return getElement("sensors");
            case OBJECT_TYPE.DOOR: return getElement("doors");
            case OBJECT_TYPE.WINDOW: return getElement("windows");
            case OBJECT_TYPE.AIR_FILTER: return getElement("airFilters");
            default: return undefined;
        }
    }
}

/**
 * Remove a {@link RoomplanElement} form the registry.
 * @param {RoomplanElement | string} element
 */
export function removeElement(element, type = undefined) {
    /**
     * @param {string} ar
     * @param {string | int} id
     * @return {int}
     */
    function getElementIndex(id, ar) {
        return ROOMPLAN_ELEMENTS[ar].findIndex(e => e.id === id || e.objectId === id);
    }
    /**
     * @param {int} index
     * @param {string} ar
     */
    function remove(index, ar) {
        if (index !== -1) {
            const el = ROOMPLAN_ELEMENTS[ar].splice(index, 1);
            if (el.length === 1) {
                el[0].delete();
            }
        }
    }
    if (element instanceof RoomplanElement) {
        if (element instanceof SensorNodeRoomplanElement) {
            remove(getElementIndex(element.id, "sensors"), "sensors");
        } else if (element instanceof DoorRoomplanElement) {
            remove(getElementIndex(element.id, "doors"), "doors");
        } else if (element instanceof WindowRoomplanElement) {
            remove(getElementIndex(element.id, "windows"), "windows");
        } else if (element instanceof AirFilterRoomplanElement) {
            remove(getElementIndex(element.id, "airFilters"), "airFilters");
        }
    }
}

export function clearAllElements() {
    for (const elArray in ROOMPLAN_ELEMENTS) {
        for (const el of ROOMPLAN_ELEMENTS[elArray]) {
            removeElement(el);
        }
    }
}


export class OpeningRoomplanElement extends RoomplanElement {
    /**
     * @type {Opening}
     */
    opening;

    constructor(position, id, name, data, state, node, controller, opening) {
        super(position, id, name, data, state, node, controller);
        this.opening = opening;
    }

    getData() {
        console.log(this.opening)
        console.log(JSON.parse(JSON.stringify(this.opening)))
        return JSON.parse(JSON.stringify(this.opening));
    }
}

export class WindowRoomplanElement extends OpeningRoomplanElement {

    constructor(position, id, name, data, state, node, controller, opening) {
        super(position, id, name, data,
            state || WindowRoomplanElement.prototype.STATES.CLOSED, node, controller, opening);
    }

    stateToEventType = function (state) {
        if (state === WindowRoomplanElement.prototype.STATES.CLOSED) {
            return EVENT.WINDOW_CLOSED;
        } else if (state === WindowRoomplanElement.prototype.STATES.TILTED) {
            return EVENT.WINDOW_TILTED;
        } else if (state === WindowRoomplanElement.prototype.STATES.OPEN) {
            return EVENT.WINDOW_OPENED;
        } else {
            return -1;
        }
    }

    /**
     * @override
     */
    setState(state) {
        if (state == WindowRoomplanElement.prototype.STATES.CLOSED) {
            this.opening.isClosed = true;
        } else {
            this.opening.isTilted = state == WindowRoomplanElement.prototype.STATES.TILTED;
            this.opening.isClosed = false;
        }
        super.setState(state);
    }

    getObjectType() {
        return OBJECT_TYPE.WINDOW;
    }
}

WindowRoomplanElement.prototype.STATES = {
    CLOSED: "CLOSED",
    TILTED: "TILTED",
    OPEN: "OPEN"
}

export class DoorRoomplanElement extends OpeningRoomplanElement {

    constructor(position, id, name, data, state, node, controller, opening) {
        super(position, id, name, data,
            state || DoorRoomplanElement.prototype.STATES.CLOSED, node, controller, opening);
    }

    stateToEventType = function (state) {
        if (state === DoorRoomplanElement.prototype.STATES.CLOSED) {
            return EVENT.DOOR_CLOSED;
        } else if (state === DoorRoomplanElement.prototype.STATES.TILTED) {
            return EVENT.DOOR_TILTED;
        } else if (state === DoorRoomplanElement.prototype.STATES.OPEN) {
            return EVENT.DOOR_OPENED;
        } else {
            return -1;
        }
    }

    /**
     * @override
     */
    setState(state) {
        if (state == DoorRoomplanElement.prototype.STATES.CLOSED) {
            this.opening.isClosed = true;
        } else {
            this.opening.isTilted = state == DoorRoomplanElement.prototype.STATES.TILTED;
            this.opening.isClosed = false;
        }
        super.setState(state);
    }

    getObjectType() {
        return OBJECT_TYPE.DOOR;
    }
}

DoorRoomplanElement.prototype.STATES = {
    CLOSED: "CLOSED",
    OPEN: "OPEN",
    TILTED: "TILTED"
}

export class AirFilterRoomplanElement extends RoomplanElement {

    constructor(position, id, name, data, state, node, controller) {
        super(position, id, name, data,
            state || AirFilterRoomplanElement.prototype.STATES.OFF, node, controller);
    }

    stateToEventType = function (state) {
        if (state === AirFilterRoomplanElement.prototype.STATES.ON) {
            return EVENT.AIR_FILTER_ENABLED;
        } else if (state === AirFilterRoomplanElement.prototype.STATES.OFF) {
            return EVENT.AIR_FILTER_DISABLED;
        } else {
            return -1;
        }
    }

    getObjectType() {
        return OBJECT_TYPE.AIR_FILTER;
    }
}

AirFilterRoomplanElement.prototype.STATES = {
    OFF: "OFF",
    ON: "ON"
}

export class SensorNodeRoomplanElement extends RoomplanElement {
    /**
     * @property {Sensor} data
     */
    /**
     *
     * @param {Coordinate} position
     * @param {string} id
     * @param {string} name
     * @param {Sensor} data
     * @param {HTMLDivElement} node
     */
    constructor(position, id, name, data, node, controller) {
        super(position, id, name, data, undefined, node, controller);
    }

    setState(state) {
        // sensor node do not have a state
    }

    changePosition(newPosition) {
        this.data.pos = newPosition;
        super.changePosition(newPosition);
    }

    getObjectType() {
        return OBJECT_TYPE.SENSOR;
    }

    getData() {
        return {};
    }

    /**
     * @return {Sensor}
     */
    getSensor() {
        return this.data;
    }
}

SensorNodeRoomplanElement.prototype.STATES = {
    WAITING: "WAITING",
    NO_DATA: "NO_DATA",
}