import {contextMenuHandler} from "../measurement/MeasurementViewUi.mjs";

export class ContextMenuOption {
    /**
     * @type {HTMLLIElement}
     */
    _node;
    /**
     * Can be used to get the position which was clicked on.
     * Maybe remove and replace with a method to retrieve the position.
     * @type {?ContextMenu}
     */
    _menu;
    _iconClass;
    _description;
    _isDisplayed;
    _onSelected;
    /**
     *
     * @param icon {string} - Bootstrap Icon class name
     * @param text {string} - the text to be displayed
     * @param isDisplayed {function} - calculated, whether the item is displayed or not in menu
     * @param onSelected {function(MouseEvent)} - actions to be performed once the option is selected
     */
    constructor(icon, text, isDisplayed, onSelected) {
        this._iconClass = icon;
        this._description = text;
        this._isDisplayed = isDisplayed;
        this._onSelected = onSelected;
    }


    get node() {
        return this._node;
    }

    set node(node) {
        this._node = node;
    }

    get iconClass() {
        return this._iconClass;
    }

    get description() {
        return this._description;
    }

    get isDisplayed() {
        return this._isDisplayed;
    }

    get onSelected() {
        return this._onSelected;
    }

    get menu() {
        return this._menu;
    }

    set menu(value) {
        this._menu = value;
    }
}

export class ContextMenu {
    /**
     * The target on that needs to be right-clicked to show the menu.
     * @type {HTMLDivElement}
     */
    #target;
    /**
     * @type {HTMLDivElement}
     */
    #menuNode;
    /**
     * @type {ContextMenuHandler}
     */
    #contextMenuHandler;
    /**
     * @type {?RoomplanElement}
     */
    #roomplanElement;
    /**
     * @type {ContextMenuOption[]}
     */
    #options;
    /**
     * The mouse click's relative position on the {@link #target} which opened the {@link ContextMenu}
     * @type Coordinate
     */
    _position;

    /**
     * This is the binding for the context menu close listener.
     * The listener closes the context menu when the user clicks anywhere on the document.
     * The binding is stored, because the <code>function.bind()</code> method creates a new function
     * which needs to be referenced when adding <em>and</em> removing the listener to the document.
     */
    contextMenuCloseListenerBinding = function (event) {
        this.#toggle(event);
        document.removeEventListener("click", this.contextMenuCloseListenerBinding);
    }.bind(this)

    /**
     * Generates a {@link ContextMenu} for an element.
     * @param target {HTMLDivElement}
     * @param contextMenuHandler {ContextMenuHandler}
     * @param roomplanElement {?RoomplanElement}
     * @param options {ContextMenuOption[]}
     */
    constructor(target, contextMenuHandler, roomplanElement, options) {
        this.#target = target;
        this.#contextMenuHandler = contextMenuHandler;
        this.#roomplanElement = roomplanElement;
        this.#options = options;

        this.#menuNode = document.createElement("div");
        this.#menuNode.classList.add("d-none", "context-menu"); // hide the context menu by default
        const contextMenuInnerNode = document.createElement("div");
        contextMenuInnerNode.classList.add("card");
        this.#menuNode.appendChild(contextMenuInnerNode);
        const contextMenuList = document.createElement("ul");
        contextMenuList.classList.add("list-group", "list-group-flush");
        for (const option of options) {
            const optionNode = document.createElement("li");
            optionNode.classList.add("list-group-item");
            const iconNode = document.createElement("i");
            iconNode.classList.add(option.iconClass, "me-2");
            optionNode.appendChild(iconNode);
            optionNode.appendChild(document.createTextNode(option.description));
            optionNode.onclick = this.onOptionSelected.bind(this, option);
            contextMenuList.appendChild(optionNode);
            option.menu = this;
            option.node = optionNode;
        }
        contextMenuInnerNode.appendChild(contextMenuList);
        this.#target.parentElement.append(this.#menuNode);
        this.#target.addEventListener("click", this.#toggle.bind(this));
    }

    /**
     * Toggle the context menu's visibility
     * @param {MouseEvent | null} event
     * @param  {boolean} forceClose - whether to force close the
     * @this {ContextMenu}
     */
    #toggle = function (event) {
        // close other context menus on the roomplan
        // const openCM = this.#contextMenuHandler.getOpenContextMenu();
        // if (openCM != null && openCM !== this) {
        //     openCM.toggle.call(openCM, null, true);
        //     return;
        // }
        // toggle this menu

        if (event != null) {
            event.stopPropagation();
        }
        if (this.#menuNode.classList.contains("d-none")) {
            // show this context menu
            this.open(event);
        } else {
            this.close();
        }
    }

    close() {
        // hide this context menu
        document.removeEventListener("click", this.contextMenuCloseListenerBinding);
        this.#menuNode.classList.add("d-none");
        this.#contextMenuHandler.setOpenContextMenu(null);
    }

    open(event) {
        if (event != null) {
            event.stopPropagation();
        }
        const menu = this;
        this._position = this.getRelativePosition({x: event.clientX, y: event.clientY})
        document.addEventListener("click", this.contextMenuCloseListenerBinding);
        let displayedOptions = 0;
        this.#options.forEach((option) => {
            if (option.isDisplayed.call(menu.#roomplanElement, event)) {
                option.node.classList.remove("d-none");
                displayedOptions++;
            } else {
                option.node.classList.add("d-none");
            }
        });
        if (displayedOptions !== 0) {
            // Only display this context menu when there are displayed elements
            // in the context menu.
            this.#menuNode.classList.remove("d-none");
            this.calculatePosition.call(this, event);
            this.#contextMenuHandler.setOpenContextMenu(this);
        } else {
            this.#contextMenuHandler.setOpenContextMenu(null);
        }
    }

    /**
     *
     * @param option {ContextMenuOption}
     * @param event {Event}
     * @this {ContextMenu}
     */
    onOptionSelected = function (option, event) {
        event.stopPropagation();
        this.#toggle.call(this, event);
        option.onSelected.call(option, event);
    }

    /**
     *
     * @param event
     * @this {ContextMenu}
     */
    calculatePosition = function (event) {
        // find the best position to put the context menu;
        // the context menu should stay on the roomplan and not overlap / hide other areas
        const menuWidth = this.#menuNode.offsetWidth;
        const menuHeight = this.#menuNode.offsetHeight;
        const pointerX = event.clientX - this.#menuNode.parentElement.getBoundingClientRect().x;
        const pointerY = event.clientY - this.#menuNode.parentElement.getBoundingClientRect().y;
        const parentHeight = this.#menuNode.parentElement.offsetHeight;
        const parentWidth = this.#menuNode.parentElement.offsetWidth;
        if (pointerX < menuWidth) {
            // context menu cannot be displayed fully left from the pointer
            this.#menuNode.style.left = pointerX + "px";
            if (parentHeight < menuHeight + pointerY) {
                // context menu cannot be displayed fully below the pointer, put it above
                this.#menuNode.style.top = (pointerY - menuHeight) + "px";
            } else {
                this.#menuNode.style.top = pointerY + "px";
            }
        } else {
            if (pointerX + menuWidth > parentWidth) {
                this.#menuNode.style.left = (parentWidth - menuWidth - 2) + "px"; // fix decimal values
            } else {
                this.#menuNode.style.left = pointerX + "px";
            }
            if (parentHeight < menuHeight + pointerY) {
                // context menu cannot be displayed fully on the right of the pointer,
                // put it on the left
                this.#menuNode.style.top = (pointerY - menuHeight) + "px";
            } else {
                this.#menuNode.style.top = pointerY + "px";
            }
        }
    }

    /**
     * Get a position which is relative to this menu (in percent)
     * @param absolute {Coordinate}
     */
    getRelativePosition = function (absolute) {
        const boundaries = this.#target.getBoundingClientRect();
        return {
            x: 100 * (absolute.x - boundaries.left) / this.#target.offsetWidth,
            y: 100 * (absolute.y - boundaries.top) / this.#target.offsetHeight
        }
    }

    delete = function () {
        this.#menuNode.remove();
    }

    get position() {
        return this._position;
    }
}

export class ContextMenuHandler {
    /**
     * @type {ContextMenu | null}
     */
    #openCM;
    constructor(cm) {
        this.#openCM = cm;
    }

    /**
     * @param {ContextMenu} cm
     */
    setOpenContextMenu(cm) {
        const cmToClose = this.#openCM;
        this.#openCM = cm;
        cmToClose?.close();

    }

    getOpenContextMenu() {
        return this.#openCM;
    }
}