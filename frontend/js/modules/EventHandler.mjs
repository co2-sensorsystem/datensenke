import * as RoomUsage from "./simulation/RoomUsage.mjs";
import {AGE_GROUP} from "./simulation/RoomUsage.mjs";
import {MODE} from "../../config.js";

import {
    AirFilterRoomplanElement,
    DoorRoomplanElement, ROOMPLAN_ELEMENTS,
    WindowRoomplanElement
} from "./room/RoomplanElement.mjs";
import {timeResolution} from "./simulation/simulationController.mjs";

/**
 * @typedef ModeChangeEvent
 * @type Event
 * @see MODE
 * @property {string} oldMode
 * @property {string} newMode
 */

/**
 *
 */
export class EventHandler {
    /**
     * @type {RoomplanElementEvent[]}
     */
    events = [];
    /**
     * @type {NoteEvent[]}
     */
    notes = [];
    /**
     * @type {RoomUsageEvent[]}
     */
    roomUsages = [];
    /**
     * @private
     * @type {Object<int>} key is
     *
     */
    timestamps = {};

    /**
     * @param {RoomplanElementEvent | NoteEvent | RoomUsageEvent} event
     */
    addEvent = function (event) {
        if (event instanceof RoomplanElementEvent) {
            this.events.push(event);
            event.roomplanElement.events[event.timestamp] = event;
        } else if (event instanceof NoteEvent) {
            this.notes.push(event);
        } else if (event instanceof RoomUsageEvent) {
            this.roomUsages.push(event);
        }
    }

    displayAt = function (timestamp) {
        function d(e) {
            e.displayAt(timestamp);
        }
        ROOMPLAN_ELEMENTS.doors.forEach(e => d(e));
        ROOMPLAN_ELEMENTS.windows.forEach(e => d(e));
        ROOMPLAN_ELEMENTS.airFilters.forEach(e => d(e));
        //ROOMPLAN_ELEMENTS.doors.forEach(e => d(e));
    }

    /**
     * Returns the Bootstrap Icon class for an icon matching the event type.
     * @param type
     * @return {string}
     */
    getIconForEventType = function (type) {
        switch (parseInt(type)) {
            case EVENT.WINDOW_CLOSED: return "bi-door-closed";
            case EVENT.WINDOW_OPENED: return "bi-door-open";
            case EVENT.DOOR_CLOSED: return "bi-door-closed";
            case EVENT.DOOR_OPENED: return "bi-door-open";
            case EVENT.AIR_FILTER_DISABLED: return "bi-filter-square";
            case EVENT.AIR_FILTER_ENABLED: return "bi-filter-square-fill";
            case EVENT.NOTE: return "bi-pencil-square";
            case EVENT.ROOM_USAGE_CHANGED: return "bi-person-circle";
            default: return "";
        }
    }

    reset() {
        this.events.length = 0;
        this.notes.length = 0;
        this.roomUsages.length = 0;
        this.timestamps.length = 0;
    }
}

export const EVENT = {
    // Air quality control
    WINDOW_CLOSED: 0,
    WINDOW_OPENED: 1,
    WINDOW_TILTED: 5,

    DOOR_CLOSED: 2,
    DOOR_OPENED: 3,
    DOOR_TILTED: 4,

    AIR_FILTER_DISABLED: 6,
    AIR_FILTER_ENABLED: 7,

    // Protocol control
    NOTE: 100,
    ROOM_USAGE_CHANGED: 200,
};

class MeasurementEvent {
    /**
     * Describes the type of event.
     * @type {int}
     */
    type;
    timestamp;

    /**
     * @param {int} type
     * @param {number} timestamp
     */
    constructor(type, timestamp = undefined) {
        this.type = type;
        if (timestamp === undefined) {
            this.timestamp = Date.now();
        } else {
            this.timestamp = timestamp;
        }
    }

    /**
     * convert the specific data of an event-child to a String,
     * which could displayed at UI in the protocol list
     */
    getDescription = function () {
      // must be implemented by child classes
      throw new Error("Not implemented");
    }
}

export class RoomplanElementEvent extends MeasurementEvent {
    roomplanElement;
    data;

    /**
     * @param {RoomplanElement} roomplanElement
     * @param {int} type
     * @param {Object} data
     * @param {number} timestamp
     */
    constructor(roomplanElement, type, data, timestamp = undefined) {
        super(type, timestamp);
        this.roomplanElement = roomplanElement;
        this.data = data;
    }

    getDescription = function () {
        switch (this.type) {
            case EVENT.WINDOW_OPENED:
            case EVENT.DOOR_OPENED:
                return this.roomplanElement.name + " geöffnet";
            case EVENT.WINDOW_CLOSED:
            case EVENT.DOOR_CLOSED:
                return this.roomplanElement.name + " geschlossen";
            case EVENT.WINDOW_TILTED:
            case EVENT.DOOR_TILTED:
                return this.roomplanElement.name + " gekippt";
            case EVENT.AIR_FILTER_ENABLED:
                return this.roomplanElement.name + " angeschaltet";
            case EVENT.AIR_FILTER_DISABLED:
                return this.roomplanElement.name + " abgeschaltet";
            default:
                return "unbekanntes Protokollereignis";
        }
    }
}

export class NoteEvent extends MeasurementEvent {
    /**
     * @type{string}
     */
    text;

    constructor(text, timestamp = undefined) {
        super(EVENT.NOTE, timestamp);
        this.text = text;
    }

    getDescription = function () {
      return this.text.replace('\n', '<br>');
    }
}

export class RoomUsageEvent extends MeasurementEvent {
    /**
     * @type {RoomUsageGroup[]}
     */
    roomUsageGroups = [];

    /**
     * @param {RoomUsageGroup[]} roomUsageGroups
     * @param {number} timestamp
     */
    constructor(roomUsageGroups, timestamp = undefined) {
        super(EVENT.ROOM_USAGE_CHANGED, timestamp);
        this.roomUsageGroups = roomUsageGroups;
    }

    getDescription = function () {
        if (this.roomUsageGroups.length === 0) {
            return "Keine Personen im Raum";
        }

        // arrays sorted by ageLevel
        let adult = [];
        let child0 = [];
        let child1 = [];
        let child2 = [];

        // order every group into the specific array for ageLevel
        this.roomUsageGroups.forEach((roomUsageGroup) => {
            // TODO: compare variable and not only age definition
            if (roomUsageGroup.ageLevel === RoomUsage.AGE_GROUP.ADULT) {
                adult.push(roomUsageGroup);
            } else if (roomUsageGroup.ageLevel === RoomUsage.AGE_GROUP.KINDERGARTEN) {
                child0.push(roomUsageGroup);
            } else if (roomUsageGroup.ageLevel === RoomUsage.AGE_GROUP.PRIMARSTUFE) {
                child1.push(roomUsageGroup);
            } else if (roomUsageGroup.ageLevel === RoomUsage.AGE_GROUP.SEKUNDARSTUFE) {
                child2.push(roomUsageGroup);
            } else {
                return "unbekanntes Protokollereignis";
            }
        });

        let orderedRoomUsageGroups = [];
        orderedRoomUsageGroups.push(adult);
        orderedRoomUsageGroups.push(child0);
        orderedRoomUsageGroups.push(child1);
        orderedRoomUsageGroups.push(child2);

        let data = "Raumnutzung geändert:  <br>";

        // counts of all in one group and every activity
        let personCount = 0;
        let lowActivity = 0;
        let mediumActivity = 0;
        let hardActivity  = 0;

        // get counts and build output string for protocol
        orderedRoomUsageGroups.forEach((roomUsageGroup) => {
            if (roomUsageGroup.length !== 0) {
                if (data.substring(data.length - 1) !== ">") {
                    data = data.concat("<br>");
                }
                roomUsageGroup.forEach((roomUsageAdult) => {
                    personCount += roomUsageAdult.personCount;
                    // TODO: compare variable and not only age definition
                    if (roomUsageAdult.activity === RoomUsage.ACTIVITY.LOW) {
                        lowActivity += roomUsageAdult.personCount;
                    }
                    if (roomUsageAdult.activity === RoomUsage.ACTIVITY.MEDIUM) {
                        mediumActivity += roomUsageAdult.personCount;
                    }
                    if (roomUsageAdult.activity === "schwer") {
                        hardActivity += roomUsageAdult.personCount;
                    }
                });

                const ageLevelName = (() => {
                    switch (roomUsageGroup[0].ageLevel) {
                        case AGE_GROUP.KINDERGARTEN: return "Kindergarten";
                        case AGE_GROUP.SEKUNDARSTUFE: return "Sekundarstufe";
                        case AGE_GROUP.PRIMARSTUFE: return "Primarstufe";
                        case AGE_GROUP.ADULT: return "Erwachsen";
                    }
                })();


                data = data.concat(personCount + " " + ageLevelName + " (");
                if (lowActivity > 0) {
                    data = data.concat(lowActivity + " gering");
                }
                if (mediumActivity > 0) {
                    if (data.substring(data.length - 1) !== "(") {
                        data = data.concat(", ");
                    }
                    data = data.concat(mediumActivity + " mittel");
                }
                if (hardActivity > 0) {
                    if (data.substring(data.length - 1) !== "(") {
                        data = data.concat(", ");
                    }
                    data = data.concat(hardActivity + " schwer");
                }
                data = data.concat(")");

                // reset counts
                personCount = 0;
                lowActivity = 0;
                mediumActivity = 0;
                hardActivity = 0;
            }
        });
        return data;
    }
}

export function EventTypeToState(type) {
    switch (type) {
        case EVENT.WINDOW_CLOSED: return WindowRoomplanElement.prototype.STATES.CLOSED;
        case EVENT.WINDOW_OPENED: return WindowRoomplanElement.prototype.STATES.OPEN;
        case EVENT.WINDOW_TILTED: return WindowRoomplanElement.prototype.STATES.TILTED;
        case EVENT.DOOR_CLOSED: return DoorRoomplanElement.prototype.STATES.CLOSED;
        case EVENT.DOOR_OPENED: return DoorRoomplanElement.prototype.STATES.OPEN;
        case EVENT.DOOR_TILTED: return DoorRoomplanElement.prototype.STATES.TILTED;
        case EVENT.AIR_FILTER_ENABLED: return AirFilterRoomplanElement.prototype.STATES.ON;
        case EVENT.AIR_FILTER_DISABLED: return AirFilterRoomplanElement.prototype.STATES.OFF;
        default: return -1;
    }
}

/**
 * @typedef Event
 * @property timestamp {int}
 * @property type {int} - see {@link TYPE}
 * @property data {Object}
 * @property [data.position] {Coordinate} position in the room / on the room plan
 */