/**
 * Get color from linear gradient at position from 0.0 to 1.0
 * @see https://stackoverflow.com/a/7128796
 * @param {number} position number greater than 0
 * @param {Object[]} colorStops
 * @param {float} colorStops[].pct
 * @param {Object} colorStops[].color
 * @return {{r: number, b: number, g: number}}
 */
import {NO_DATA} from "../../../config.js";

/**
 * @typedef ColorStop
 * @property {number} pct
 * @property {RGBColor} color
 */

/**
 *
 */
class MeasurementValueInterpreter {
    /**
     * @type {RGBColor}
     */
    _NO_DATA_COLOR = {r: 0, g: 0, b: 255}
    /**
     *
     * @type {ColorStop[]}
     */
    _colorStops = [];
    _getColorForPercentage = function (pct) {
        let i;
        for (i = 1; i < this._colorStops.length - 1; i++) {
            if (pct < this._colorStops[i].pct) {
                break;
            }
        }
        let lower = this._colorStops[i - 1];
        let upper = this._colorStops[i];
        let range = upper.pct - lower.pct;
        let rangePct = (pct - lower.pct) / range;
        let pctLower = 1 - rangePct;
        let pctUpper = rangePct;
        return  {
            r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
            g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
            b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
        };
    }

    /**
     * @param {number} value
     * @return RGBColor
     */
    getColor(value) {
        throw new Error("abstract method");
    }

}

export class CO2ValueInterpreter extends MeasurementValueInterpreter {
    static  WARNING_THRESHOLD = 1000;
    static THRESHOLD_GREEN = 800;
    static THRESHOLD_YELLOW = 1100;
    static #THRESHOLD_RED = 1300;


    constructor() {
        super();
        const getBase = (v) => v - 400;
        this._colorStops = [
            {
                pct: getBase(CO2ValueInterpreter.THRESHOLD_GREEN) / getBase(CO2ValueInterpreter.#THRESHOLD_RED),
                color: { r: 0x1c, g: 0xc1, b: 0x14 }
            },
            {
                pct: getBase(CO2ValueInterpreter.THRESHOLD_YELLOW) / getBase(CO2ValueInterpreter.#THRESHOLD_RED),
                color: { r: 0xfc, g: 0xff, b: 0x00 }
            },
            {
                pct: 1,
                color: { r: 0xff, g: 0x00, b: 0x00 }
            },
            {
                pct: 1.5,
                color: { r: 0xaf, g: 0x01, b: 0x01 }
            }
        ];
    }

    /**
     * Get the corresponding color for a CO2 concentration.
     * @param {number} concentration the concentration in ppm
     * @return {RGBColor}
     * @override
     */
    getColor (concentration) {
        if (concentration === NO_DATA) {
            return this._NO_DATA_COLOR;
        }
        return this._getColorForPercentage(
            Math.max(400, concentration) / CO2ValueInterpreter.#THRESHOLD_RED);
    }
}

export class PMValueInterpreter extends MeasurementValueInterpreter{
    maxValue = 0.00001;
    constructor() {
        super();
        this._colorStops = [
            {
                pct: 0,
                color: {r: 0xff, g: 0xe6, b: 0x00}
            },
            {
                pct: 1,
                color: {r: 0xe6, g: 0x12, b: 0xd9}
            }
        ];
    }
    getColor(concentration) {
        if (concentration === NO_DATA) {
            return this._NO_DATA_COLOR;
        }
        return this._getColorForPercentage(concentration / this.maxValue);
    }
}
