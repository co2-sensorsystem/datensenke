import {
    CO2ValueInterpreter
} from "./MeasurementValueInterpreter.mjs";
import * as timeline from './Timeline.mjs';
import {ConfirmationDialog, TYPE as ConfirmationDialogType} from "../ui/modals/ConfirmationDialog.mjs";
import * as ChartHandler from "../ChartHandler.mjs";
import {ContextMenuHandler} from "../room/ContextMenu.js";
import {
    findElement,
    removeElement,
    ROOMPLAN_ELEMENTS
} from "../room/RoomplanElement.mjs";
import * as CommunicationHandler from "../CommunicationHandler.mjs";
import * as ErrorModal from "../ui/modals/ErrorModal.mjs";
import * as RoomUsage from "../simulation/RoomUsage.mjs";
import {NoteEvent} from "../EventHandler.mjs";
import {
    createElementFromHTML,
    getLocalizedDateForTimeString,
    selectToggle,
    sleep
} from "../utils.mjs";
import {createRoomplanElement} from "../room/RoomplanElementFactory.mjs";
import * as RoomEditor from "../room/roomEditor.mjs";
import {CONTENT, FINEDUSTSENSOR_STATES, LED_STATES, MODE, NO_DATA, OBJECT_TYPE} from "../../../config.js";
import * as ImprintModal from "../ui/modals/imprintModal.mjs";
import {AverageCO2ConcentrationCalculator} from "./AverageConcentrationPerRoomUsage.mjs";

/**
 * @type MeasurementViewController
 */
let controller;

/**
 * Content of the main display area.
 * @see CONTENT
 * @type {string}
 */
let content = CONTENT.ROOMPLAN;

let chart = null;

export let roomplan;
let roomplanContainer;
let roomplanSensorDisplayType = "co2";
let roomPropertiesContainer;

const protocolEventContainer = document.getElementById("protocol-event-container");

/**
 * TODO: add method <code>selectSensor(Sensor)</code> which also triggers the UI updates.
 * @type {Sensor, null}
 * @private
 */
let selectedSensor = null

/**
 * Select a Sensor to view more information about it.
 * This also highlights the selected sensor on the roomplan.
 * @param sensor {Sensor | null}
 */
function selectSensor(sensor) {
    selectedSensor = sensor;
    let oldSelected = roomplan.querySelector(".selected");
    if (oldSelected != null) {
        oldSelected.classList.remove("selected");
    }
    if (sensor == null) {
        updateUiWithSensor(null, null);
    } else {
        if (controller.currentlyDisplayedTimestamp !== null
            && controller.currentlyDisplayedTimestamp !== -1) {
            let datapoint = sensor.data[controller.currentlyDisplayedTimestamp];
            if (datapoint !== undefined) {
                updateUiWithSensor(sensor, datapoint);
            } else {
                updateUiWithSensor(sensor, null);
            }
        } else {
            updateUiWithSensor(sensor, null);
        }
        // highlight sensor on roomplan
        document.getElementById("sensor-" + sensor.id).classList.add("selected");
    }
}

/**
 * @param modal {bootstrap.Modal} measurement list modal
 * @param list {HTMLElement} measurement list
 */
function onLoadMeasurementSubmitButtonClicked(modal, list) {
    controller.requestMeasurement.call(controller,
        parseInt(list.getElementsByClassName("active")[0].dataset.measurementId));
    modal.hide();
}

/**
 * @param modal {bootstrap.Modal | he} measurement list modal
 * @param list {HTMLElement} measurement list
 */
function onDeleteMeasurementSubmitButtonClicked(modal, list) {
    const child = list.getElementsByClassName("active")[0];
    const measurementName = child.dataset.measurementName;
    // hide the measurement list modal to make space for the confirmation dialog
    modal.hide();
    askForConfirmation(
        "Messung löschen?",
        `<p>Möchten Sie die Messung <i>"${measurementName}"</i> wirklich permanent löschen?</p>`
        + "<p>Dieser Vorgang kann <b>nicht rückgängig</b> gemacht werden!</p>",
        () => {
            CommunicationHandler.send(
                {
                    event: "deleteMeasurement",
                    id: child.dataset.measurementId
                },
                {
                    context: null,
                    onSuccess: function () {
                        if (child.nextElementSibling !== null) {
                            child.nextElementSibling.classList.add("active");
                        } else if (child.previousElementSibling !== null) {
                            child.previousElementSibling.classList.add("active");
                        }
                        child.remove();
                        controller.requestMeasurementList(); // make the measurement list modal visible again
                    },
                    onError: function (info) {
                        ErrorModal.open("Löschen der Messung fehlgeschlagen", info);
                        controller.requestMeasurementList(); // make the measurement list modal visible again
                    }
                })
        },
        () => {
            modal.show(); // make the measurement list modal visible again
        },
        ConfirmationDialogType.CONTINUE
    );
}

export function onClickSensor(event) {
    event.stopPropagation();
    selectSensor(controller.sensorHandler.getSensorById(event.target.id));

    // delete info-box
    document.getElementById("sensor-info-click").innerHTML = "";
    // show sensor delete button
    document.getElementById("remove-sensor").classList.remove("d-none");
}

function onChangeMeasurementRoom() {
    let isCreateRoomSelected = isCreateNewMeasurementRoomSelected()
    RoomEditor.setCreateRoom(isCreateRoomSelected);
    document.getElementById("sidebar").querySelectorAll('[data-room-info="true"]')
        .forEach((el) => {
            if (isCreateRoomSelected) {
                el.classList.remove("d-none")
                if (el.getElementsByTagName("input")[0].name !== "room-property-roomplan-image")
                    el.getElementsByTagName("input")[0].required = true
            } else {
                el.classList.add("d-none")
                el.getElementsByTagName("input")[0].required = false
            }
        })
    const selectedRoomId = parseInt(document.getElementById("measurementRoom").value);
    updateRoomData(false, selectedRoomId === 0 ? undefined : selectedRoomId);
}

function updateRoomData(changeRoomplanElements = false, selectedRoomId = undefined) {
    if (!selectedRoomId) {
        selectedRoomId = RoomEditor.getRoomId();
    }
    let selectedRoom = controller.rooms.find((r) => r.id === selectedRoomId);
    if (selectedRoomId == null) {
        RoomEditor.getRoom();
    }

    RoomEditor.clearStandardOpenings();

    RoomEditor.changeRoom(selectedRoom, changeRoomplanElements);
    if (selectedRoom != null && selectedRoom.elements instanceof Array) {
        for (const elementsKey in selectedRoom.elements) {
            const e = selectedRoom.elements[elementsKey];
            selectedRoom.elements[elementsKey] = createRoomplanElement(
                roomplan, e.type, e.position, e.id, e.data, contextMenuHandler, controller);

        }
    }
}

function onRemoveSensorClick() {
    if (selectedSensor != null) {
        // add sensor to list of removed sensors allowing to re-add it later
        let div = document.createElement('div');
        div.classList.add('row');
        div.innerHTML =
            '<div class="col">Sensor ' + selectedSensor.id + '</div>\n' +
            '<div class="col-auto">\n' +
            '<svg class="bi" width="32" height="32" fill="currentColor">\n' +
            '<use xlink:href="bootstrap-icons.svg#plus"/>\n' +
            '</svg>\n' +
            '</div>';
        document.getElementById("removed-sensors-list").appendChild(div);
        div.onclick = onReAddSensorClick;

        // remove the sensor from the frontend
        controller.removeSensor(selectedSensor)

        // show delete sensor list
        document.getElementById("removed-sensors").classList.remove("d-none")
        // display none the delete sensor button
        document.getElementById("remove-sensor").classList.add("d-none")
    }
}

/**
 *
 * @param {MouseEvent} event
 */
function onReAddSensorClick(event) {
    let row = event.target.closest('.row');
    let removedRows = document.getElementById('removed-sensors-list').children;
    for (let i = 0; i < removedRows.length; i++) {
        if (removedRows[i] === row) {
            let sensor = controller.sensorHandler.reAddSensor(i);
            if (sensor != null) {
                createRoomplanElement(
                    roomplan, OBJECT_TYPE.SENSOR, sensor.pos, "sensor-" + sensor.id, sensor,
                    contextMenuHandler, controller)
                //addSensor(sensor)
                checkSensorDraggable()
                row.remove();
            } else {
                // should never happen
                ErrorModal.open(
                    "Konnte Sensor nicht wieder hinzufügen",
                    "sensor ist <code>null</code>");
            }
            break;
        }
    }
    if (removedRows.length === 0) {
        document.getElementById("removed-sensors").classList.add("d-none");
    }
}

function onInputMeasurementNameInputField(event) {
    if (event.target.value.trim().length === 0) {
        document.getElementById("nav-start-measurement")
            .classList.add("disabled");
    } else {
        document.getElementById("nav-start-measurement")
            .classList.remove("disabled");
    }
}

/**
 * Listener called when creating a new {@link NoteEvent} via the UI
 * @param {SubmitEvent} event
 */
function onCreateNewProtocolEvent(event) {
    event.preventDefault();

    createNewProtocolEvent();

    return false; // do not submit the form; we do not want to reload the page
}

/**
 * creating a new {@link NoteEvent} via the UI
 */
function createNewProtocolEvent() {
    const textarea = document.getElementById("protocolEventInput");
    const value = textarea.value;
    if (value) {
        const protocolEvent = new NoteEvent(value);
        controller.sendEvent(protocolEvent, null);
    }
}



function onShowCalibrationModal() {
    const sensorList = document.getElementById("calibration-sensor-list");
    let sensorHtml = "";
    controller.sensorHandler.sensors.forEach(sensor => {
        sensorHtml +=
            '<div class="col-sm-4 col-md-3">' +
            '<input type="checkbox" class="btn-check" value="' + sensor.id + '" id="calibration-sensor-' + sensor.id + '" autocomplete="off">' +
            '<label class="btn btn-outline-success w-100" for="calibration-sensor-' + sensor.id + '">Sensor ' + sensor.id + '</label>' +
            '</div>'
    });
    sensorList.innerHTML = sensorHtml;
    document.getElementById("calibration-sensor-list-select-all").onclick = () => {
        const inputs = document.getElementById("calibration-form")
            .querySelectorAll('input[id^="calibration-sensor-"]');
        const checked = document.getElementById("calibration-form")
            .querySelectorAll('input[id^="calibration-sensor-"]:checked');
        // check all inputs / checkboxes if at least on is not checked;
        // if all checkboxes are checked, deselect all
        const check = inputs.length !== checked.length;
        for (let input of inputs) {
            input.checked = check;
        }
        validateCalibrationForm();
    };
    validateCalibrationForm();
}

function validateCalibrationForm() {
    const checked = document.getElementById("calibration-form")
        .querySelectorAll('input[id^="calibration-sensor-"]:checked');
    const checkedCalibrationChoice = document.getElementById("calibration-form")
        .querySelectorAll('input[id^="calibration-choice-"]:checked');
    const calibrationCO2Value = document.getElementById("calibration-co2-value").value;
    const calibrationTemperatureValue = document.getElementById("calibration-temperature-value").value;
    const submitButton = document.getElementById("calibrationModal")
        .querySelector('.modal-footer button[type="submit"]');
    submitButton.disabled = (checked.length === 0 || checkedCalibrationChoice.length === 0
        || (calibrationCO2Value === "" && calibrationTemperatureValue === "") || calibrationCO2Value === undefined
        || calibrationCO2Value === undefined);
}

function startCalibration(modal, event) {
    event.preventDefault();
    let checked = document.getElementById("calibration-form")
        .querySelectorAll('input[id^="calibration-sensor-"]:checked');
    const checkedCalibrationChoice = document.getElementById("calibration-form")
        .querySelectorAll('input[id^="calibration-choice-"]:checked');
    if (checkedCalibrationChoice.length === 0) return false;
    if (checked.length === 0) return false;

    const calibrationChoice = [];
    checkedCalibrationChoice.forEach(
        (checkbox) => {
            calibrationChoice.push(checkbox.value);
        }
    )

    let calibrationMessage = {};
    if (calibrationChoice.length === 2) {
        calibrationMessage = {
            event: "forcedCalibration",
            co2: parseInt(document.getElementById("calibration-co2-value").value),
            temperature: parseFloat(document.getElementById("calibration-temperature-value").value),
            sensorknots: [],
        };
    } else if (calibrationChoice.length === 1) {
        if (calibrationChoice[0] === "co2") {
            calibrationMessage = {
                event: "forcedCalibration",
                co2: parseInt(document.getElementById("calibration-co2-value").value),
                sensorknots: [],
            };
        } else if (calibrationChoice[0] === "temperature") {
            calibrationMessage = {
                event: "forcedCalibration",
                temperature: parseFloat(document.getElementById("calibration-temperature-value").value),
                sensorknots: [],
            };
        } else {
            return false;
        }
    } else {
        return false;
    }
    for (let checkbox of checked) {
        calibrationMessage.sensorknots.push(parseInt(checkbox.value));
    }
    CommunicationHandler.send(calibrationMessage, {
        context: null,
        onSuccess: () => {
            modal.hide();
        },
        onError: (errorInfo) => {
            let message;
            if (errorInfo.length === 1) {
                message = `Der Sensor mit der ID ${errorInfo[0].id} wurde nicht kalibriert.`;
            } else {
                message = `Die Sensoren mit den IDs ${errorInfo.map((info) => info.id).join(", ")} wurden nicht kalibriert.`;
            }
            ErrorModal.open("Kalibrierung konnte nicht durchgeführt werden", message)
        },
    });
}

/**
 * @param checkboxTrafficLight {HTMLInputElement}
 */
function onSwitchTrafficLight(checkboxTrafficLight) {
    const state = checkboxTrafficLight.checked ? LED_STATES.ON : LED_STATES.OFF;
    for (const sensor of controller.sensorHandler.sensors) {
        sensor.LED = state;
    }
    const trafficLightMessage = {
        event: "trafficLight",
        state: state
    };

    CommunicationHandler.send(trafficLightMessage, {
        context: null,
        onSuccess: () => {
            console.log("traffic-light changed");
        },
        onError: () => {
            // no error message; the webserver does not respond to this broadcast
            // console.log("traffic-light not changed");
        },
    });
}

/**
 * @param checkboxFineDustSensor {HTMLInputElement}
 */
function onSwitchFineDustSensor(checkboxFineDustSensor) {
    const state = checkboxFineDustSensor.checked ? FINEDUSTSENSOR_STATES.ON : FINEDUSTSENSOR_STATES.OFF;
    for (const sensor of controller.sensorHandler.sensors) {
        sensor.FINEDUSTSENSOR = state;
    }
    const fineDustSensorMessage = {
        event: "fineDustSensor",
        state: state
    };

    CommunicationHandler.send(fineDustSensorMessage, {
        context: null,
        onSuccess: () => {
            console.log("fine dust sensor changed");
        },
        onError: () => {
            // no error message; the webserver does not respond to this broadcast
            // console.log("fine dust sensor not changed");
        },
    });
}

/**
 * @param date {Date}
 * @return {string} the German date
 */
function formatDate(date) {
    return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
}

/**
 * Retrieve a German string representation of a {@link Date} object.
 * @param date {Date}
 * @param seconds whether the seconds are also rendered
 * @return {string} the German time
 */
function formatTime(date, seconds = false) {
    function pad(n) {
        // Add leading zeros to the numbers to get a number like 8 => 08, 10 => 10
        return String(n).padStart(2, '0');
    }
    if (seconds) {
        return `${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`;
    }
    return `${pad(date.getHours())}:${pad(date.getMinutes())}`;
}

/**
 * Redraw an element forcefully
 * @param {HTMLDivElement} element
 */
function forceRedraw(element) {
    const n = document.createTextNode(' ');
    const disp = element.style.display;  // don't worry about previous display style

    element.appendChild(n);
    element.style.display = 'none';

    setTimeout(function(){
        element.style.display = disp;
        n.parentNode.removeChild(n);
    },150); // you can play with this timeout to make it as short as possible
}

/**
 * Whether an animation is running
 * @type boolean
 */
export let isDataReviewActive = false;

/**
 * Contains the {@link ContextMenu} which is currently opened.
 * Is <code>null</code> if no {@link ContextMenu} is opened.
 * @type {ContextMenuHandler | null}
 */
export const contextMenuHandler = new ContextMenuHandler(null);

/**
 * @param {MeasurementViewController} _controller
 */
export function initialize(_controller) {
    controller = _controller;
    // initialize listeners
    timeline.init(controller);
    // no open context menu at initialization
    // create new instance of RoomplanContextMenu
    // noinspection JSUnusedLocalSymbols - TODO: do not use a constructor, but function to create it
    //const contextMenu = new RoomplanContextMenu(roomplan, controller, contextMenuHandler);
    const roomplanContainerWrapper = document.getElementById('roomplanContainerWrapper');
    RoomEditor.init(controller, contextMenuHandler, false, document.getElementById("room-properties-container"), roomplanContainerWrapper)
    RoomEditor.setCreateRoom(true);
    roomplan = document.getElementById("roomplan");
    roomplanContainer = document.getElementById("roomplan-container");
    roomPropertiesContainer  = document.getElementById("room-properties-container");
    roomPropertiesContainer.querySelector('[name="room-property-name"]').oninput = onInputMeasurementNameInputField;
    //document.getElementById("room-property-width").oninput = () => {updateRoomData(false);};
    //document.getElementById("room-property-length").oninput = () => {updateRoomData(false);};
    document.getElementById("newMeasurementConfiguration").addEventListener('submit',
        function (event) {
        event.preventDefault()
        event.stopPropagation()

        if (!event.target.checkValidity()) {
            event.target.classList.add('was-validated')
        } else {
            controller.prepareRoomInformation.call(controller, true)
        }
    }, false)

    RoomUsage.init(true);

    document.getElementById("add-protocol-event").onsubmit = function (event) {
        onCreateNewProtocolEvent(event);
    }

    document.getElementById("add-room-usage-event").onsubmit = function (event) {
        RoomUsage.onCreateNewRoomUsageEvent(event, controller);
    }

    const roomUsageGroupContainer = document.getElementById("room-usage-group-container");
    // always provide a default group at startup to save a click by the user and give a hint
    // which action by the user is expected
    RoomUsage.onAddUserGroup(roomUsageGroupContainer);

    document.getElementById("add-room-usage-group").onclick = function () {
        RoomUsage.onAddUserGroup(roomUsageGroupContainer);
    }

    document.getElementById("remove-room-usage-group-entry-1").onclick = function () {
        RoomUsage.onRemoveRoomUsageGroup(1);
    }

    document.getElementById("no-room-usage-group").onclick = () => RoomUsage.onNoRoomUsageGroup(controller);

    document.getElementById("stop-measurement").onclick = () => {
        controller.stopMeasurement()
    }

    document.getElementById("stop-viewer").onclick = () => {
        controller.resetMeasurementData()
        controller.setMode(MODE.CONFIGURATION)
        // TODO: request available sensors

        // reset all inputs on website
        document.getElementById("api-name").innerText
            = "Lüffi - Lüftungskonzepte überprüfen & analysieren";
        document.getElementById("api-date").innerText = "";
        document.getElementById("api-time").innerText = "";
        document.getElementById("sensor-id-state").innerHTML = "";
        document.getElementById("protocol-event-container").innerHTML = "<!-- Protocol events are " +
            "inserted here either when loading a measurement or when submitting a new event. -->";
        // create new default input field for room usage group
        document.getElementById("room-usage-group-container").innerHTML = "";
        RoomUsage.onAddUserGroup(roomUsageGroupContainer);
    }

    document.getElementById("change-content").onclick = () => {
        changeContent(content === CONTENT.ROOMPLAN ? CONTENT.GRAPH : CONTENT.ROOMPLAN);
    }
    document.getElementById("measurementRoom").onchange = onChangeMeasurementRoom;
    document.getElementById("nav-load-measurement").onclick =
        controller.requestMeasurementList.bind(controller);
    document.getElementById("remove-sensor").onclick = onRemoveSensorClick;

    roomplan.addEventListener('drop', function (event) {
        const elementType = parseInt(event.dataTransfer.getData("type"));
        if (elementType === OBJECT_TYPE.SENSOR) {
            updateUiWithSensor(selectedSensor);
            // delete info-box
            document.getElementById("sensor-info-drag").innerHTML = "";
        }
    });

    // document.getElementById('short-evaluation-average-concentration')
    //     .querySelector('button')
    //     .addEventListener('click', generateAverageCO2Concentrations);

    ImprintModal.init();

    // initialize calibration modal and listeners
    const calibrationModal = document.getElementById("calibrationModal");
    const calibrationModalObject = new bootstrap.Modal(calibrationModal);
    calibrationModal.addEventListener("show.bs.modal", onShowCalibrationModal);
    const calibrationForm = document.getElementById("calibration-form");
    calibrationForm.onsubmit = (event) => startCalibration(calibrationModalObject, event);
    calibrationForm.onchange = validateCalibrationForm;


    // initialize checkbox for traffic-light
    const checkboxTrafficLight = document.getElementById("traffic-light-checkbox");
    checkboxTrafficLight.addEventListener('change',
        () => onSwitchTrafficLight(checkboxTrafficLight));

    // initialize checkbox for fine dust sensor
    const checkboxFineDustSensor = document.getElementById("fine-dust-sensor-checkbox");
    checkboxFineDustSensor.addEventListener('change',
        () => onSwitchFineDustSensor(checkboxFineDustSensor));


    // initialize the roomplan with a default room
    updateRoomData(true);

    // initialize the listeners for changing the displayed values on the roomplan
    const roomplanTypeToggles = document.getElementsByName("roomplan-type");
    const roomplanTypeScaleLegend = document.getElementById('roomplan-type-unit')
    const roomplanTypeScaleCO2 = document.getElementById('co2-scale');
    const roomplanTypeScalePM = document.getElementById('pm-scale');
    let updatingRoomplanTypeToggles = false;
    for (const roomplanTypeToggle of roomplanTypeToggles) {
        roomplanTypeToggle.addEventListener('change', function () {
                // The toggles are going to be updated in selectToggle(),
                // but we just want to re-create the chart once.
                if (!updatingRoomplanTypeToggles) {
                    updatingRoomplanTypeToggles = true;
                    selectToggle(roomplanTypeToggle, roomplanTypeToggles);
                    roomplanSensorDisplayType = roomplanTypeToggle.value;
                    if (roomplanSensorDisplayType === 'co2') {
                        roomplanTypeScaleLegend.innerHTML = 'ppm';
                        roomplanTypeScalePM.classList.add('d-none');
                        roomplanTypeScaleCO2.classList.remove('d-none');
                    } else {
                        roomplanTypeScaleLegend.innerHTML = 'µg/m³';
                        roomplanTypeScaleCO2.classList.add('d-none');
                        roomplanTypeScalePM.classList.remove('d-none');
                        roomplanTypeScalePM.children[0].children[1].innerHTML =
                            (roomplanSensorDisplayType === 'pm25' ?
                                controller.PM25ValueInterpreter : controller.PM10ValueInterpreter)
                                .maxValue.toFixed(1).toString();
                    }

                    displayAt(new Date(controller.currentlyDisplayedTimestamp))
                    updatingRoomplanTypeToggles = false;

                }
            });
    }

    // initialize the Controller for
    initAverageCO2ConcentrationCalculator();
}

export function initAverageCO2ConcentrationCalculator() {
    if (controller.averageCO2ConcentrationCalculator != null) {
        controller.averageCO2ConcentrationCalculator.destroy();
    }
    controller.averageCO2ConcentrationCalculator = new AverageCO2ConcentrationCalculator(
        document.getElementById('average-co2-start'),
        document.getElementById('average-co2-period-length'),
        document.getElementById('short-evaluation-average-concentration-result'),
        controller.currentMeasurementData.timestamps,
        controller.sensorHandler.sensors
    );
    if (controller.currentMeasurementData.timestamp != null) {
        controller.averageCO2ConcentrationCalculator
            .setStartingTime(new Date(controller.currentMeasurementData.timestamp))
    }
}

/**
 * Get the configuration from the measurement configuration screen
 * @return {Promise<{name: string, roomId: number}>}
 */
export function getNewMeasurementConfig() {
    return new Promise( async function (resolve, reject) {
        let measurementConfig = {
            name: document.getElementById("measurementName").value,
            roomId: RoomEditor.getRoomId()
        };
        if (measurementConfig.roomId == null) {
            RoomEditor.setCreateRoom(true);
            RoomEditor
                .saveRoom(true)
                .then(
                    r => {
                        measurementConfig.roomId = r
                    },
                    () => {
                        measurementConfig.roomId = -1;
                    })
            while (measurementConfig.roomId == null) {
                await sleep(50);
            }

        }
        if (measurementConfig.roomId === -1) {
            return reject(measurementConfig);
        }
        return resolve(measurementConfig);
    });
}

export function checkSensorDraggable() {
    let dragMode = controller.mode === MODE.CONFIGURATION ? "true" : "false";
    for (let sensor of roomplan.children) {
        sensor.setAttribute("draggable", dragMode);
    }
}

export function removeSensor(sensor) {
    // deselect sensor if it is selected
    if (selectedSensor === sensor) {
        selectSensor(null);
    }
    // remove sensor from sensor array
    removeElement(findElement("sensor-" + sensor.id, OBJECT_TYPE.SENSOR))
}

export function clearProtocolEventInput() {
    document.getElementById("protocolEventInput").value = "";
}

/**
 * @param {int} timestamp
 * @param {boolean} redraw
 */
export function registerNewTimestamp(timestamp, redraw) {
    timeline.addTimestamp(timestamp, redraw)
}

/**
 * Display the CO2 values off all sensors at a given timestamp
 * @param time {Date}
 */
export function displayAt(time) {
    let displayWarning = false;
    controller.currentlyDisplayedTimestamp = time.getTime()
    controller.sensorHandler.sensors.forEach(sensor => {
        let datapoint = sensor.data[time.getTime()];
        if (datapoint) {
            // set background color of sensor
            let color;
            if (roomplanSensorDisplayType === "pm25") {
                color = controller.PM25ValueInterpreter.getColor(datapoint.pm25);
            } else if (roomplanSensorDisplayType === "pm10") {
                color = controller.PM10ValueInterpreter.getColor(datapoint.pm10);
                document.getElementById('pm-scale')
            } else if (roomplanSensorDisplayType === "co2") {
                color = controller.CO2ValueInterpreter.getColor(datapoint.co2);
            }

            const sensorRE = findElement("sensor-" + sensor.id, OBJECT_TYPE.SENSOR);
            sensorRE.node.style.backgroundColor = `rgb(${[color.r, color.g, color.b].join(',')})`;

            // update sidebar if necessary
            if (sensor === selectedSensor) {
                updateUiWithSensor(sensor,
                    Object.assign({time: time}, datapoint))
            }
            // check whether the sensor's co2 value is too height
            if (datapoint.co2 >= CO2ValueInterpreter.WARNING_THRESHOLD) {
                displayWarning = true;
            }
        }
    });

    timeline.setDisplayedTimestamp(controller.currentlyDisplayedTimestamp);

    // Display a warning if one on the sensor's values is above the warning threshold.
    // Otherwise, remove a possible outdated warning.
    if (displayWarning) {
        document.getElementsByTagName("body")[0].classList.add("warning");
    } else {
        document.getElementsByTagName("body")[0].classList.remove("warning");
    }

    controller.eventHandler.displayAt(time.getTime());
}

/**
 * @param sensor {Sensor | null}
 * @param datapoint {Datapoint, null}
 */
export function updateUiWithSensor(sensor, datapoint = null) {
    if (sensor == null) {
        updateApiField("api-sensor-nr", '-');
        updateApiField("api-sensor-x", '-');
        updateApiField("api-sensor-y", '-');
        updateApiField("api-sensor-battery", '-');
        updateApiField("api-sensor-sensor-time", '-');
        updateApiField("api-sensor-co2", '-');
        updateApiField("api-sensor-humidity", '-');
        updateApiField("api-sensor-temperature", '-');
        updateApiField("api-sensor-pm25", '-');
        updateApiField("api-sensor-pm10", '-');
        updateApiField("api-sensor-height", '-');
        updateApiField("api-sensor-note", '-');
    } else {
        updateApiField("api-sensor-nr", sensor.id);
        updateApiField("api-sensor-x", (sensor.pos.x).toFixed(1));
        updateApiField("api-sensor-y", (sensor.pos.y).toFixed(1));
        updateApiField("api-sensor-battery", sensor.battery, "Batteriespannung nicht verfügbar", v => v.toFixed(1) + ' V');
        updateApiField("api-sensor-height", sensor.height, "Keine Höhe angegeben", v => `${v} m`);
        updateApiField("api-sensor-note", sensor.note, "-");
        if (datapoint != null) {
            updateApiField("api-sensor-time",
                new Date(controller.currentlyDisplayedTimestamp).toLocaleTimeString());
            // update co2 value on api field
            updateApiField("api-sensor-co2", datapoint.co2, "Keine Daten vorhanden", v => v + ' ppm');
            // update humidity value on api field
            updateApiField("api-sensor-humidity", datapoint.humidity,
                "Keine Daten vorhanden", v => v.toFixed(1) + '%');
            // update temperature value on api field
            updateApiField("api-sensor-temperature", datapoint.temperature,
                "Keine Daten vorhanden", v => v.toFixed(1) + ' °C');
            updateApiField("api-sensor-pm25", datapoint.pm25,
                "Keine Daten vorhanden", v => v.toFixed(2) + ' µg/m³');
            updateApiField("api-sensor-pm10", datapoint.pm10,
                "Keine Daten vorhanden", v => v.toFixed(2) + ' µg/m³');

            //updateApiField("api-sensor-time", datapoint.time.toTimeString());
        }
    }
}

export function updateBatteryInfo() {
    updateUiWithSensor(selectedSensor);
}

/**
 * @param name {string}
 * @param timestamp {Date}
 */
export function showMeasurementMetaData(name, timestamp) {
    document.getElementById("api-name").innerText = name + " - ";
    document.getElementById("api-date").innerText = formatDate(timestamp);
    document.getElementById("api-time").innerText = formatTime(timestamp) + " Uhr";
}

/**
 * @param sensor {Sensor | null}
 * @param datapoint {Datapoint, null}
 */
export function showBatteryWarning(sensor, datapoint) {
    if (datapoint.battery === NO_DATA) {
        return;
    }
    const message = document.getElementById('sensor-id-state');
    let node = document.createElement("span");
    node.id = "sensor-id-" + sensor.id;
    if (3.4 > datapoint.battery || 4.95 <= datapoint.battery) {
        if (document.getElementById("sensor-id-" + sensor.id) != null) {
            message.removeChild(document.getElementById("sensor-id-" + sensor.id));
        }
    } else {
        if (document.getElementById("sensor-id-" + sensor.id) == null) {
            if (message.hasChildNodes()) {
                node.innerHTML = ", " + sensor.id;
            } else {
                node.innerHTML = sensor.id;
            }
            message.appendChild(node);
            if (message.firstChild.innerText.charAt(0) === ",") {
                message.firstChild.innerText = message.firstChild.innerText.substr(2, message.firstChild.innerText.length);
            }
        }
    }
}

/**
 * Sets the inner HTML of all elements which have the given class name to a given value.
 * <br />
 * Alternative: use <code>data-api="name"</code> properties
 * @param name {string} The class name of the elements to update
 * @param value {number, string} value to set
 * @param [defaultValue] {string | number} value to be used when <code>value</code> is {@link NO_DATA}.
 * @param [modifier] {function(any)} function which modifies the value when it is not {@link NO_DATA}.
 */
export function updateApiField(name, value, defaultValue = undefined, modifier = undefined) {
    if ((value == NO_DATA || value == null) && defaultValue) {
        value = defaultValue;
    } else if (modifier) {
        value = modifier(value);
    }
    let fields = document.getElementsByClassName(name);
    for (let i = 0; i < fields.length; i++) {
        fields[i].innerHTML = value;
    }
}

/**
 * Add a event in the protocol list on the sidebar.
 * @param {RoomplanElementEvent | NoteEvent | RoomUsageEvent} event
 */
export function addEvent(event) {
    const icon = controller.eventHandler.getIconForEventType(event.type);
    let text = "";
    text = event.getDescription();

    const target = event.roomplanElement?.id == null ? '' : event.roomplanElement.id;
    const time = formatTime(new Date(event.timestamp), true);
    if (text != null) {
        let protocolEntryElement = createElementFromHTML(`
                    <div class="col-12 protocol-entry"
                         data-protocol-type="${event.type}"
                         data-protocol-target="${target}"
                         data-protocol-timestamp="${event.timestamp}">
                        <div class="row">
                            <div class="col-1 me-1"><i class="${icon}"></i></div>
                            <div class="col">${text}</div>
                            <div class="col-3">${time}</div>
                        </div>
                    </div>
            `);
        protocolEventContainer.appendChild(protocolEntryElement);
    }
}

/**
 * after start of new measurement, this function saves the note and room usage events,
 * which written in configuration of the measurement
 */
export function saveStartEvents() {
    createNewProtocolEvent();
    RoomUsage.createNewRoomUsageEvent(controller);
}

/**
 * @param measurements {StoredMeasurementInfo[]}
 */
export function showStoredMeasurementList(measurements) {
    let modal = document.getElementById('loadMeasurementModal')
    let modalObject = new bootstrap.Modal(modal, {
        keyboard: false
    })
    let list = document.getElementById('loadMeasurementModalList')
    let loadButton = modal.querySelector('[data-load-measurement="true"]');
    let deleteButton = modal.querySelector('[data-delete-measurement="true"]');
    if (measurements.length === 0) {
        list.innerHTML = '<li class="list-group-item">Keine Messungen vorhanden</li>';
        loadButton.classList.add("disabled");
        deleteButton.classList.add("disabled");
    } else {
        let measurementListItems = "";
        measurements.forEach((info) => {
            let date = new Date(info.timestamp);
            let room =  controller.rooms.find(r => r.id === info.room);

            measurementListItems +=
                `<li class="list-group-item" data-measurement-id="${info.id}" data-measurement-name="${info.name}">`
                + '<div class="row">'
                + '<div class="col-6">' + info.name + '</div>'
                + '<div class="col-3">' + room.name + '</div>'
                + '<div class="col-3">' + formatDate(date) + '</div>'
                + '</div>'
                + '</li>';
        })
        list.innerHTML = measurementListItems;
        list.children[0].classList.add("active");
        for (let i = 0; i < list.children.length; i++) {
            list.children[i].addEventListener('click', function () {
                let active = list.getElementsByClassName("active")
                if (active && active.item(0) !== null) {
                    active.item(0).classList.remove("active")
                }
                this.classList.add("active");
            }, false);
        }
        loadButton.classList.remove("disabled")
        loadButton.addEventListener('click',
            onLoadMeasurementSubmitButtonClicked.bind(this, modalObject, list),
            {once: true}
            )
        deleteButton.classList.remove("disabled")
        deleteButton.addEventListener('click',
            () => onDeleteMeasurementSubmitButtonClicked(modalObject, list),
            {once: true}
            )
    }
    modalObject.show()
}

/**
 * Create a confirmation dialog
 * @param title {string}
 * @param message {string}
 * @param onPositive {function}
 * @param onNegative {function}
 * @param type {int | Object} specifies the dialog's type - either one of the predefined types
 * from {@see ConfirmationDialogType} or an <code>Object</code> which contains the text
 * for the <code>positive</code> and <code>negative</code> buttons.
 * @param type.positive {string}
 * @param type.negative {string}
 * @param context {Object}
 */
export function askForConfirmation(title, message, onPositive, onNegative, type, context) {
    return new ConfirmationDialog(title, message, onPositive, onNegative, type, context);
}

export function isCreateNewMeasurementRoomSelected () {
    return document.getElementById("measurementRoom").value === "0";
}

/**
 * Reset UI
 * @param softReset {boolean}
 */
export function reset(softReset) {
    timeline.reset();
    if (chart != null) {
        chart.destroy();
    }
    let removedSensors = document.getElementById("removed-sensors-list").children;
    if (softReset) {
        ROOMPLAN_ELEMENTS.sensors.forEach((sensor) => {
            sensor.node.style.backgroundColor = "grey"
        });
    } else {
        roomplan.innerHTML = "";
        selectSensor(null);
        for (let removedSensor of removedSensors) {
            removedSensor.remove();
        }
    }
    initAverageCO2ConcentrationCalculator();
}

/**
 * @param {Room[]} rooms
 */
export function setRooms(rooms) {
    let roomSelect = document.getElementById("measurementRoom");
    roomSelect.length = 0;
    roomSelect.innerHTML = '<option value="0">Raum anlegen</option>';
    rooms.forEach((_room, index) => {
        roomSelect[index + 1] = new Option(_room.name, _room.id, false, false)
    });
}

export function checkChart() {
    if (controller.mode === MODE.CONFIGURATION) {
        if (chart != null) {
            chart.destroy.call(chart);
            chart = null;
        }
    }
}

export function addSingleDatasetToChart(data) {
    if (chart != null) {
        // chart can be null when a measurement is received
        // and the chart is not yet initialized
        chart.addSingleDataset(data);
    }
}

export function changeContent(newContent) {
    if (content === newContent) return;
    const oldContent = content;
    const classList = document.body.classList;

    if (oldContent === CONTENT.ROOMPLAN) {
        classList.remove("content-roomplan")
    } else if (oldContent === CONTENT.GRAPH) {
        classList.remove("content-graph")
    }

    if (newContent === CONTENT.ROOMPLAN) {
        classList.add("content-roomplan")
        RoomEditor.resizeRoomplanArea();
    } else if (newContent === CONTENT.GRAPH) {
        classList.add("content-graph")
        if (chart === null || chart === undefined) {
            if (controller.sensorHandler.sensors.length > 0) {
                const chartContainer = document.getElementById("chart-container");
                chartContainer.classList.add("loading");
                forceRedraw(chartContainer);
                const i = async () => {
                    if (Object.keys(controller.sensorHandler.sensors[0].data).length === 0) {
                        chartContainer.classList.add("no-data");
                        if (controller.mode === MODE.MEASUREMENT) {
                            setTimeout(i, 1000);
                        } else {
                            chartContainer.classList.remove("loading");
                        }
                        return;
                    }
                    chartContainer.classList.remove("no-data")
                    chart = ChartHandler.createDiagramFromMeasurementData(
                        document.getElementById("evaluationCanvas"),
                        controller.sensorHandler.sensors
                    );
                    chart.registerTypeToggle(
                        chartContainer.querySelectorAll('input[name="chart-type"]')
                    );
                    chart.registerDisplayAverageToggle(
                        chartContainer.querySelectorAll('input[name="chart-average"]')
                    );
                    chart.registerOnHoverCallback(highlightClosestEvent)
                    // Ensure that the CO2 chart type toggle is selected by default,
                    // because the chart displays the CO2 concentration by default.
                    document.getElementById('chart-type-co2').click();
                    // Same thing for the button disabling the average mode.
                    document.getElementById('chart-average-off').click();
                    chartContainer.classList.remove("loading");
                }
                i();
            }
        }
    }

    content = newContent;

}

export function isPlaybackActive() {
    return timeline.isPlaybackActive;
}

export function updateRoomImage(_room) {
    // no need to implement method for this controller
}

function highlightClosestEvent(position, time) {
    const localizedMillis = getLocalizedDateForTimeString(
        new Date(controller.currentMeasurementData.timestamp), time);
    if (Number.isNaN(localizedMillis)) {
        return;
    }
    // const localizedDate = new Date(localizedMillis);
    // console.log(localizedDate.toLocaleTimeString());
    const protocolEls = protocolEventContainer.querySelectorAll('[data-protocol-timestamp]');
    let highlighted = false;
    for (let i = 0; i < protocolEls.length; i++) {
        protocolEls[i].classList.remove('highlighted-protocol-entry');
        const elTimestamp = parseInt(protocolEls[i].dataset.protocolTimestamp);
        if (elTimestamp > localizedMillis && !highlighted) {
            highlighted = true;
            const index = i === 0 ? 0 : i - 1;
            protocolEls[index].classList.add('highlighted-protocol-entry');
            const bounding = protocolEls[index].getBoundingClientRect();
            if (bounding.top < 0 || bounding.bottom > window.innerHeight) {
                protocolEls[index].scrollIntoView({behavior: "smooth"})
            }
        }
    }
    if (!highlighted && protocolEls.length !== 0) {
        protocolEls[protocolEls.length - 1].classList.add('highlighted-protocol-entry');
    }
}
