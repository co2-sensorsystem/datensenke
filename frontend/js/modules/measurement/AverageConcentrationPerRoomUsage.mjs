import {NO_DATA} from "../../../config.js";
import {getLocalizedDateForTimeString} from "../utils.mjs";

export class AverageCO2ConcentrationCalculator {
    /**
     * @type {HTMLInputElement}
     */
    #startInput;
    /**
     * @type {HTMLInputElement}
     */
    #durationInput;
    /**
     * @type {HTMLDivElement}
     */
    #outputDiv;
    /**
     * @type {number[]}
     */
    #timestamps;
    /**
     * @type {Sensor[]}
     */
    #data;

    /**
     * @type Object
     * @property startInput
     * @property durationInput
     */
    #listener;
    /**
     *
     * @param {HTMLInputElement} startInput
     * @param {HTMLInputElement} durationInput
     * @param {HTMLDivElement} outputDiv
     * @param {number[]} timestamps
     * @param {Sensor[]} data
     */
    constructor(startInput, durationInput, outputDiv, timestamps, data) {
        this.#startInput = startInput;
        this.#durationInput = durationInput;
        this.#outputDiv = outputDiv;
        this.#timestamps = timestamps;
        this.#data = data;

        this.#listener = {
            startInput: this.#onStartChanged.bind(this),
            durationInput: this.#calculateAverageConcentration.bind(this)
        }
        this.#startInput.addEventListener('change', this.#listener.startInput);
        this.#durationInput.addEventListener('change', this.#listener.durationInput);
    }

    /**
     * @return {boolean}
     */
    #isStartingTimeValid() {
        const end = this.#timestamps[this.#timestamps.length - 1];
        const start = this.#timestamps[0];
        const newStart = this.#getStartingTime()?.getTime();
        return newStart !== null && newStart >= start && newStart < end;
    }

    /**
     * @return {null|Date}
     */
    #getStartingTime() {
        if (this.#timestamps.length === 0) return null;
        const start = new Date(getLocalizedDateForTimeString(
            new Date(this.#timestamps[0]), this.#startInput.value));
        const t = start.getTime();
        if (t < this.#timestamps[0]
            && t + 60 * 1000 > this.#timestamps[0]) {
            return new Date(this.#timestamps[0]);
        }
        return start;
    }

    #calculateAverageConcentration(e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.#timestamps.length === 0) return;
        const duration = parseInt(this.#durationInput.value);
        const durationMillis = duration * 60 * 1000;
        let start = this.#getStartingTime().getTime();

        let timestamps = {};
        for (const t of this.#timestamps) {
            if (t >= start) {
                timestamps[t] = {n: 0, s: 0, a:0};
            }
        }
        this.#data.forEach(s => {
            Object.entries(timestamps).forEach((t) => {
                const tt = timestamps[t[0]];
                if (s.data[t[0]].co2 !== NO_DATA) {
                    tt.s += s.data[t[0]].co2;
                    tt.n++;
                }
            })
        });
        let periods = [];
        let currentPeriod;
        Object.entries(timestamps).forEach((o) => {
            const t = parseInt(o[0]);
            if (!currentPeriod || currentPeriod.end < t) {
                if (currentPeriod) {
                    currentPeriod.average = Math.round(currentPeriod.sum / currentPeriod.number);
                    periods.push(currentPeriod);
                }
                currentPeriod = {
                    start: t,
                    end: (currentPeriod ? currentPeriod.end : t) + durationMillis,
                    average: 0,
                    sum: 0,
                    number: 0
                };
            }
            // average per timestamp
            o[1].a = o[1].s / o[1].n;
            currentPeriod.number++;
            currentPeriod.sum += o[1].a;
        });
        currentPeriod.average = Math.round(currentPeriod.sum / currentPeriod.number);
        periods.push(currentPeriod);
        let result = '';

        periods.forEach((p, i) => {
            result += `<div class="col">Nutzungseinheit ${i + 1}: ${p.average} ppm</div>`;
        });
        this.#outputDiv.innerHTML = result;
    }

    setStartingTime(date) {
        const s = date.toLocaleTimeString().split(':');
        this.#startInput.value = s[0] + ':' + s[1];
    }

    destroy() {
        this.#startInput.removeEventListener('change', this.#listener.startInput);
        this.#durationInput.removeEventListener('change', this.#listener.durationInput);
    }

    #onStartChanged(e) {
        e.stopPropagation();
        if (this.#isStartingTimeValid()) {
            this.#startInput.classList.remove('is-invalid');
            this.#calculateAverageConcentration();
        } else {
            this.#startInput.classList.add('is-invalid');
        }
    }
}