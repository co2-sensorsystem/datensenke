import * as MeasurementViewUi from "./MeasurementViewUi.mjs";
import {dLog} from "../../../config.js";

/** @type MeasurementViewController */
let controller;
/**
 * @type {bootstrap.Tooltip}
 */
let tooltip;
/**
 * @type {HTMLDivElement}
 */
let tooltipEl;

let initialized = false;
let startTime = -1, endTime = -1, currentIndex = -1;
let points = [];
let timelineContainer;
let beforeEl, currentEl, afterEl;
let maxAnimationDuration = 60 * 1000; // 60s
let maxDisplayTimestampDuration = 500; // 0,5s
let tooltipHidden;
/**
 * @type {boolean}
 */
export let isPlaybackActive = false;
function onTimestampClick (event) {
    let element = event.target;
    let timestamp = parseInt(element.dataset.timestamp);
    MeasurementViewUi.displayAt(new Date(timestamp));
}
function getIndex(timestamp) {
    for (let i = 0; i < points.length; i++) {
        if (points[i].timestamp === timestamp) {
            return i;
        }
    }
    return -1;
}

function resize(index) {
    // TODO: do not use three divs which are resized, but just one div which changes the left value
    //       => significant performance increase
    // hide the before / after elements if current is the first or last element
    if (index === 0) {
        beforeEl.classList.add("d-none");
    } else {
        beforeEl.classList.remove("d-none");
    }
    if (index === points.length - 1) {
        afterEl.classList.add("d-none");
    } else {
        afterEl.classList.remove("d-none");
    }

    let countBefore = index;
    let countAfter = points.length - index - 1;

    beforeEl.style.width = 100 * countBefore / points.length + "%";
    currentEl.style.width = 100 / points.length + "%";
    afterEl.style.width = 100 * countAfter / points.length + "%";

    // update tooltip / currently displayed time and its position
    if (tooltipHidden) {
        tooltip.show();
        tooltipHidden = false;
    }

    /*
    Sometimes when you reload the page during a measurement,
    you get this error in the console:
    Uncaught TypeError: this._config is null
    This error comes from bootstrap.
     */
    // TODO: maybe fix better, if possible
    if (tooltipEl != undefined) {
        tooltipEl.innerText = points[currentIndex].label;
        tooltip.update();
    }
    tooltip.update();
}

/**
 * @param {int} index
 * @param {boolean} [changeRoomplan]
 */
function goToIndex(index, changeRoomplan) {
    currentIndex = index
    if (changeRoomplan) {
        MeasurementViewUi.displayAt(new Date(points[index].timestamp));
    } else {
        resize(index)
    }
}

function goToLatest() {
    isPlaybackActive = false
    goToIndex(points.length - 1, true)
}

function playLoop(index) {
    if (isPlaybackActive) {
        if (points[index] != null) {
            MeasurementViewUi.displayAt(new Date(points[index].timestamp));
            //goToIndex(index, true)
            let timeout = Math.min(
                maxDisplayTimestampDuration,
                (maxAnimationDuration / points.length) >> 0);
            if (++index < points.length) {
                setTimeout(playLoop, timeout, index)
            } else {
                // latest datapoint reached; finishing playback
                pause()
            }
        }
    }
}

/**
 * @param {MeasurementViewController} _controller
 */
export function init (_controller) {
    controller = _controller
    currentIndex = 0;
    if (!initialized) {
        timelineContainer = document.getElementById("timeline");
        beforeEl = document.getElementById("timeline-before");
        currentEl = document.getElementById("timeline-current");
        afterEl = document.getElementById("timeline-after");
        document.querySelectorAll('[data-timeline-action]').forEach((element) => {
            new bootstrap.Tooltip(element, {container: document.getElementById("footer")});
            switch (element.dataset.timelineAction) {
                case "skip-start":
                    element.addEventListener('click', () => goToIndex(0, true));
                    break;
                case "skip-end":
                    element.addEventListener('click', goToLatest);
                    break;
                case "play":
                    element.addEventListener('click', play);
                    break;
                case "pause":
                    element.addEventListener('click', pause);
                    break;
            }
        });

        timelineContainer.onmousemove = function (event) {
            let width = this.offsetWidth;
            let position = event.offsetX;
            let percentage = position / width;
            let index = (points.length * percentage) >> 0;
            goToIndex(index, true);
        };
        initialized = true;
    }
    if (tooltip != null) {
        dLog("Tooltip disposed");
        tooltip.dispose();
    }
    tooltip = bootstrap.Tooltip.getOrCreateInstance(currentEl,
        {container: document.getElementById("footer")});
    tooltipHidden = true;
    // Add tooltip to DOM allowing to change its message later.
    // If this is done at a later point of time, we can get a NPE.
    const tooltipInitShowListener = () => {
        const tooltipDescribedBy = currentEl.getAttribute("aria-describedBy")
        const tooltipElO = document.getElementById(tooltipDescribedBy);
        // if (tooltipEl == null
        //     || (tooltipElO != null && tooltipEl != tooltipElO.getElementsByClassName("tooltip-inner")[0])) {
        //     tooltipEl = tooltipElO.getElementsByClassName("tooltip-inner")[0];
        // }
        tooltipEl = tooltipElO.getElementsByClassName("tooltip-inner")[0];
        tooltip.hide();
        tooltipHidden = true;
        currentEl.removeEventListener('shown.bs.tooltip', tooltipInitShowListener);
    }
    currentEl.addEventListener('shown.bs.tooltip', tooltipInitShowListener);

    tooltip.show();
}
export function reset() {
    points = []
    startTime = -1
    endTime = -1
    init(controller)
}

export function play() {
    if (currentIndex === points.length - 1) {
        // jump to the beginning if the playback is started
        // while the last timestamp is displayed
        goToIndex(0, true)
    }
    isPlaybackActive = true;
    document.querySelectorAll('[data-timeline-action="play"]').forEach((element) => {
        element.classList.add("d-none")
    })
    document.querySelectorAll('[data-timeline-action="pause"]').forEach((element) => {
        element.classList.remove("d-none")
    })
    playLoop(getIndex(controller.currentlyDisplayedTimestamp))
}
export function pause() {
    isPlaybackActive = false;
    document.querySelectorAll('[data-timeline-action="play"]').forEach((element) => {
        element.classList.remove("d-none")
    })
    document.querySelectorAll('[data-timeline-action="pause"]').forEach((element) => {
        element.classList.add("d-none")
    })
}
/**
 * @param {int} timestamp
 * @param {boolean} update
 */
export function addTimestamp(timestamp, update) {
    if (points.length === 0) {
        points = [];
        startTime = timestamp;
    }

    let label = (new Date(timestamp)).toLocaleTimeString();

    points.push({timestamp: timestamp, label: label});
    if (update) {
        resize(currentIndex)
    }
}
/**
 * @param {int} timestamp
 */
export function setDisplayedTimestamp(timestamp) {
    for (let i = 0; i < points.length; i++) {
        if (points[i].timestamp === timestamp) {
            goToIndex(i, false);
            break;
        }
    }
}

