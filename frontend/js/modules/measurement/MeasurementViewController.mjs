import * as MeasurementViewUi from "./MeasurementViewUi.mjs";
import {
    ROOMPLAN_ELEMENTS, findElement
} from "../room/RoomplanElement.mjs";
import {createRoomplanElement} from "../room/RoomplanElementFactory.mjs";
import * as CommunicationHandler from "../CommunicationHandler.mjs";
import * as ErrorModal from "../ui/modals/ErrorModal.mjs";
import {
    EventHandler,
    EVENT,
    RoomplanElementEvent,
    NoteEvent,
    RoomUsageEvent, EventTypeToState
} from "../EventHandler.mjs";
import {RoomUsageGroup} from "../simulation/RoomUsage.mjs";
import {OBJECT_TYPE, MODE, CONTENT} from "../../../config.js";
import * as RoomEditor from "../room/roomEditor.mjs";
import {
    CO2ValueInterpreter, PMValueInterpreter
} from "./MeasurementValueInterpreter.mjs";
import {AverageCO2ConcentrationCalculator} from "./AverageConcentrationPerRoomUsage.mjs";
import {updateUiWithSensor} from "./MeasurementViewUi.mjs";
import {updateRoomImage} from "../simulation/simulationController.mjs";

/**
 * @type MeasurementViewController
 */
export let controller;
window.onload = () => {
    if (!controller) {
        // create application
        controller = new MeasurementViewController();
        CommunicationHandler.init(controller);
    }
};


class MeasurementViewController {

    /**
     * @readonly
     * @type {string}
     * @see MODE
     */
    mode
    /**
     * Data about the current measurement
     * @type {Object}
     * @property {Room} room room info
     * @property {string} name measurement id
     * @property {int} id measurement id
     * @property {int[]} timestamps - timestamps of all measurement cycles
     * @property {int[]} events - timestamps of all {@link Event}s;
     *           if the measurement has not been started yet, but events have been added
     *           during the configuration phase,
     */
    currentMeasurementData = {
        room : {
            length: 0,
            width: 0
        },
        name: "",
        id: -1,
        timestamps: [],
        events:[]
    }

    /**
     * Timestamp in milliseconds of the dataset displayed currently
     * @type {number}
     */
    currentlyDisplayedTimestamp = -1

    /**
     * @type {SensorHandler}
     */
    sensorHandler = new SensorHandler(this);
    eventHandler = new EventHandler();
    /**
     * List of rooms which are already stored in the database
     * @type {Room[]}
     */
    rooms = [];

    /**
     * Whether the shown measurement was recorded before in this session.
     * @type {boolean}
     */
    wasMeasurementRecorded = false;

    /**
     *
     * @type {boolean}
     */
    isLoadingMeasurement = false;

    CO2ValueInterpreter = new CO2ValueInterpreter();
    PM25ValueInterpreter = new PMValueInterpreter();
    PM10ValueInterpreter = new PMValueInterpreter();

    /**
     * Initialized in {@link MeasurementViewUi.initialize()}
     * @type AverageCO2ConcentrationCalculator
     */
    averageCO2ConcentrationCalculator;

    constructor() {
        MeasurementViewUi.initialize(this);

        // Initial mode is configuration.
        // This frontend instance can be disabled by the server
        // if another instance is already registered with it.
        this.setMode(MODE.CONFIGURATION);
    }

    setMode (newMode) {
        if (this.mode === newMode) return;
        const oldMode = this.mode;
        const list = document.body.classList;

        if (this.mode === MODE.CONFIGURATION) {
            list.remove("mode-configuration");
        } else if (this.mode === MODE.MEASUREMENT) {
            list.remove("mode-measurement");
        } else if (this.mode === MODE.VIEWER) {
            list.remove("mode-viewer")
        } else if (this.mode === MODE.BLOCKED) {
            list.remove("mode-blocked")
        }

        if (newMode === MODE.CONFIGURATION) {
            list.add("mode-configuration");
        } else if (newMode === MODE.MEASUREMENT) {
            list.add("mode-measurement");
        } else if (newMode === MODE.VIEWER) {
            list.add("mode-viewer");
        } else if (newMode === MODE.BLOCKED) {
            list.add("mode-blocked")
        }

        this.mode = newMode;
        // The footer might have been hidden or displayed
        // and thus the space for the roomplan might have been changed.
        RoomEditor.resizeRoomplanArea();
        // Sensors are only draggable in configuration mode.
        // This needs to be enforced when either the old or new mode was the config mode.
        if (oldMode === MODE.CONFIGURATION || newMode === MODE.CONFIGURATION) {
            MeasurementViewUi.checkSensorDraggable();
            MeasurementViewUi.checkChart();
        }
        if (newMode === MODE.CONFIGURATION) {
            // ensure that sensor positioning can be done
            MeasurementViewUi.changeContent(CONTENT.ROOMPLAN);
            // FIXME: get list of all currently available sensor nodes
        }

        // trigger mode change event to which other parts of the application listen to
        document.dispatchEvent(new Event('app.mode', {oldMode: oldMode, newMode: newMode}));
    }

    /**
     * Method called to add a Sensor to the application
     * @param {int} id
     * @param {string} mac
     * @param {Coordinate | undefined} position
     * @param {number | undefined} battery
     * @param {string} LED
     * @param {number} height
     * @param {string} note
     */
    addSensor (id, mac, position = undefined, battery = undefined,
               LED = undefined, height = undefined, note = "") {
        if (this.sensorHandler.getSensorById(id) != null) {
            return; // sensor with the same id does already exist
        }
        // If no position was given, place the sensor in the middle of the room
        // TODO: idea for later: place them in the "removed sensors" section
        //  and rename that section to "available sensors"
        if (!position) {
            position = {
                x: 50,
                y: 50
            }
        }
        const sensor = new Sensor(id, mac, position, [], height, note)
        if (battery !== undefined) {
            sensor.battery = battery;
        }
        if (LED !== undefined) {
            sensor.LED = LED;
        }
        this.sensorHandler.addSensor.call(this.sensorHandler, sensor)
        createRoomplanElement(
            MeasurementViewUi.roomplan, OBJECT_TYPE.SENSOR, position, "sensor-" + id, sensor,
            MeasurementViewUi.contextMenuHandler, controller);
        //UiHandler.addSensor(sensor)
        MeasurementViewUi.checkSensorDraggable()
    }

    /**
     * Remove a sensor from the system
     * @param {Sensor} sensor
     */
    removeSensor (sensor) {
        this.sensorHandler.removeSensor(sensor)
        MeasurementViewUi.removeSensor(sensor)
    }

    /**
     * Handle a single data set with sensor data to the data store and trigger UI updates for it.
     * @public
     * @type function
     * @param {JSON} newData
     * @param {number} newData.timestamp
     * @param {SingleMeasurementData} newData.sensorknots
     */
    handleSingleDataSet (newData) {
        this.currentMeasurementData.timestamps.push(newData.timestamp)
        let timestamp = new Date(newData.timestamp);
        this.sensorHandler.sensors.forEach(
            (sensor) => {
                newData.sensorknots.forEach((sensorInfo) => {
                    if (sensorInfo.id === sensor.id) {
                        // convert data for UI handler
                        /**
                         * @type {Datapoint}
                         */
                        let datapoint = Object.assign({time: timestamp}, sensorInfo);
                        MeasurementViewUi.showBatteryWarning(sensor, datapoint);
                        sensor.data[newData.timestamp] = datapoint;
                        if (datapoint.pm25 > this.PM25ValueInterpreter.maxValue) {
                            this.PM25ValueInterpreter.maxValue = datapoint.pm25;
                        }
                        if (datapoint.pm10 > this.PM10ValueInterpreter.maxValue) {
                            this.PM10ValueInterpreter.maxValue = datapoint.pm10;
                        }
                    }
                });
            });
        MeasurementViewUi.addSingleDatasetToChart(newData);
    }

    /**
     *
     * @param {RoomplanElementEvent | NoteEvent | RoomUsageEvent} event
     */
    addEvent (event) {
        this.eventHandler.addEvent(event);
        MeasurementViewUi.addEvent(event);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //// COMMUNICATION WITH THE BACKEND
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**********************************************************************************************
     * * * SENDING DATA TO THE BACKEND
     *********************************************************************************************/

    /********************* SEND COMMANDS *********************/

    /**
     * Send the measurement configuration to the backend.
     * This starts collecting data from sensors.
     */
    prepareRoomInformation () {

        this.wasMeasurementRecorded = true;
        const ctx = this;
        function f(newMeasurementConfig) {
            if (MeasurementViewUi.isCreateNewMeasurementRoomSelected()) {
                console.log("newMeasurementconfig", newMeasurementConfig)
                ctx.currentMeasurementData.room = {
                    id: newMeasurementConfig.roomId
                };
            } else {
                // noinspection EqualityComparisonWithCoercionJS
                ctx.currentMeasurementData.room =
                    ctx.rooms.filter((room) => room.id == newMeasurementConfig.roomId)[0];
            }
            ctx.currentMeasurementData.name = newMeasurementConfig.name;
            let objectConfig = [];
            ctx.sensorHandler.sensors.forEach((sensorInfo) => {
                const sensorConfig = {
                    id: sensorInfo.id,
                    position: sensorInfo.pos,
                    type: OBJECT_TYPE.SENSOR,
                    data: {height: sensorInfo.height || 1.2, note: sensorInfo.note || null}
                };
                objectConfig.push(sensorConfig)
            });

            /**
             * @param {RoomplanElement} roomplanElement
             */
            function prepareRoomplanElementObject(roomplanElement) {
                let objData = roomplanElement.data || {};
                objData.state = roomplanElement.state;
                let type = roomplanElement.getObjectType();

                objectConfig.push({
                    id: roomplanElement.id,
                    type: type,
                    position: roomplanElement.position,
                    data: objData
                })
            }

            ROOMPLAN_ELEMENTS.doors.forEach((windowElement) => {
                prepareRoomplanElementObject(windowElement);
            });
            ROOMPLAN_ELEMENTS.windows.forEach((airFilterElement) => {
                prepareRoomplanElementObject(airFilterElement);
            });
            ROOMPLAN_ELEMENTS.airFilters.forEach((airFilterElement) => {
                prepareRoomplanElementObject(airFilterElement);
            });

            const config = {
                event: "newMeasurementConfig",
                objects: objectConfig,
                room: ctx.currentMeasurementData.room,
                name: ctx.currentMeasurementData.name
            };
            CommunicationHandler.send(config, {
                onError: (errorInfo) => {
                    ErrorModal.open("Messung konnte nicht angelegt werden", errorInfo);
                },
                // TODO: success in own function
                onSuccess: (data) => {
                    ctx.currentMeasurementData.id = data.id;
                    if (data.roomId) {
                        ctx.currentMeasurementData.room.id = data.roomId;
                        RoomEditor.sendRoomplanImage(ctx.currentMeasurementData.room, ctx.currentMeasurementData.room.image)
                    }
                    ctx.setMode(MODE.MEASUREMENT)
                    CommunicationHandler.send(
                        {event: "start", id: data.id},
                        {
                            onError: (error) => {
                                ErrorModal.open(
                                    "Messung konnte nicht gestartet werden", error)
                            },
                            onSuccess: () => {
                                // show measurement name and date at website, date without function
                                let timestamp = Date.now();
                                ctx.currentMeasurementData.timestamp = timestamp;
                                MeasurementViewUi.showMeasurementMetaData(
                                    ctx.currentMeasurementData.name, new Date(timestamp));
                            },
                            context: ctx
                        })
                    MeasurementViewUi.saveStartEvents();
                },
                context: ctx
            })
        }
        MeasurementViewUi.getNewMeasurementConfig().then(f, () => {});

    }

    /**
     * Stop measurement
     */
    stopMeasurement () {
        CommunicationHandler.send({event: "stop"},
            {
                context: this,
                onError: function (error) {
                    ErrorModal.open("Konnte Messung nicht stoppen", error);
                },
                onSuccess: function () {
                    this.setMode(MODE.VIEWER)
                }
            })
    }

    /**
     * Store an event in the database and register it with the UI.
     * @param chosenEvent {RoomplanElementEvent | NoteEvent | RoomUsageEvent}
     * @param id {string | null}
     */
    sendEvent (chosenEvent, id) {
        const text = chosenEvent ? {text: chosenEvent.text} : null;

        let ageLevel;
        let activity;
        let personCount;

        /**
         * @type {RoomUsageGroup[]}
         */
        let roomUsage = [];
        let data;

        if (chosenEvent.type === EVENT.ROOM_USAGE_CHANGED) {
            chosenEvent.roomUsageGroups.forEach(element => {
                ageLevel = element.ageLevel;
                activity = element.activity;
                personCount = element.personCount;

                roomUsage.push(new RoomUsageGroup(ageLevel, activity, personCount));
            });
            data = Object.assign({type: chosenEvent.type}, chosenEvent.data, text, {roomUsage: roomUsage});
        } else {
            data = Object.assign({type: chosenEvent.type}, chosenEvent.data, text);
        }

        if (data != null) {
            CommunicationHandler.send(
                {
                    event: "addProtocolEntry",
                    measurement: this.currentMeasurementData.id,
                    timestamp: chosenEvent.timestamp,
                    id: id,
                    type: chosenEvent.type,
                    data: data
                },
                {
                    context: this,
                    onError: function (error) {
                        ErrorModal.open("Konnte Event nicht speichern", error);
                    },
                    onSuccess: function () {
                        if (chosenEvent instanceof NoteEvent) {
                            MeasurementViewUi.clearProtocolEventInput();
                        }
                        this.addEvent(chosenEvent);
                    }
                }
            )
        } else {
            ErrorModal.open("Event konnte nicht gespeichert werden", error);
        }
    }

    /********************* REQUEST DATA *********************/

    /**
     * Request loading a measurement from the database
     * @param id {int}
     */
    requestMeasurement(id) {
        CommunicationHandler.send(
            {event: "sendSingleMeasurement", id: id},
            {
                context: this,
                onError: function (error) {
                    ErrorModal.open("Messung konnte nicht geladen werden", error);
                },
                onSuccess: function (measurement) {
                    this.wasMeasurementRecorded = false;
                    this.onMeasurementReceived(measurement, false)
                }
            })
    }

    requestMeasurementList() {
        CommunicationHandler.send(
            {
                event: "sendMeasurementList"
            },
            {
                context: this,
                /**
                 * @param {StoredMeasurementInfo[]} measurementList
                 */
                onSuccess: function (measurementList) {
                    MeasurementViewUi.showStoredMeasurementList(measurementList)
                },
                onError: function (error) {
                    ErrorModal.open(
                        "List der vorhandenen Messungen konnte nicht geladen werden",
                        error)
                }
            });
    }

    /**********************************************************************************************
     * * * RECEIVING DATA FROM THE BACKEND
     *********************************************************************************************/

    /**
     * @param {Sensor[]} sensorknots
     */
    onLoggedInSensorListReceived(sensorknots) {
        sensorknots.forEach((sensor) => {
            this.addSensor(sensor.id, sensor.mac,
                sensor.position | sensor.pos | undefined, sensor.battery, sensor.LED)
        })
    }

    onRoomsReceived(rooms) {
        this.rooms = rooms
        MeasurementViewUi.setRooms(rooms)
    }

    /**
     * Handle events which are caused by the sensor,
     * e.g. logging in or out
     * @param {Object} eventInfo
     * @param {string} eventInfo.event Event type, either "login" or "logout"
     * @param {int} eventInfo.id
     * @param {string} eventInfo.mac
     * @param {number} eventInfo.battery
     * @param {string} eventInfo.LED
     */
    onSensorWifiEventReceived (eventInfo) {
        if (eventInfo.event === "login") {
            if (this.mode === MODE.CONFIGURATION) {
                this.addSensor(eventInfo.id, eventInfo.mac, undefined, eventInfo.battery, eventInfo.LED)
            }
        } else if (eventInfo.event === "logout") {
            if (this.mode === MODE.CONFIGURATION) {
                this.removeSensor(this.sensorHandler.getSensorById(eventInfo.id));
            }
        }
    }

    /**
     *
     * @param {Object[]} knots
     * @param {string} knots.mac
     * @param {int} knots.id
     * @param {number} knots.battery
     */
    onBatteryStatusReceived(knots) {
        const s = this.sensorHandler;
        knots.forEach((knot) => {
            const sensor = s.getSensorById(knot.id);
            if (sensor != null) { // can be the case when viewing an old measurement
                sensor.battery = knot.battery;
            }
        });
        MeasurementViewUi.updateBatteryInfo();
    }

    /**
     *
     * @param data {Object}
     * @param data.timestamp {int}
     * @param data.sensorknots {Array}
     */
    onSingleDataSetReceived (data) {
        console.log(data)
        let timestamp = new Date(data.timestamp)
        if (this.mode === MODE.MEASUREMENT && data.id === this.currentMeasurementData.id) {
            this.handleSingleDataSet(data)
            MeasurementViewUi.registerNewTimestamp(data.timestamp, true)
            if (!MeasurementViewUi.isPlaybackActive()) {
                // only display the new CO2 values when no animation is running
                // otherwise, the new values will be displayed for a fraction of a second
                // and after that old data will be displayed
                MeasurementViewUi.displayAt(timestamp)
            }
            // TODO: idea for later: when looking at old values,
            //  wait for 30 seconds before displaying current values
            /*if (this.currentMeasurementData.timestamps.length < 2
                || this.currentlyDisplayedTimestamp === this.currentMeasurementData.timestamps[
                    this.currentMeasurementData.timestamps.length - 2]) {
                // Only update the view with the current data
                // if the previously latest timestamp was displayed.
                // The timestamp of this dataset has already been added to timestamps.
                // Therefore compare it to the timestamp at the second highest index.

            }*/
        }


    }

    /**
     *
     * @param {Measurement} measurement
     * @param {Boolean} isRunning
     */
    async onMeasurementReceived (measurement, isRunning) {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * When a completed or running measurement was received, following steps are required:
         * 1. Remove data which is associated with the previous displayed measurement
         * 2. Change the application mode to match the state (running or completed)
         *    of the measurement.
         * 3. Register the new measurement data and corresponding listeners and apply UI changes
         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        this.resetMeasurementData(false);

        this.isLoadingMeasurement = true;

        if (isRunning) {
            // TODO: check if data conversion is running when receiving a new singleDataSet
            //       and queue the data update if necessary
            this.setMode(MODE.MEASUREMENT)
        } else {
            this.setMode(MODE.VIEWER)
        }

        // REGISTER NEW DATA | APPLY THE MEASUREMENT DATA

        // set measurement data
        this.currentMeasurementData = measurement;
        this.currentMeasurementData.timestamps = [];

        // Prevent error when roomplan elements are not set.
        // They are going to be added after the room has been changed.
        if (measurement.room.elements == null) {
            measurement.room.elements = [];
        }
        // adjust UI to match the room info and load room plan if it exists
        RoomEditor.changeRoom(measurement.room, true, false);

        // restructure the measurement data

        // register sensorknots
        for (let measurementObj of measurement.objects) {
            const type = parseInt(measurementObj.type);
            switch (type) {
                case OBJECT_TYPE.SENSOR:
                    this.addSensor(measurementObj.id, measurementObj.mac, measurementObj.position,
                        undefined, measurementObj.LED, measurementObj.height,
                        measurementObj.note);
                    break;
                case OBJECT_TYPE.DOOR:
                case OBJECT_TYPE.WINDOW:
                case OBJECT_TYPE.AIR_FILTER:
                    createRoomplanElement(
                        MeasurementViewUi.roomplan, type, measurementObj.position,
                        measurementObj.id, measurementObj.data,
                        MeasurementViewUi.contextMenuHandler, controller);
                    break;
                default:
                    console.error(`Did not recognize measurement object type '${measurementObj.type}'`);
            }
        }

        // convert measurement data points and add them to the sensorHandler
        Object.keys(measurement.data).forEach(timestamp => {
            let dataForTimestamp = measurement.data[timestamp];
            let singleDataSet = {
                timestamp: parseInt(timestamp),
                sensorknots: dataForTimestamp
            }
            this.handleSingleDataSet(singleDataSet);
            MeasurementViewUi.registerNewTimestamp(parseInt(timestamp), false)
        });
        let firstMeasurementTime = this.currentMeasurementData.timestamps[0];
        if (firstMeasurementTime != null) {
            MeasurementViewUi.displayAt(new Date(firstMeasurementTime))
        }

        // register events
        for (const event of measurement.events) {
            let data = JSON.parse(event.data);
            if (event.type === EVENT.NOTE) {
                this.addEvent(new NoteEvent(data.text, event.timestamp));
            } else if (event.type === EVENT.ROOM_USAGE_CHANGED) {
                this.addEvent(new RoomUsageEvent(data.roomUsage, event.timestamp))
            } else {
                const el = findElement(event.objectId);
                el.setState(EventTypeToState(event.type));
                this.addEvent(new RoomplanElementEvent(
                    el,
                    event.type,
                    event.data,
                    event.timestamp));
            }
        }

        // show measurement name and date at website, date without function
        let timestamp = new Date(this.currentMeasurementData.timestamp);
        MeasurementViewUi.showMeasurementMetaData(this.currentMeasurementData.name, timestamp);
        MeasurementViewUi.initAverageCO2ConcentrationCalculator();

        // chart can only be created when the sensors and their data is available.
        MeasurementViewUi.checkChart();

        // delete the data property because the data was restructured
        // and is now stored in the sensor handler
        delete this.currentMeasurementData.data;

        this.isLoadingMeasurement = false;
    }

    resetMeasurementData(stop = true) {
        if (this.mode === MODE.MEASUREMENT && stop) {
            this.stopMeasurement();
        }

        this.sensorHandler.reset(this.wasMeasurementRecorded);
        this.eventHandler.reset();
        this.currentMeasurementData = {
            room : {
                length: 0,
                width: 0
            },
            name: "",
            id: -1,
            timestamps : []
        }

        MeasurementViewUi.reset(this.wasMeasurementRecorded);
        this.isLoadingMeasurement = false;
        this.PM25ValueInterpreter = new PMValueInterpreter();
        this.PM10ValueInterpreter = new PMValueInterpreter();
    }

    /**
     * Handle a message sent by the server that is NOT a response to a previously sent request.
     * @param {Object} data
     * @return {boolean}
     */
    handleServerMessage(data) {
        if (data.event === "register-client") {
            // data sent to the frontend once the frontend connects to the backend

            // add rooms to room list
            if (data.hasOwnProperty("rooms")) {
                controller.onRoomsReceived.call(controller, data.rooms)
            } else {
                console.error("Did not receive room list on load");
                // should not happen
            }

            // check server state and go into the corresponding application mode
            if (data.status === "blocked") {
                // another frontend instance is already running, thus this one is blocked
                controller.setMode.call(controller, MODE.BLOCKED)
            } else if (data.status === "measurement") {
                // a measurement is currently running and the web application was reloaded
                controller.onMeasurementReceived.call(controller, data["measurement"], true)
            } else { // "no-measurement"
                // backend waits for configuration info,
                // frontend receives a list of logged in sensors
                controller.onLoggedInSensorListReceived.call(controller, data.sensorknots)
            }
        } else if (data.event === "login" || data.event === "logout") {
            controller.onSensorWifiEventReceived.call(controller, data);
        } else if (data.event === "measurementCycleData") {
            controller.onSingleDataSetReceived.call(controller, data);
        } else if (data.event === "batteryStatus") {
            controller.onBatteryStatusReceived.call(controller, data.sensorknots);
        } else {
            return false;
        }
        return true;
    }

    updateRoomImage() {}
}
