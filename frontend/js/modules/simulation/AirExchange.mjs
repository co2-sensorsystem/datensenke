/**
 * The air flow ratio depending on the opening type and the opening width.
 * @type {{tilted: {"0": number, "1": number, cm: {"12": number, "2": number, "14": number, "4": number, "6": number, "8": number, "10": number}}, turned: {"0": number, deg: {"45": number, "90": number}, cm: {"15": number, "5": number, "10": number}}}}
 */
export const FLOW_RATIO = {
    tilted: {
        "cm": {
            0: 0,
            2: 0.0715,
            4: 0.0943,
            6: 0.1204,
            8: 0.1426,
            10: 0.1752,
            12: 0.2036,
            14: 0.2172,
        },
        0: 0,
        1: 1
    },
    turned: {
        "cm": {
            0: 0,
            5: 0.19148,
            10: 0.2890,
            15: 0.3850,
        },
        "deg": {
            0: 0,
            45: 0.8208,
            90: 1
        },
        0: 0,
    }
}

/**
 * Temperature delta in <code>K</code> for each season.
 */
export const TEMPERATURE_DELTA = {
    SPRING: 2,
    SUMMER: 5,
    AUTUMN: 2,
    WINTER: 15
}

/**
 * Wind speed in <code>m/s</code> for each season.
 */
export const WIND_SPEED = {
    SPRING: 5,
    SUMMER: 3,
    AUTUMN: 5,
    WINTER: 3
}

/**
 * The default air exchange rate of a room with closed doors and goof isolated windows.
 * Unit: <code>1/h</code>
 * @type {number}
 */
export const DEFAULT_AIR_EXCHANGE_RATE = 0.3;

export class AirProperties {
    /**
     * @type {number}
     */
    _indoorTemperature;
    /**
     * @type {number}
     */
    _outdoorTemperature;
    /**
     * @type {number}
     */
    _indoorCO2;
    /**
     * @type {number}
     */
    _outdoorCO2;
    /**
     * @type {number}
     */
    _temperatureDelta;
    /**
     * @type {number}
     */
    _windSpeed;

    /**
     * @param {number} windSpeed
     * @param {number} indoorCO2
     * @param {number} outdoorCO2
     * @param {number} [indoorTemperature]
     * @param {number} [outdoorTemperature]
     * @param {number} [temperatureDelta]
     */
    constructor(windSpeed, indoorCO2, outdoorCO2,
                indoorTemperature = undefined, outdoorTemperature = undefined, temperatureDelta = undefined) {
        if (indoorTemperature !== undefined && outdoorTemperature !== undefined) {
            this._temperatureDelta = Math.abs(indoorTemperature - outdoorTemperature);
        }
        if (temperatureDelta !== undefined) {
            this._temperatureDelta = temperatureDelta;
        }
        this._windSpeed = windSpeed;
        this._indoorCO2 = indoorCO2;
        this._outdoorCO2 = outdoorCO2;
        this._indoorTemperature = indoorTemperature;
        this._outdoorTemperature = outdoorTemperature;
        this._temperatureDelta = temperatureDelta;
    }


    get windSpeed() {
        return this._windSpeed;
    }

    get indoorCO2() {
        return this._indoorCO2;
    }

    get outdoorCO2() {
        return this._outdoorCO2;
    }

    get indoorTemperature() {
        return this._indoorTemperature;
    }

    get outdoorTemperature() {
        return this._outdoorTemperature;
    }

    get temperatureDelta() {
        return this._temperatureDelta;
    }
}

export class Opening {
    height;
    width;
    /**
     * <code>{@link height} * {@link width}</code>
     */
    area;
    /**
     * The angle in which an opening is opened when it is turned in parallel to the wall
     * <pre>
     * -------==----------------|
     * |        --__            |
     * |            --          |
     * |             |
     * |             |
     * |         O   |
     * |             |
     * |             |
     * |__           |
     *      --       |
     *          __   |
     *             --|
     *
     * </pre>
     * @type {string}
     */
    turnedOpening;
    /**
     * The width of the opening when the opening is tilted.
     * @type {string}
     */
    tiltedOpening;
    /**
     * Whether the opening is tilted or turned.
     * @type boolean
     */
    isTilted;
    /**
     * Whether the opening is opened at all,
     * @type boolean
     */
    isClosed;

    constructor(height, width, turnedOpening, tiltedOpening, isTilted = false, isClosed = false) {
        this.height = height;
        this.width = width;
        this.area = height * width;
        this.turnedOpening = turnedOpening;
        this.tiltedOpening = tiltedOpening;
        this.isTilted = isTilted;
        this.isClosed = isClosed;
    }

    /**
     * @param obj
     * @return {Opening}
     */
    static deserialize(obj) {
        return new Opening(obj.height, obj.width, obj.turnedOpening, obj.tiltedOpening, obj.isTilted, obj.isClosed);
    }

    /**
     * <p>Get the air's flow ration through an opening.</p>
     * The volume of air which flows through an opening is dependent on how much
     * the opening is opened (or how much the air is blocked by the remaining parts of the opening).
     * @param [tilted] Whether the flow rate should be passed for opening's <code>tilted</code> state
     * or the <code>turned</code> state.
     * @return number
     */
    getAirFlowRatio(tilted = undefined) {
        let returnTilted = (tilted === undefined ? this.isTilted : tilted);

        /**
         * Get the value between two points using linear approximation.
         * @param obj
         * @param {number | float} key
         * @param {number} upper
         * @return number
         */
        function getNearestValues(obj, key, upper) {
            let v = {lower: 0, upper: upper};
            for (const objKey in obj) {
                if (objKey < key) {
                    v.lower = objKey;
                } else if (objKey >= key) {
                    v.upper = objKey;
                    break;
                }
            }
            const frac = (key - v.lower) / (v.upper - v.lower);
            return obj[v.lower] + (obj[v.upper] - obj[v.lower]) * frac;
        }

        function getApproxFlowRatio(ob, val) {
            if (ob.hasOwnProperty(val)) {
                return ob[val];
            }
            const num = parseFloat(val.match(/\d+/));
            if (val.includes('deg')) {
                if (num >= 90) return 1;
                if (ob.hasOwnProperty(num)) {
                    return ob[val];
                }
                return getNearestValues(ob['deg'], num, 90);
            } else {
                const upper = ob['cm'].hasOwnProperty(14) ? 14 : 15;
                if (num >= upper) return 1;
                return getNearestValues(ob['cm'], num, upper);
            }
        }
        if (returnTilted) {
            return getApproxFlowRatio(FLOW_RATIO.tilted, this.tiltedOpening);
        }
        return getApproxFlowRatio(FLOW_RATIO.turned, this.turnedOpening);
    }
}

/**
 * Calculate the air change rate (in <code>m³/s</code>) for one window or door.
 * The air change must take place one-sided,
 * e.g. there must not be an opening on the opposite site of this opening.
 * @param {Opening} opening
 * @param {AirProperties} airSetting
 * @return rate in <code>m³/s</code>
 */
export function getOneSidedAirFlowVolume(opening, airSetting) {
    if (opening.isClosed) {
        return 0;
    }
    const C1 = 0.0056;
    const C2 = 0.0037;
    const C3 = 0.012;
    const a = 0.5 * opening.area * opening.getAirFlowRatio();
    const b = C1 * airSetting.windSpeed * airSetting.windSpeed;
    const c = C2 * opening.height * airSetting.temperatureDelta;
    return a * Math.sqrt(b + c + C3);
}

/**
 * The air exchange between two openings on opposite sides of a room in <code>m³/s</code>.
 * @param o1 {Opening}
 * @param o2 {Opening}
 * @param as {AirProperties}
 * @return rate in <code>m³/s</code>
 */
export function getTwoSidedAirFlowVolume(o1, o2, as) {
    const C1 = 0.01965;
    const C2 = 0.001896; // [m/(s²*K)]
    const C3 = 0.01706; // [m²/s²]
    const C4 = 0.01946;

    const u2 = as.windSpeed * as.windSpeed;
    const H = (o1.height + o2.height) / 2;

    return 0.5 * (o1.getAirFlowRatio() * o1.area + o2.getAirFlowRatio() * o2.area)
        * Math.sqrt(C1 * u2 + C2 *  H * as.temperatureDelta + C3)
        + (Math.sqrt(C4 * u2)
            / (Math.sqrt(Math.pow(1/ (o1.getAirFlowRatio() * o1.area), 2)
                + Math.pow(1/ (o2.getAirFlowRatio() * o2.area), 2)) ));
}


export class RoomProperties {
    /**
     * @type AirProperties
     */
    airProperties;
    length;
    width;
    height;
    volume;
    /**
     * @type Opening[]
     */
    openings = [];

    /**
     * @param {Room} room
     * @param {AirProperties} airProperties
     * @param {Opening[]} openings
     */
    constructor(room, airProperties, openings) {
        this.length = room.length;
        this.width = room.width;
        this.height = room.height;
        this.volume = room.volume;
        this.openings = openings;
        this.airProperties = airProperties;
    }

    /**
     * Air flow rate int <code>m³/s</code>
     * @return number
     */
    getAirFlowRate() {
        let rate = 0;
        for (const opening of this.openings) {
            rate += getOneSidedAirFlowVolume(opening, this.airProperties);
        }

        console.log(rate)
        return rate;
    }

    /**
     * The air exchange rate in <code>1/h</code>
     * @return {number}
     */
    getAirExchangeRate() {
        let a = (this.getAirFlowRate() * 3600) / this.volume;
        if (a == 0) {
            return DEFAULT_AIR_EXCHANGE_RATE;
        }
        return a;
    }

    /**
     * The duration for replacing approximately the whole air in the room once in seconds.
     * @return {number} time in seconds
     */
    getDurationForOneAirChange() {
        const airExchangeRate = this.getAirExchangeRate();
        if (airExchangeRate === 0) {
            return Number.MAX_VALUE;
        }
        return 1 / airExchangeRate * 3600;
    }

}