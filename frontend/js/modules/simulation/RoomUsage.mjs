import {RoomUsageEvent} from "../EventHandler.mjs";
import {createElementFromHTML} from "../utils.mjs";

let small = false;
let onChange;

export const AGE_GROUP = {
    KINDERGARTEN: 1,
    PRIMARSTUFE: 2,
    SEKUNDARSTUFE: 3,
    ADULT: 5
};

export const ACTIVITY = {
    LOW: 1,
    MEDIUM: 3,
    HIGH: 5
};

/**
 * @param {boolean} _small
 * @param {function(RoomUsageGroup[])} _onChange
 */
export function init(_small = false, _onChange = undefined) {
    small = _small;
    onChange = _onChange;
}

/**
 * add a new input field for a new room usage group on the UI
 * @param {HTMLDivElement} target
 */
export function onAddUserGroup(target) {
    let userGroupElements = document.getElementsByClassName("room-usage-group");
    let userGroupCount = 1;

    // get the next free number for the designation of the id for the new input field
    if (userGroupElements.length > 0) {
        for (let i = 0; document.getElementById("room-usage-group-" + userGroupCount) != null; i++) {
            userGroupCount++;
        }
    }

    const userGroup = createElementFromHTML( `
            <div class="${small ? 'mb-4' : 'mb-3'} room-usage-group" id="room-usage-group-${userGroupCount}">
                <div class="row ${small ? 'd-none' : ''}">
                    <div class="col text-muted room-usage-group-title">
                        Nutzergruppe ${userGroupCount}
                    </div>
                    <button type="button" class="btn col-auto" id="remove-room-usage-group-entry-${userGroupCount}">
                        <i class="bi-trash text-danger"></i>
                    </button>
                </div>
                <div class="row  ${small ? '' : 'mb-3'}">
                    <label for="person-count-${userGroupCount}" class="col-4 col-form-label text-dark">Personenanzahl</label>
                    <div class="col">
                        <input type="number" class="form-control ${small ? 'form-control-sm' : ''}" id="person-count-${userGroupCount}" min="1" step="1" value="1" required>
                    </div>
                </div>
                <div class="row ${small ? '' : 'mb-3'}" data-api-action-before="delete-age-group">
                    <label class="col-4 col-form-label text-dark" for="age-level-${userGroupCount}">Altersgruppe</label>
                    <div class="col">
                        <select class="col form-select ${small ? 'form-select-sm' : ''} form-control" id="age-level-${userGroupCount}">
                            <!--<option value="${AGE_GROUP.KINDERGARTEN}">Kindergarten</option>-->
                            <option value="${AGE_GROUP.PRIMARSTUFE}">Primarstufe (Jahrgang 1-4)</option>
                            <option value="${AGE_GROUP.SEKUNDARSTUFE}">Sekundarstufe (Jahrgang 5-13)</option>
                            <option value="${AGE_GROUP.ADULT}" selected>Erwachsene Personen</option>
                        </select>
                    </div>
                </div>
                <div class="row  ${small ? '' : 'mb-3'}">
                    <label class="col-4 col-form-label text-dark" for="activity-${userGroupCount}">Aktivität</label>
                    <div class="col">
                        <select class="form-select ${small ? 'form-select-sm' : ''} form-control w-100" id="activity-${userGroupCount}">
                            <option value="${ACTIVITY.LOW}" selected>gering</option>
                            <option value="${ACTIVITY.MEDIUM}">mittel</option>
                            <option value="${ACTIVITY.HIGH}">schwer</option>
                        </select>
                    </div>
                </div>
            </div>
        `);
    target.appendChild(userGroup);

    document.getElementById("remove-room-usage-group-entry-" + userGroupCount).onclick = function () {
        onRemoveRoomUsageGroup(userGroupCount);
    }
    let selector = `#room-usage-group-${userGroupCount} [data-api-action-before="delete-age-group"]`;
    const deleteButton = target.querySelector(selector);
    if (deleteButton) {
        deleteButton.addEventListener('click', (event) => {
            event.stopPropagation();
            // Only delete the group when the ::before pseudo-element was clicked.
            // It is placed on the left side of the div causing it to have an offsetX < 0.
            if (event.offsetX < 0 && event.target === deleteButton) {
                onRemoveRoomUsageGroup(userGroupCount)
            }

        })
    }

    selector = `#room-usage-group-${userGroupCount} select, #room-usage-group-${userGroupCount}  input`;
    target.querySelectorAll(selector).forEach((i) => {
        i.addEventListener('change', () => {
            callOnChange();
        })
    })

    checkRemoveButton(target);
    callOnChange();
}

/**
 *
 * @param {int} groupNumber
 */
export function onRemoveRoomUsageGroup(groupNumber) {
    const element = document.getElementById("room-usage-group-" + groupNumber);
    const parent = element.parentElement;
    element.parentNode.removeChild(element);
    checkRemoveButton(parent);
    callOnChange();
}

/**
 *
 * @param {HTMLElement} group
 */
function checkRemoveButton(group) {
    if (group.children.length === 1) {
        group.classList.add('no-remove');
    } else {
        group.classList.remove('no-remove');
    }
}


/**
 * for easy and fast input of no persons in room
 * (only during measurement)
 * @param {MeasurementViewController} controller
 */
export function onNoRoomUsageGroup(controller) {
    // TODO: delete by click the room usage groups at UI?
    const roomUsageEvent = new RoomUsageEvent([]);
    controller.sendEvent(roomUsageEvent, null);
}
/**
 * Listener called when creating a new {@link RoomUsageEvent} via the UI
 * @param {SubmitEvent} event
 * @param {MeasurementViewController} controller
 */
export function onCreateNewRoomUsageEvent(event, controller) {
    event.preventDefault();

    createNewRoomUsageEvent(controller);

    return false; // do not submit the form; we do not want to reload the page
}

export class RoomUsageGroup {
    /**
     * @type {int}
     */
    ageLevel;
    /**
     * @type{int}
     */
    activity;
    /**
     * @type{int}
     */
    personCount;

    constructor(ageLevel, activity, personCount) {
        this.ageLevel = ageLevel;
        this.activity = activity;
        this.personCount = personCount;
    }

    /**
     * Get the text content of this note.
     * @param html whether the content should be rendered in HTML or not
     * @return {string}
     */
    getText(html = false) {
        if (html) {
            return this.text.replace("\n", "<br/>");
        } else {
            return this.text;
        }
    }
}

/**
 * creating a new {@link RoomUsageEvent} via the UI.
 * @param {MeasurementViewController} controller
 */
export function createNewRoomUsageEvent(controller) {
    const roomUsageGroups = getRoomUsage();
    if (roomUsageGroups) {
        const roomUsageEvent = new RoomUsageEvent(roomUsageGroups);
        controller.sendEvent(roomUsageEvent, null);
    }
}

/**
 * Get the room usage which is currently put into the container.
 * @return {RoomUsageGroup[]}
 */
export function getRoomUsage() {
    let ageLevel;
    let activity;
    let personCount;

    /**
     * @type {RoomUsageGroup}
     */
    let roomUsageGroup;

    /**
     * @type {RoomUsageGroup[]}
     */
    let roomUsageGroups = [];

    /**
     * get all defined room usage groups and put them as {@link RoomUsageGroup}
     * in the array roomUsageGroups
     */
    let userGroupElements = document.getElementsByClassName("room-usage-group");
    let userGroupCount = 1;
    for (let i = 0; i < userGroupElements.length; i++) {
        if (document.getElementById("room-usage-group-" + userGroupCount) != null) {
            ageLevel = parseInt(document.getElementById("age-level-" + userGroupCount).value);
            activity = parseInt(document.getElementById("activity-" + userGroupCount).value);
            personCount = parseInt((document.getElementById("person-count-" + userGroupCount).value));
            if (ageLevel && activity && personCount) {
                roomUsageGroup = new RoomUsageGroup(ageLevel, activity, personCount);
                roomUsageGroups.push(roomUsageGroup);
            }
        } else {
            i--;
        }
        userGroupCount++;
    }
    return roomUsageGroups;
}

function callOnChange() {
    if (onChange != null) {
        onChange();
    }
}