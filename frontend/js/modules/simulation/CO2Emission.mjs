/**
 * CO₂ emissions by children in Sekundarstufe I (age XX - XX) depending on their activity.
 * @property {number} low Low activity while sitting or standing
 * @property {number} middle low Low activity while sitting or standing
 * @type {{high: number, middle: number, low: number}}
 */
import {ACTIVITY, AGE_GROUP} from "./RoomUsage.mjs";
import {DEFAULT_AIR_EXCHANGE_RATE} from "./AirExchange.mjs";

export const CHILD = {
    LOW: 15.6,
    MEDIUM: 19.4,
    HIGH: 33.6
};

export const TEENAGER = {
    LOW: 18.9,
    MEDIUM: 22.0,
    HIGH: 43.5
};

export const ADULT = {
    SITTING: 20,
    LOW: 40,
    MEDIUM: 70
};

/**
 * A
 * @param {RoomUsageGroup[]} roomUsageGroups
 */
export function getCO2Emission(roomUsageGroups) {
    let emission = 0;
    const mapping = {};
    mapping[ACTIVITY.LOW] = "LOW";
    mapping[ACTIVITY.MEDIUM] = "MEDIUM";
    mapping[ACTIVITY.HIGH] = "HIGH";
    for (const roomUsageGroup of roomUsageGroups) {
        const ageGroup = roomUsageGroup.ageLevel;
        if (ageGroup === AGE_GROUP.ADULT) {
            mapping[ACTIVITY.LOW] = "SITTING";
            mapping[ACTIVITY.MEDIUM] = "LOW";
            mapping[ACTIVITY.HIGH] = "MEDIUM";
            emission += roomUsageGroup.personCount * ADULT[mapping[roomUsageGroup.activity]];
        } else {
            mapping[ACTIVITY.LOW] = "LOW";
            mapping[ACTIVITY.MEDIUM] = "MEDIUM";
            mapping[ACTIVITY.HIGH] = "HIGH";

            if (ageGroup === AGE_GROUP.PRIMARSTUFE) {
                emission += roomUsageGroup.personCount * CHILD[mapping[roomUsageGroup.activity]]
            } else if (ageGroup === AGE_GROUP.SEKUNDARSTUFE) {
                emission += roomUsageGroup.personCount * TEENAGER[mapping[roomUsageGroup.activity]]
            }
        }
    }
    return emission;
}

export class CO2ConcentrationCalculator {
    /**
     * Starting point in minutes
     * @type {number}
     */
    _start;
    /**
     * CO₂ concentration of the ambient / outdoor air in ppm
     * @type {number} greater than zero; typically at values between 400 ppm and 450 ppm
     */
    _cA = 400;

    /**
     * CO₂ concentration at the beginning of the calculation
     * @type {number}
     */
    _c0;

    /**
     * <p>Air change number / rate in <code>1/h</code></p>
     * Default value is {@link DEFAULT_AIR_EXCHANGE_RATE}.
     * @type {number} greater than zero
     */
    _n = DEFAULT_AIR_EXCHANGE_RATE;

    /**
     * The room's air volume in <code>m³</code>
     * @type {number} greater than zero
     */
    _V;

    /**
     * The CO₂ emissions by the people in the room in <code>l/m³</code>.
     * @see getCO2Emission
     * @type {number}
     */
    _q;

    constructor(cA, cO, V, n, q, start = 0) {
        this._cA = cA;
        this._c0 = cO;
        this._V = V;
        this._n = n === 0 ? 0.000001 : n; // ensure that n > 0
        this._q = q;
        this._start = start;
    }

    /**
     * Calculate the CO₂ concentration at a certain point.
     * @param t {number} time in minutes
     * @returns {number} the CO₂ concentration in <code>ppm</code>
     */
    concentrationAfter(t) {
        // get relative time for this calculator and convert from minutes to hours
        //if (t > 116) debugger
        const timestamp = (t - this._start) / 60;
        const airChange = Math.pow(Math.E, -1 * this._n * timestamp);
        return this._cA + (this._c0 - this._cA) * airChange
            // * 10^6 to convert into ppm and * 10^-3 to convert l to m³ => factor 10^3
            + (this._q / (this._n * this._V)) * (1 - airChange) * 1000;
    }
    get cA() {
        return this._cA;
    }

    get cO() {
        return this._c0;
    }

    get V() {
        return this._V;
    }

    get n() {
        return this._n;
    }

    get q() {
        return this._q;
    }

    get start() {
        return this._start;
    }

    set start(start) {
        this._start = start;
    }
}


