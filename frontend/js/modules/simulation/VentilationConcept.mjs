import {AirProperties, Opening, RoomProperties} from "./AirExchange.mjs";
import {CO2ConcentrationCalculator} from "./CO2Emission.mjs";
import {CO2ValueInterpreter} from "../measurement/MeasurementValueInterpreter.mjs";
import {timeResolution} from "./simulationController.mjs"

export class VentilationConcept {
    /**
     * @type CO2ConcentrationCalculator[]
     * @private
     */
    _calculators;
    #currentTimestamp = 0;

    constructor(calculators) {
        this._calculators = calculators;
    }

    /**
     *
     * @param t the time in minutes
     * @return {number}
     */
    getCO2At(t) {
        t = t / timeResolution;
        let calculator;
        if (this.#currentTimestamp === this._calculators.length - 1
            || t < this._calculators[this.#currentTimestamp + 1].start) {
            calculator = this._calculators[this.#currentTimestamp];
        } else {
            calculator = this._calculators[++this.#currentTimestamp];
        }
        return calculator.concentrationAfter(t);
    }

    /**
     *
     * @param duration in minutes
     * @param startingAt starting point in minutes
     */
    getAverageCO2(duration, startingAt = 0) {
        this.reset();
        let sum = 0;
        for (let t = startingAt; t < duration + startingAt; t++) {
            sum += this.getCO2At(t);
        }
        this.reset();
        return sum / duration;
    }

    reset() {
        this.#currentTimestamp = 0;
    }

    get calculators() {
        return this._calculators;
    }
}

/**
 * Suggest a ventilation concept for the given Room and CO2 emission.
 * @param {int} duration in minutes - the duration of the ventilation concept
 * @param {RoomProperties} roomProperties the properties of the room for which the ventilation concept is generated
 * @param {number} co2emission - the CO2 emission in <code>ppm</code>;
 *                            the CO2 emission is considered to be constant for the given duration
 * @param {number} maxCo2 in ppm - maximum CO2 concentration allowed
 * @param {number} closedAirExchangeRate in <code>1/m³</code> - the air exchange rate when all openings are closed.
 * @param {number} airExchangeEfficiencyThreshold
 * @param {"single" | "target" | "mixed"} mode the mode deciding how the duration of a ventilation phase is calculated.
 * <ul>
 * <li><b><code>single</code></b>: the goal is to exchange the whole air of the room once;</li>
 * <li><b><code>target</code></b>: the <code>targetCO2AfterVentilation</code> threshold needs to be undercut
 * before the ventilation phase is stopped.</li>
 * <li><b><code>mixed</code></b>: the system checks whether the <code>targetCO2AfterVentilation</code> can be undercut in a reasonable time;
 * if this is true, the behaviour matches the <code>target</code> mode, otherwise a <code>single</code> air exchange is performed.</li>
 * </ul>
 * @param targetCO2AfterVentilation - the target CO₂ concentration after a full ventilation phase used in <code>target</code> and </code>mixed</code> mode.
 * @return VentilationConcept
 */
export function generateConcept(duration, roomProperties, co2emission, maxCo2,
                                closedAirExchangeRate, airExchangeEfficiencyThreshold, mode, targetCO2AfterVentilation = 600) {
    // Set up the first calculator starting with the default CO2 concentration, default indoor CO2 air exchange rate.
    // The first calculators starts at 0 seconds.
    let airProperties = roomProperties.airProperties;
    /**
     * Current time in minutes;
     * The ventilation concept is generated starting at minute <code>zero</code>.
     * @type {number}
     */
    let currentTime = 0;
    /**
     * List of used CO2ConcentrationCalculators
     * @type {CO2ConcentrationCalculator[]}
     */
    let calculators = [new CO2ConcentrationCalculator(
        airProperties.outdoorCO2,
        airProperties.indoorCO2,
        roomProperties.volume,
        closedAirExchangeRate,
        co2emission,
        currentTime)];
    /**
     * Index of the currently used CO2 calculator.
     * @type {number}
     */
    let currentCalcIndex = 0;
    /**
     * Duration remaining until the current air exchange is completed.
     * If no air exchange is active, this variable is equal to <code>-1</code>.
     * @type {number}
     */
    let remainingAirExchangeDuration = -1;
    let isAirExchangeActive = false;
    let lastCO2;
    while (currentTime < duration) {
        let currentCO2 = calculators[currentCalcIndex].concentrationAfter(currentTime);
        // There are basically two states in which the room can be:
        // The windows are either opened or closed.
        // The windows need to be opened once the threshold for ventilation is met.
        if (currentCO2 >= maxCo2 && !isAirExchangeActive && roomProperties.openings.length !== 0) {
            let oC = calculators[currentCalcIndex];
            const newCalculator = new CO2ConcentrationCalculator(
                oC.cA, lastCO2 || oC.c0, oC.V, roomProperties.getAirExchangeRate(), oC.q, currentTime - 1);
            calculators.push(newCalculator);
            isAirExchangeActive = true;
            currentCalcIndex++;
            if (mode === "single") {
                // seconds to minutes
                remainingAirExchangeDuration = Math.round(roomProperties.getDurationForOneAirChange() / 60);
            } else if (mode === "mixed") {
                remainingAirExchangeDuration = getBestDuration(
                    newCalculator, targetCO2AfterVentilation, airExchangeEfficiencyThreshold);
            }
        }
        if (isAirExchangeActive) {
            if ((mode === "single" || mode === "mixed") && remainingAirExchangeDuration === 0
                || (mode === "target" || mode === "mixed") && currentCO2 < targetCO2AfterVentilation) {
                let oC = calculators[currentCalcIndex - 1];
                const newCalculator = new CO2ConcentrationCalculator(
                    oC.cA, currentCO2, oC.V, oC.n, oC.q, currentTime);
                calculators.push(newCalculator);
                isAirExchangeActive = false;
                currentCalcIndex++;
            }
        }
        remainingAirExchangeDuration--;
        currentTime++;
        lastCO2 = currentCO2;
    }

    const concept = new VentilationConcept(calculators);
    if (mode === "mixed" && concept.getAverageCO2(duration) > 1000) {
        return generateConcept(duration, roomProperties, co2emission, 1100, closedAirExchangeRate, 25, "target", targetCO2AfterVentilation)
    }
    return concept;
}

/**
 *
 * @param {CO2ConcentrationCalculator} calculator
 * @param {number} targetConcentration
 * @param {number} airExchangeEfficiencyThreshold
 * @return {number} duration
 */
function getBestDuration(calculator, targetConcentration, airExchangeEfficiencyThreshold) {
    const s = calculator.start;
    calculator.start = 0;
    let t = 0;
    const initialChange = calculator.concentrationAfter(0) - calculator.concentrationAfter(1);
    console.log("initialchange",initialChange)
    let lastCO2;
    let currentCO2 = calculator.concentrationAfter(0);
    do {
        t++;
        lastCO2 = currentCO2;
        currentCO2 = calculator.concentrationAfter(t);
    } while (currentCO2 > targetConcentration && /*t < maxDuration &&*/ lastCO2 - currentCO2 > airExchangeEfficiencyThreshold);
    calculator.start = s;
    return t;
}

/**
 *
 * @param {number} duration duration in seconds
 * @param {RoomProperties} roomProperties
 * @param {number} emission
 * @return {VentilationConcept}
 */
export function getCustomConcept(duration, roomProperties, emission) {
    const openings = [
        new Opening(1.15, 0.75, "90deg", "1", false),
        new Opening(1.15, 0.75, "90deg", "1", false),
        new Opening(1.15, 0.75, "90deg", "1", false),
        new Opening(1.15, 0.75, "90deg", "1", false),
        new Opening(1.15, 0.75, "90deg", "1", false),
    ];
    const airSetting = new AirProperties(3, 450, 400);
    airSetting.temperatureDelta = 15;
    const roomSetting = new RoomProperties({}, airSetting, openings);
    roomSetting.volume = 277;
    let calculators = [new CO2ConcentrationCalculator(400, 600, roomSetting.volume, 0.4, emission)],
        timestamps = [0];
    /**
     * Time in minutes
     * @type {number}
     */
    let t = 0;
    let c = 0;
    /**
     * Duration remaining until the current air exchange is completed,
     * @type {number}
     */
    let durationForAirExchange = -1;
    let isAirExchangeActive = false;
    while (t < duration) {
        let currentCO2 = calculators[c].concentrationAfter(t);
        if (currentCO2 >= CO2ValueInterpreter.THRESHOLD_YELLOW && !isAirExchangeActive) {
            let oC = calculators[c];
            const newCalculator = new CO2ConcentrationCalculator(
                oC.cA, currentCO2, oC.V, roomSetting.getAirExchangeRate(), oC.q, t);
            calculators.push(newCalculator);
            isAirExchangeActive = true;
            durationForAirExchange = Math.ceil(roomSetting.getDurationForOneAirChange() / 60);
            c++;
            timestamps.push(t);
        } else if (isAirExchangeActive && durationForAirExchange === 0) {
            isAirExchangeActive = false;
            let oC = calculators[c - 1];
            const newCalculator = new CO2ConcentrationCalculator(oC.cA, currentCO2, oC.V, oC.n, oC.q, t);
            calculators.push(newCalculator);
            c++;
            timestamps.push(t);
        }
        durationForAirExchange--;
        t++;
    }

    return new VentilationConcept(calculators)
}

