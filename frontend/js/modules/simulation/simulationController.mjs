// Initialize the CommunicationHandler before importing other modules,
// because those might need to work with the CommunicationHandler.
import * as CommunicationHandler from "../CommunicationHandler.mjs";
import * as ChartHelper from '../ChartHandler.mjs'
import {CHART_TYPE, DATA_TYPE} from '../ChartHandler.mjs'
import * as CO2Emission from './CO2Emission.mjs';
import * as VentilationConcept from "./VentilationConcept.mjs";
import {
    AirProperties,
    RoomProperties,
    TEMPERATURE_DELTA
} from "./AirExchange.mjs";
import * as SelectRoomDialog from "../ui/modals/SelectRoomModal.mjs";
import * as RoomUsage from "./RoomUsage.mjs";
import * as RoomEditor from "../room/roomEditor.mjs";
import {ContextMenuHandler} from "../room/ContextMenu.js";
import {MODE} from "../../../config.js";
import * as ImprintModal from "../ui/modals/imprintModal.mjs";
import {OpeningRoomplanElement} from "../room/RoomplanElement.mjs";

export const timeResolution = 1;

/**
 * @type {DiagramHolder}
 */
let chart;
/**
 * @type HTMLCanvasElement
 */
let canvas;
/**
 * Currently displayed ventilation concept.
 * @type VentilationConcept.VentilationConcept
 */
let concept;

/**
 * @type {Room}
 */
let room = {width:0, length: 0, height: 0, volume: 0, name: "", id: -1, image: null, elements: []};

let season = "SPRING";
let mode = "mixed";
let contextMenuHandler;

/**
 * Initialize the module once the page was loaded (document.onload triggered)
 */
export function init() {
    // Initialize the CommunicationHandler before importing other modules,
    // because those might need to work with the CommunicationHandler.
    // It has a queue for outgoing requests, but there is no reason to delay sending them.
    const controller = {
        mode: MODE.CONFIGURATION,
        handleServerMessage: handleServerMessage,
        updateRoomImage: updateRoomImage
    };
    CommunicationHandler.init(controller);

    // Initialize the ChartHandler Object, because the Chart might be updated
    // due to changes done by the other Modules.
    canvas = document.getElementById('prognose');
    //chart = ChartHelper.create(canvas, [], DATA_TYPE.CO2, CHART_TYPE.LINE, 90);

    contextMenuHandler = new ContextMenuHandler(null);

    // Initialize the dialog to select a room and the listeners to open it.
    // This module will get a list with all rooms and set an initial room for this calculator
    SelectRoomDialog.init((rooms) => {changeRoom(rooms[0])});
    document.querySelectorAll('[data-api-action="change-room"]').forEach((d) => {
        d.addEventListener('click', () => {
            SelectRoomDialog.openSelectRoomDialog((_room) => changeRoom(_room), () => {});
        })
    })

    // Initialize the Room editor and the listeners to open it.
    RoomEditor.init(controller, contextMenuHandler,true);
    RoomEditor.setOnSave(changeRoom)
    document.querySelectorAll('[data-api-action="edit-room"]').forEach((d) => {
        d.addEventListener('click', () => {RoomEditor.openModal(room, false)});
    })
    document.querySelectorAll('[data-api-action="create-room"]').forEach((d) => {
        d.addEventListener('click', () => {RoomEditor.openModal(null)});
    })

    // Initialize the module taking care of the room usage and the corresponding listeners.
    RoomUsage.init(true, (newUsage) => generateVentilationConcept());
    const roomUsageGroupContainer = document.getElementById('room-usage-group-container');
    RoomUsage.onAddUserGroup(roomUsageGroupContainer);
    document.querySelectorAll('[data-api-action="add-usage-group"]').forEach(
        (d) => d.addEventListener("click",
            () => RoomUsage.onAddUserGroup(roomUsageGroupContainer)));

    // Change the season which results in different room outdoor temperatures;
    // When the user used the calculator before, the last season is automatically chosen
    // by the browser, so let's use that:
    const seasonInput = document.getElementById('input-season');
    season = seasonInput.value;
    seasonInput.onchange = function () {
        season = this.value;
        generateVentilationConcept()
    }
    document.getElementById('input-target-concentration').addEventListener('change',
        () => generateVentilationConcept());
    document.getElementById('input-duration').addEventListener('change',
        () => generateVentilationConcept());
    document.getElementById('input-concentration-start').addEventListener('change',
        () => generateVentilationConcept());
    document.getElementById('input-concentration-max').addEventListener('change',
        () => generateVentilationConcept());
    document.getElementById('input-air-exchange-rate').addEventListener('change',
        () => generateVentilationConcept());
    document.getElementById('input-air-exchange-efficiency').addEventListener('change',
        () => generateVentilationConcept());

    // Button to manually trigger the calculation of new ventilation concept
    document.querySelectorAll('[data-api-action="calc-concept-suggest"]').forEach(
        (d) => d.addEventListener("click",
            () => generateVentilationConcept("mixed")));
    document.querySelectorAll('[data-api-action="calc-concept-target"]').forEach(
        (d) => d.addEventListener("click",
            () => generateVentilationConcept("target")));
    document.querySelectorAll('[data-api-action="calc-concept-single"]').forEach(
        (d) => d.addEventListener("click",
            () => generateVentilationConcept("single")));

    ImprintModal.init();
}

/**
 * @param {Room} _room
 */
function changeRoom(_room) {
    room = _room;
    console.log("Changing room. New room: ", room);
    updateApiField('room-property-name', room.name, "unbekannt");
    updateApiField('room-property-width', room.width , "unbekannt", ' m');
    updateApiField('room-property-length', room.length, "unbekannt", ' m');
    updateApiField('room-property-height', room.height , "unbekannt", ' m');
    updateApiField('room-property-volume', room.volume, "unbekannt", ' m³');
    if (room.elements.length !== 0
        && room.elements.filter(e => e instanceof OpeningRoomplanElement).length === 0) {
        RoomEditor.generateRoomplanElements(room);
    }
    if (chart != null) {
        generateVentilationConcept()
    }
}

function updateApiField(name, content, defaultValue = '', suffix = '') {
    if (content == null || content.length === 0) {
        content = defaultValue;
    } else {
        content += suffix;
    }
    const fields = document.querySelectorAll(`[data-api-content="${name}"]`);
    for (const field of fields) {
        field.innerHTML = content;
    }
}

function generateVentilationConcept(_mode) {
    if (_mode != null) {
        mode = _mode;
    }
    // First gather all data needed to generate the new concept and its graph
    const duration = parseInt(document.getElementById('input-duration').value);
    const roomUsages = RoomUsage.getRoomUsage();
    const emission = CO2Emission.getCO2Emission(roomUsages);
    const openings = room.elements.filter(e => e instanceof OpeningRoomplanElement).map(e => e.opening)
    const indoorCO2 = parseInt(document.getElementById('input-concentration-start').value);
    const maxCo2 = parseInt(document.getElementById('input-concentration-max').value);
    const airExchangeEfficiency = parseFloat(document.getElementById('input-air-exchange-efficiency').value);
    const closedAirExchangeRate = parseFloat(document.getElementById('input-air-exchange-rate').value);
    const airProperties = new AirProperties(3, indoorCO2, 450,
        undefined, undefined, TEMPERATURE_DELTA[season]);

    const roomProperties = new RoomProperties(room, airProperties, openings);

    // Generate the new concept
    if (mode === "single") {
        concept = VentilationConcept.generateConcept(
            duration, roomProperties, emission, maxCo2,
            closedAirExchangeRate, airExchangeEfficiency, "single");
    } else if (mode === "target" || mode === "mixed") {
        const targetConcentration = parseInt(
            document.getElementById('input-target-concentration').value);
        concept = VentilationConcept.generateConcept(
            duration, roomProperties, emission, maxCo2,
            closedAirExchangeRate, airExchangeEfficiency, mode, targetConcentration);
    } else {
        concept = VentilationConcept.getCustomConcept(duration, roomProperties, emission);
    }

    // generate the sidebar information about the concept's data
    const airChangeTimestamps = document.querySelectorAll('[data-api-input="calc-concept-air-change-timestamps"]');
    let content = "";
    for (let t in concept.calculators) {
        t = parseInt(t);
        const currentCalculator = concept.calculators[t];
        let phaseDuration;
        try {
            phaseDuration = concept.calculators[t + 1].start - currentCalculator.start;
        } catch (e) {
            // the ventilation phase exceeds the period which is calculated.
            phaseDuration = duration - currentCalculator.start;
        }
        if (currentCalculator.concentrationAfter(0) < currentCalculator.concentrationAfter(1)) {
            content += `<div class="col-12">
                   Für ${phaseDuration} Minuten nicht lüften.
                </div>`;
        } else {
            content += `<div class="col-12">
                   Ab Minute ${concept.calculators[t].start} für ${phaseDuration} Minuten lüften.
                </div>`;
        }
    }
    airChangeTimestamps.forEach((d) => {
        d.innerHTML = content;
    })

    document.querySelectorAll('[data-api-content="average-co2"]')
        .forEach(t => t.innerHTML = (concept.getAverageCO2(duration, 0) << 0) + ' ppm' );


    // Clean up the old graph and create a new one
    if (chart != null) {
        chart.destroy();
    }
    chart = ChartHelper.createDiagramFromVentilationConcept(canvas, concept, duration);
}

/**
 * @type handleServerMessageFunction
 */
export function handleServerMessage(data) {
    return true;
}

export function updateRoomImage(_room) {
    if (room.id === _room.id) {
        room.image = _room.image;
        RoomEditor.changeRoom(room, false);
    }
}