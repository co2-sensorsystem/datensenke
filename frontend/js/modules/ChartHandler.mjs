import {CO2ValueInterpreter} from "./measurement/MeasurementValueInterpreter.mjs";
import {VentilationConcept} from "./simulation/VentilationConcept.mjs";
import {NO_DATA} from "../../config.js";
import {hslToRgb, selectToggle} from "./utils.mjs";

/**
 * The type of data to be displayed by the {@link DiagramHolder}.
 * The chart uses different display settings for each data type.
 * @type {{PM2_5: string, HUMIDITY: string, TEMPERATURE: string, CO2: string, PM10: string}}
 */
export const DATA_TYPE = {
    CO2: "co2",
    TEMPERATURE: "temperature",
    HUMIDITY: "humidity",
    PM2_5: "pm25",
    PM10: "pm10"
}

/**
 * The chart type is a display settings which changes the chart's appearance.
 * The <code>LINE</code> chart type results in data points which are connected via lines.
 * @type {{LINE: string, POINT: string}}
 */
export const CHART_TYPE = {
    LINE: "line",
    POINT: "point"
}

const scales = {
    co2: {
        suggestedMin: 300,
        max: 1800,
        suggestedMax: 1500
    },
    pm25: {
        min: 0,
        suggestedMax: 5
    },
    pm10: {
        min: 0,
        suggestedMax: 20
    },
    temperature: {
        suggestedMin: 0,
        suggestedMax: 20
    },
    humidity: {
        suggestedMin: 10,
        suggestedMax: 80
    }
}

const noTransition  = {
    animation: {
        duration: 0
    }
};

const backgroundColors = {
    green: {r: 219, g: 240, b: 218},
    yellow: {r: 240, g: 238, b: 218},
    red: {r: 240, g: 218, b: 218}
};

const categorizedBackgroundPlugin = {
    id: 'categorizedBackgroundPlugin',
    beforeDraw(chart, args, options) {
        const {ctx, chartArea: {left, top, right, bottom}, scales: {x, y}} = chart;
        const width =  chart.chartArea.width + 10;
        const yYellow = y.getPixelForValue(CO2ValueInterpreter.THRESHOLD_YELLOW);
        const yGreen = y.getPixelForValue(CO2ValueInterpreter.THRESHOLD_GREEN);
        ctx.save();
        ctx.fillStyle = 'rgb(240, 218, 218)';
        ctx.fillRect(left - 5, top - 5, width, yYellow - top + 5);
        ctx.fillStyle = 'rgb(240, 238, 218)';
        ctx.fillRect(left - 5, yYellow, width, yGreen - yYellow);
        ctx.fillStyle = 'rgb(219, 240, 218)';
        ctx.fillRect(left - 5, yGreen, width, bottom - yGreen + 5);
        ctx.restore();
    }
};

const drawVentilationConceptPlugin = {
    id: 'drawVentilationConceptPlugin',
    beforeInit: function(chart) {
        const data = chart.config.data;
        for (const dataset of data.datasets) {
            const concept = dataset.ventilationConcept;
            if (concept) {
                for (const timestamp of data.labels) {
                    dataset.data.push(concept.getCO2At(parseInt(timestamp)));
                }
            }
        }
    }
}

const defaultConf = {
    type: 'line',
    data:  {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
        }]
    },
    options: {
        datasets: {},
        borderWidth: 0,
        scales: {
            y: scales.co2
        },
        responsive: true,
        maintainAspectRation: false,
        transitions: {
            active: noTransition,
            show: noTransition,
            hide: noTransition
        },
        animation: false,

    },
    plugins: []
};

/**
 * Handler for the graph showing the measurement data.
 * <p>
 * The graph is created by using <a href="https://charjs.com">ChartJS</a>.
 * </p>
 */
class DiagramHolder {
    /**
     * The corresponding Chart.js object.
     * @type {null | an}
     */
    #chart = null;
    /**
     * The canvas on which the chart is drawn
     * @type {HTMLCanvasElement}
     */
    #canvas = null;

    #chartType;
    #dataType;
    #displayAverage = false;
    #data;
    #onHoverCallback;
    #disabledDatasets = new Set();

    #sensorNodesWithoutValidData = [];

    /**
     * @param canvas {HTMLCanvasElement}
     * @param sensorData {Sensor[] | VentilationConcept}
     * @param dataType {string} See {@link DATA_TYPE}
     * @param chartType {string} See {@link CHART_TYPE}
     * @param [duration] {false | int} duration in minutes or <code>false</code> to ignore
     * @param [onHoverCallback] {function}
     */
    constructor(canvas, sensorData, dataType, chartType, duration = false) {
        this.#canvas = canvas;
        this.#dataType = dataType;
        this.#chartType = chartType;
        this.duration = duration;
        this.#data = sensorData;
        this.#chart = this.#createNewDiagram(sensorData, duration)
    }

    /**
     * @param data {Object}
     * @param data.timestamp {int}
     * @param data.sensorknots {Array}
     */
    addSingleDataset (data) {

        let count = 0, sum = 0;
        if (this.#displayAverage) {
            for (let i in data.sensorknots) {
                const datapoint = data.sensorknots[i][this.#dataType];
                if (datapoint !== NO_DATA) {
                    count++;
                    sum += datapoint;
                }
            }
            if (count !== 0) {
                this.#chart.data.labels.push(new Date(data.timestamp).toLocaleTimeString());
                this.#chart.data.datasets[0].data.push({
                    x: this.#chart.data.labels.length - 1,
                    y: sum / count
                });
            }
        } else {
            this.#chart.data.labels.push(new Date(data.timestamp).toLocaleTimeString());
            let invalidOffset = 0;
            for (let i in data.sensorknots) {
                const datapoint = data.sensorknots[i][this.#dataType];
                const isSensorWithInvalidData = this.#sensorNodesWithoutValidData.includes(data.sensorknots[i].id);
                if (isSensorWithInvalidData) {
                    invalidOffset++;
                }
                if (datapoint !== NO_DATA) {
                    if (isSensorWithInvalidData) {
                        this.#chart = this.#createNewDiagram(this.#data, false)
                        return;
                    }
                    this.#chart.data.datasets[i - invalidOffset].data.push({
                        x: this.#chart.data.labels.length - 1,
                        y: datapoint
                    });
                }
            }
        }
        this.#checkPointSize();
        this.#chart.update('none');
    }

    /**
     * Register toggles which are bound to the data type shown.
     * The toggles need to have values from {@link DATA_TYPE}.
     * When the <code>change</code> event for on of the toggles is fired,
     * the chart is going to be re-created with the toggle's type.
     * @param toggles {NodeListOf<HTMLInputElement> | HTMLInputElement[]}
     * see e.g. {@link https://getbootstrap.com/docs/5.1/forms/checks-radios/#radio-toggle-buttons}
     */
    registerTypeToggle(toggles) {
        const ctx = this;
        let updatingToggles = false;
        for (const toggle of toggles) {
            toggle.addEventListener('change', function () {
                // The toggles are going to be updated in selectToggle(),
                // but we just want to re-create the chart once.
                if (!updatingToggles) {
                    updatingToggles = true;
                    ctx.#saveDisabledDatasets();
                    ctx.destroy();
                    ctx.#dataType = toggle.value;
                    ctx.#chart = ctx.#createNewDiagram(ctx.#data, false);
                    selectToggle(toggle, toggles);
                    ctx.#restoreDisabledDatasets();
                    updatingToggles = false;
                }
            })
        }

    }

    registerDisplayAverageToggle(toggles) {
        const ctx = this;
        let updatingToggles = false;
        for (const toggle of toggles) {
            toggle.addEventListener('change', function () {
                // The toggles are going to be updated in selectToggle(),
                // but we just want to re-create the chart once.
                if (!updatingToggles) {
                    updatingToggles = true;
                    ctx.destroy();
                    ctx.#displayAverage = toggle.value === "true"
                    ctx.#chart = ctx.#createNewDiagram(ctx.#data, false);
                    selectToggle(toggle, toggles);
                    if (!ctx.#displayAverage) {
                        ctx.#restoreDisabledDatasets();
                    }
                    updatingToggles = false;
                }
            })
        }
    }

    registerOnHoverCallback(onHoverCallback) {
        const chart = this.#chart;
        this.#onHoverCallback = onHoverCallback;
        const ctx = this;
        this.#chart.config.options.onHover = function (e) {
            ctx.#onChartHoverProxy(e, chart, onHoverCallback);
        }
    }

    /**
     * Destroy the chart and the corresponding objects.
     */
    destroy() {
        this.#chart.destroy();
    }

    #generateAverageDataset(sortedData, datasets, labels) {
        let data = [];
        let OData = {};
        sortedData.forEach((sensor, index) => {
            Object.keys(sensor.data).forEach((key, dataIndex) => {
                const datapoint = sensor.data[key];
                const value = datapoint[this.#dataType];
                if (value !== NO_DATA) {
                    if (OData[key] == null) {
                        labels.push(datapoint.time.toLocaleTimeString());
                        OData[key] = {
                            c: 0,
                            s: 0,
                            x: dataIndex
                        }
                    }
                    OData[key].c++;
                    OData[key].s += value;
                }

            });
        });
        if (OData.length !== 0) {
            Object.keys(OData).forEach((key) => {
                const d = OData[key];
                data.push({x: d.x, y: d.s / d.c});
            });
            datasets.push({
                parsing: false,
                data: data,
                label: "Durchschnittswert",
                backgroundColor: `rgba(0, 0, 0, 1)`,
            })
        }
    }

    #generateSingleDatasets(sortedData, datasets, labels) {
        const datapointColors = this.#generateDatapointColors(sortedData.length);
        sortedData.forEach((sensor, index) => {
            let data = [];
            Object.keys(sensor.data).forEach((key, dataIndex) => {
                const datapoint = sensor.data[key];
                const value = datapoint[this.#dataType];
                if (value !== NO_DATA) {
                    if (labels.length === data.length) {
                        labels.push(datapoint.time.toLocaleTimeString())
                    }
                    data.push({x: dataIndex, y: value});
                }

            });
            if (data.length === 0) {
                this.#sensorNodesWithoutValidData.push(sensor.id);
            } else {
                let color = datapointColors[index];
                datasets.push({
                    parsing: false,
                    data: data,
                    label: "Sensor " + sensor.id,
                    backgroundColor: `rgba(${color.r}, ${color.g}, ${color.b}, 1)`,
                })
            }

        });
    }

    /**
     * @param {int} count
     * @return {Array<RGBColor>}
     */
    #generateDatapointColors(count) {
        let res = [];
        for (let i = 0; i < count; i++) {
            // HLS
            res.push(hslToRgb(i / count, 1, 0.5))
        }
        return res;
    }

    /**
     *
     * @param {Object[] | VentilationConcept} dataToDisplay
     * @param duration {false | int} duration in minutes or <code>false</code> to ignore
     * @returns {an}
     */
    #createNewDiagram(dataToDisplay, duration) {
        this.#sensorNodesWithoutValidData = [];
        const oldChart = Chart.getChart(this.#canvas.id)
        if (oldChart != null) {
            oldChart.destroy()
        }
        let datasets = [];
        let labels = [];

        if (Array.isArray(dataToDisplay)) {
            // Order sensor data, chartJs needs always the same order.
            // Measurement cycle data comes ordered by ID,
            // so the sensorData also needs to be ordered by ID.
            // ChartJS needs the data points to be in the same order all the time
            // to assign the to the correct label & sensor.
            const sortedSensorData = dataToDisplay.map(a => {
                return {...a}
            });
            sortedSensorData.sort((a, b) => a.id - b.id);
            if (this.#displayAverage) {
                this.#generateAverageDataset(sortedSensorData, datasets, labels);
            } else {
                this.#generateSingleDatasets(sortedSensorData, datasets, labels);
            }
        } else if (dataToDisplay instanceof VentilationConcept) {
            if (typeof duration !== "number") {
                throw new Error("A valid duration must be specified when passing a function for to generate the data");
            }
            datasets.push({
                data: [],
                ventilationConcept: dataToDisplay,
                backgroundColor: 'rgba(0,0,0,1)'}
            );
            // generate labels with integers from 0 to duration
            labels = Array(duration).fill().map((_, idx) => idx)

        } else {
            throw new Error("Invalid type of param 'dataToDisplay'. Must be either Array or function")
        }

        // assemble the config depending on the data dataType
        let config = {};
        Object.assign(config, JSON.parse(JSON.stringify(defaultConf)));
        config.options.onClick = this.#onChartHoverProxy;
        config.options.scales.y = scales[this.#dataType];
        config.data = {
            labels: labels,
            datasets: datasets
        }

        if (dataToDisplay instanceof VentilationConcept) {
            config.plugins.push(drawVentilationConceptPlugin);
        }

        if (this.#dataType === DATA_TYPE.CO2) {
            config.plugins.push(categorizedBackgroundPlugin);
        }
        if (this.#chartType === CHART_TYPE.LINE) {

            config.options.tension = 0.2
            config.options.elements = {
                point: {
                    radius: 0
                },
            };
            config.options.datasets.line = {
                borderColor: "#000",
                borderWidth: 2
            }
            if (!this.#displayAverage) {
                config.options.scales.y.max = 1400;
                config.options.scales.y.min = 300;
                config.options.plugins = {
                    legend: {display: false},
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem) {
                                return tooltipItem.yLabel;
                            }
                        }
                    }
                };
            }
        } else {
            config.options.datasets.line = {
                showLine: false
            }
        }
        const chart = new Chart(this.#canvas, config);
        const ctx = this;
        if (ctx.#onHoverCallback) {
            chart.config.options.onHover = function (e) {
                ctx.#onChartHoverProxy(e, chart, ctx.#onHoverCallback);
            }
        }
        return chart;
    }

    /**
     *
     * @param e
     * @param chart
     * @param {function} callback
     */
    #onChartHoverProxy(e, chart, callback) {
        if (callback == null || typeof callback != "function") {
            return;
        }
        const canvasPosition = Chart.helpers.getRelativePosition(e, chart);
        const els = chart.getElementsAtEventForMode(e, 'index', { intersect: false }, false);
        const index = els.length === 0 ? undefined : els[0].index;
        if (index) {
            const label = chart.config.data.labels[index];
            callback(canvasPosition, label);
        } else {
            callback(canvasPosition);
        }
    }

    #checkPointSize() {
        if (this.#chartType === CHART_TYPE.POINT) {
            const count = this.#chart.data.labels.length;
            const radius = count < 20 ? 5 : count < 100 ? 4 : count < 200 ? 3 : 2;
            this.#chart.options.elements.point.radius = radius;
        }
    }
    #saveDisabledDatasets() {
        for (const i in this.#chart.data.datasets) {
            const d = this.#chart.data.datasets[i];
            if (this.#chart.isDatasetVisible(i)) {
                if (this.#disabledDatasets.has(d.label)) {
                    this.#disabledDatasets.delete(d.label);
                }
            } else {
                this.#disabledDatasets.add(d.label);
            }
        }
    }
    #restoreDisabledDatasets() {
        for (const i in this.#chart.data.datasets) {
            this.#chart.setDatasetVisibility(
                i, !this.#disabledDatasets.has(this.#chart.data.datasets[i].label));
        }
        this.#chart.update();
    }
}

/**
 * Create a Chart
 * @param canvas {HTMLCanvasElement}
 * @param data {Sensor[] | VentilationConcept}
 * @param dataType {string} value from {@link DATA_TYPE}
 * @param chartType {string} value from {@link CHART_TYPE}
 * @param duration {false | int} duration in minutes or <code>false</code> to ignore
 * @return {DiagramHolder}
 */
export function create(canvas,
                       data,
                       dataType = DATA_TYPE.CO2,
                       chartType = CHART_TYPE.POINT,
                       duration = false) {
    return new DiagramHolder(canvas, data, dataType, chartType, duration)
}

/**
 *
 * @param canvas {HTMLCanvasElement}
 * @param concept {VentilationConcept}
 * @param duration {int | number}
 * @return {DiagramHolder}
 */
export function createDiagramFromVentilationConcept(canvas, concept, duration) {
    return new DiagramHolder(canvas, concept, DATA_TYPE.CO2, CHART_TYPE.LINE, duration);
}

/**
 *
 * @param canvas {HTMLCanvasElement}
 * @param data {Sensor[]}
 * @return {DiagramHolder}
 */
export function createDiagramFromMeasurementData(canvas, data) {
    return new DiagramHolder(canvas, data, DATA_TYPE.CO2, CHART_TYPE.POINT, false);
}