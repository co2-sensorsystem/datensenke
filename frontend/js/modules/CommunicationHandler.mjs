import * as ErrorModal from "./ui/modals/ErrorModal.mjs";
import {MODE} from "../../config.js";

/**
 * @typedef Controller
 * @property {handleServerMessageFunction} handleServerMessage Handle a message sent by the server that is NOT a response to a previously sent request.
 * @property {string} mode the current mode; see {@link MODE}
 */

/**
 * Handle a message sent by the server that is NOT a response to a previously sent request.
 * @typedef handleServerMessageFunction
 * @type function
 * @param {Object} data
 * @return {boolean} whether the message was recognized
 */

/**
 * @typedef CallbackInfo
 * @property {string} event
 * @property {string} [messageId]
 * @property {int} [timeoutId]
 * @property {function(Object)} onSuccess
 * @property {function(string)} onError
 * @property {Object} context / scope
 */

/**
 * @type {Controller}
 */
let controller;

/**
 * IP of the socket / server.
 * @type {string}
 */
const IP = '127.0.0.1';

/**
 * Port of the socket listening for the web application on the server.
 * @type {int}
 */
const port = 12345;

/**
 * @type {WebSocket | null}
 */
let webSocket = null;

/**
 * Whether there is a connection through the {@link webSocket} to the backend.
 * @type {boolean}
 */
let isConnected = false;

/**
 * Index of callbacks indexed by the messageId
 * @type {Object}
 * @property {Object.<string, CallbackInfo>}
 */
let callbacks = {};

/**
 * Counter for messages sent by this handler.
 * The message IDs are generated from this.
 * A message ID has the structure
 * <code>f{@link messageCounter}</code> (f for frontend)
 * @type {number}
 */
let messageCounter = 0;

let queue = [];

let leavingPage = false;

export function init(_controller) {
    controller = _controller
    window.addEventListener('beforeunload', function () {
        leavingPage = true;
    })
    connect();
}

/**
 * Connect to the backend socket.
 * @param retry {boolean}
 * @return {Promise<void>}
 */
async function connect(retry = false) {
    try {
        webSocket = new WebSocket('ws://' + IP + ':' + port);
        webSocket.onmessage = onMessage;
        webSocket.onopen = function () {
            isConnected = true;
            ErrorModal.hide("connection-lost");
            for (const queueElement of queue) {
                send(queueElement.data, queueElement.callback);
                queue.shift();
            }
        }
        webSocket.onclose = function () {
            isConnected = false;
            if (leavingPage) {
                return;
            }
            if (!retry) {
                ErrorModal.open(
                    "Verbindung zum Server getrennt",
                    '<p>Bitte vergewissern Sie sich, dass der Server läuft. ' +
                    'Wenn der Server nicht korrekt läuft, können keine Messdaten ' +
                    'gesammelt werden.</p>',
                    "connection-lost"
                );
            }
            connect(true);
        };
    } catch (exception) {
        console.log(exception);
        // wait 1 second and try again to connect to the backend
        setTimeout(() => {connect(true)}, 1000);
    }
}

/**
 * Send data / request to the backend and execute callbacks depending on the backend's response.
 * @param {Object | ArrayBufferLike | Blob} data the data or request to be sent to the backend
 * @param {CallbackInfo | {}} callbackInfo the callbacks which are executed depending
 * on the response of the request
 * @param {"text" | "file"} [type] - the message
 * @param {string} [messageId]
 */
export function send (data, callbackInfo, type = "text", messageId = null) {
    if (controller && controller.mode === MODE.BLOCKED) {
        // Do not send messages to the server when in blocked mode.
        // They will be ignored anyway.
        return;
    }
    if (callbackInfo === {}) {
        callbackInfo = {
            context: null,
            onSuccess: () => { /* do nothing */ },
            onError: () => { /* do nothing */ }
        }
    }
    if (!isConnected) {
        queue.push({data: data, callback: callbackInfo})
        console.log("No connection to backend: queueing message and attempting to send it"
            + " once a connection is established");
        return;
    }

    if (messageId == null) {
        /**
         * create message ID
         * @see messageCounter
         */
        messageId = "f" + ++messageCounter;
    }

    if (type === "text") {
        data.messageId = messageId;
        webSocket.binaryType = "blob";
        webSocket.send(JSON.stringify(data));
    } else if (type === "file") {
        webSocket.binaryType = "arraybuffer";
        webSocket.send(data);
    } else {
        return;
    }

    // start timout which triggers the callbackInfo's onError method
    // when there was no response from the server within 7 seconds
    callbackInfo.timeoutId = setTimeout(function () {
        callbackInfo.onError.call(callbackInfo.context, " Keine Antwort vom Webserver erhalten.");
        delete callbacks[messageId];
    }, 7000);

    // store the callback info and send the message to the backend
    callbacks[messageId] = callbackInfo;
}

/**
 * <p>Handle a message received on the {@link webSocket}.</p>
 * <p>
 * The message is parsed and checked for errors.
 * If the message is a response to a previous request, the corresponding callback is executed.
 * Other message contents are handled individually.
 * If an error occurs while the message content is handled
 * an error message is displayed to the user.
 * </p>
 * @param {Event} event
 */
function onMessage (event) {
    if (controller && controller.mode === MODE.BLOCKED) {
        // If this client is blocked, we should not get any messages from the backend.
        // But if do we get data for whatever reason,
        // we ignore it to not trigger a mode change.
        return;
    }

    // parse message content
    let data;
    try {
        data = JSON.parse(event.data);
    } catch (e) {
        ErrorModal.open(
            "Fehler beim Parsen einer Servernachricht",
            '<p>Konnte folgende Nachricht des Servers nicht parsen</p>'
                + '<pre><code>' + event.data.toString() + '</code></pre>');
        console.error("Error while parsing the message data");
        console.error(e);
    }

    // handle the message content
    try {
        if (!data.hasOwnProperty("event")) {
            console.log("Received unrecognized message. Message has no 'event' property");
            console.log(event.data);
            return;
        }

        // If the message is a response to a previous request,
        // call the corresponding function stored in the callbackInfo.
        // Otherwise, the method within the controller handling the server messages is called
        // to handle the information.
        if (data.event === "response" && data.hasOwnProperty("messageId")) {
            // response on a message sent by the frontend
            const callbackInfo = callbacks[data.messageId];
            // response came too late; the onError() method has already been called
            // and the callbackInfo has been destroyed.
            if (callbackInfo == null) {
                return;
            }
            // response received, so stop the timeout
            clearTimeout(callbackInfo.timeoutId);
            // call the onError() or onSuccess() callbacks
            // depending on the message's status
            if (data.status === "error") {
                callbackInfo.onError.call(callbackInfo.context, data.data)
            } else {
                callbackInfo.onSuccess.call(callbackInfo.context, data.data)
            }
            // delete callback info, because it is not needed anymore
            delete callbacks[data.messageId];
        } else {
            const recognizedEvent = controller.handleServerMessage(data);
            if (!recognizedEvent) {
                console.error("Received unrecognized message. 'event' property has no known value.");
                console.error(event.data);
            }
        }

    } catch (e) {
        ErrorModal.open(
            "Laufzeitfehler bei Verarbeitung einer Servernachricht",
            '<pre><code>' + e.toString() + '</code></pre>');
        console.error(e);
    }
}

