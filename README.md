# Lüffi

Dies ist die Datensenke und Benutzerschnittstelle des Messsystems zur Optimierung von Lüftungskonzepten ("Lüffi").
Weitere Informationen zu Lüffi und dem generellen Betrieb befinden sich in der zugehörigen Dokumentation, die unter https://gitlab.uni-oldenburg.de/co2-sensorsystem/dokumentation eingesehen werden kann.

## Setup

Für eine erfolgreiche Installation und den Betrieb der Datensenke wird NodeJs in Version 16 oder neuer benötigt.
Zudem sind Werkzeuge zum Kompilieren nativen Programmcodes erforderlich.
Weitere Informationen dazu finden sich unter https://github.com/WiseLibs/better-sqlite3/blob/master/docs/troubleshooting.md.


Für die Installation dieses NodeJs Modules und aller seiner Abhängigkeiten ist folgender Befehl in diesem Verzeichnis auszuführen:
```shell
npm install
```

## Betrieb

Um die gesamte Datensenke zu starten, nutze

```shell
npm run all
```

Zum Testen kann die Datensenke mit emulierten Sensorknoten alternativ mit gestartet werden:

```shell
npm run test
```

-------------

Des Weiteren können die Komponenten der Datensenke auch einzeln ausgeführt werden:

Da die emulierten Sensorknoten nicht funktionsidentisch mit den ihrem physischen Abbild sind, sind bei der separaten Ausführung der Komponenten folgende Regeln zu beachten.
Zuerst muss der _Controller_, der für die Kommunikation zwischen Sensorknoten,
Datenbank und Website zuständig ist, gestartet werden:

```shell
npm run wserver
```

Anschließend können Verbindungen vom Microcontroller und/oder von der Webanwendung hergestellt werden.

Zum Öffnen der Website kann folgender Befehl genutzt werden:

```shell
npm run serve
```

Alternativ kann die Website auch direkt im Browser über den Pfad
`./frontend/index.html` geöffnet werden.

Zum Starten eines einzelnen emulierten Sensorknotens bzw. dessen Mikrocontrollers, kann der Befehl

```shell
npm run micro1
```

verwendet werden. Zum Starten von zehn emulierten Sensorknoten existiert der Befehl

```shell
npm run micro
```


### Datenströme [veraltet]

#### Datenfluss von Webserver zu Webanwendung

co2, temperature, humidity, battery bei nicht vorhandener Messung auf -9999
setzen.

##### Bei Anmeldung eines Clients

```json
{
  "event": "loggedInSensorknots",
  "sensorknots": [
    "id"
  ]
}
```

Ausserdem wird eine Nachricht der aktuellsten Messung gesendet (siehe
`Automatische Messdatenweitergabe für einen Zeitpunkt`).

##### Anmeldung / Abmeldung eines Sensorknotens am Webserver

```json
{
  "event": "login"/"logout",
  "id": int
}
```

##### Automatische Messdatenweitergabe für einen Zeitpunkt

```json
{
  "timestamp": "timestamp",
  "sensorknots": [
    {
      "id": int,
      "co2": int,
      "temperature": float,
      "humidity": float,
      "battery": float
    }
  ],
}
```

##### Verfügbare Messungen

```json
[
  "name": string,
  "timestamp": int,
  "id": int
]
```

##### Abfrage für eine Messung

```json
{
  "timestamp": int,
  "id": int,
  "name": string,
  "sensorknots": [
    {
      "id": int,
      "position": {
        "x": int,
        "y": int
      },
      "mac": string
    }
  ],
  "room": {
    "id": int,
    "name": string,
    "width": int,
    "length": int,
    "image": string|null
  },
  data: {
      "timestamp": [
        {
            "id": int,
            "co2": int,
            "temperature": float,
            "humidity": float,
        }
      ]
    }
}
```

#### Datenfluss von Webanwendung zu Webserver

##### Konfiguration senden

```json
{
  "name": string,
  "room": {
    "id": int,
    OR
    "name": string,
    "width": int,
    "length": int,
  },
  "sensorknots": [
    {
      "id": int,
      "position": {
        "x": int,
        "y": int
      }
    }
  ]
}
```

##### Messung starten/stoppen

```json
{
  "event": "start"/"stop"
}
```

##### Anfrage für Liste aller Messungen

```json
{
  "event": "sendMeasurementList"
}
```

##### Anfrage für eine Messung

```json
{
  "event": "sendSingleMeasurement",
  "id": int
}
```
